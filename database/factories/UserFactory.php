<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {

    $types = [USER_TYPE_KOORDINATOR, USER_TYPE_DRIVER, USER_TYPE_PENGURUS];

    $type = $faker->randomElement($types);
    $status  = ['aktif','nonaktif', 'blokir'];
    return [
        'name' => $faker->name,
        'kode'=>$type == USER_TYPE_DRIVER ? str_random(5) : '',
        'email' => $faker->unique()->safeEmail,
        'username' => $faker->unique()->userName,
        'api_token' => Str::random(60),
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
        'type'=>$type,
        'alamat'=>$faker->address,
        'no_ktp'=>$faker->creditCardNumber,
        'kabupaten'=>$faker->city,
        'kecamatan'=>$faker->city,
        'provinsi'=>$faker->city,
        'status'=>$faker->randomElement($status),
        'verified'=>1
    ];
});
