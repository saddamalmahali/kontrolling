<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Notification::class, function (Faker $faker) {
    $user = \App\User::where('type', '=', USER_TYPE_ADMIN)->get(['id', 'name'])->toArray();
    $user = array_column($user, 'id');

    $types = [DO_SENDING_START, DO_SENDING_STOP];
    $id_notif = gen_uuid();

    $user_driver = \App\User::where('type', '=', USER_TYPE_DRIVER)->get(['id', 'name'])->toArray();

    $data_user = $faker->randomElement($user_driver);
//    echo $data_user['id'];die();

    $data['pesan'] = $faker->paragraph;
    $data['userid'] = $data_user['id'];
    $data['username'] = $data_user['name'];
    $data['transaction_id'] = random_int(1, 1000);


    return [
        'id'                =>  $id_notif,
        'type'              =>  $faker->randomElement($types),
        'notifiable_type'   =>  NOTIFIABLE_USER,
        'notifiable_id'     =>  $faker->randomElement($user),
        'data'              =>  json_encode($data)
    ];
});
