<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBiayaOperasionalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('biaya_operasionals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nomor')->nullable();
            $table->bigInteger('do_id')->nullable();
            $table->date('tanggal')->nullable();
            $table->bigInteger('penerima_id')->nullable();
            $table->integer('kuantitas')->nullable();
            $table->enum('satuan', ['ton', 'kg'])->nullable();
            $table->double('biaya_satuan')->nullable();
            $table->double('total')->nullable();
            $table->enum('jenis_pembayaran', ['cash', 'loan']);
            $table->text('bukti')->nullable();
            $table->enum('status', ['kirim', 'pending'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('biaya_operasionals');
    }
}
