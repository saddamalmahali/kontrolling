<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableClient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('clients')){
            Schema::table('clients', function(Blueprint $table){
                if(!Schema::hasColumn('clients', 'npwp')) $table->string('npwp')->nullable();
                if(!Schema::hasColumn('clients', 'no_rek')) $table->string('no_rek')->nullable();
                if(!Schema::hasColumn('clients', 'tipe_bank')) $table->string('tipe_bank')->nullable();
                if(!Schema::hasColumn('clients', 'telepon')) $table->string('telepon')->nullable();
                if(!Schema::hasColumn('clients', 'email')) $table->string('email')->nullable();
                if(!Schema::hasColumn('clients', 'foto')) $table->string('foto')->nullable();
                if(!Schema::hasColumn('clients', 'jatuh_tempo')) $table->date('jatuh_tempo')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('clients')){
            Schema::table('clients', function(Blueprint $table){
                $table->dropColumn('npwp');
                $table->dropColumn('no_rek');
                $table->dropColumn('tipe_bank');
                $table->dropColumn('telepon');
                $table->dropColumn('email');
                $table->dropColumn('foto');
                $table->dropColumn('jatuh_tempo');
            });
        }
    }
}
