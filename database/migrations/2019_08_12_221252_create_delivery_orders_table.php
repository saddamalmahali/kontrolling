<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('delivery_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nomor')->nullable();
            $table->date('tanggal')->nullable();
            $table->string('provinsi')->nullable();
            $table->string('kabupaten')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('alamat')->nullable();
            $table->string('nama_penerima')->nullable();
            $table->string('client_id')->nullable();
            $table->bigInteger('kuantitas')->nullable();
            $table->enum('satuan', ['ton', 'kg'])->default('ton');
            $table->string('nama_driver')->nullable();
            $table->bigInteger('driver_id')->nullable();
            $table->string('no_kendaraan')->nullable();
            $table->bigInteger('approved_by')->nullable();
            $table->date('approved_date')->nullable();
            $table->text('keterangan')->nullable();
            $table->text('bukti')->nullable();
            $table->enum('status', ['aktif', 'proses', 'selesai'])->default('aktif');
            $table->timestamps();
        });

        Schema::create('detile_do', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_do')->nullable();
            $table->string('kode')->nullable();
            $table->string('nama')->nullable();
            $table->integer('kuantitas')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('delivery_orders');
        Schema::dropIfExists('detile_do');

    }
}
