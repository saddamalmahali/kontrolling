<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableUserV2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('users')){
            Schema::table('users', function(Blueprint $table){
                if(!Schema::hasColumn('users', 'jabatan')) $table->string('jabatan')->nullable();

                if(!Schema::hasColumn('users', 'telepon')) $table->string('telepon')->nullable();
                if(!Schema::hasColumn('users', 'no_rek')) $table->string('no_rek')->nullable();
                if(!Schema::hasColumn('users', 'tipe_bank')) $table->string('tipe_bank')->nullable();
                if(!Schema::hasColumn('users', 'asuransi')) $table->string('asuransi')->nullable();
                if(!Schema::hasColumn('users', 'tanggal_mulai_bekerja')) $table->date('tanggal_mulai_bekerja')->nullable();
                if(!Schema::hasColumn('users', 'status_karyawan')) $table->enum('status_karyawan', ['freelance', 'tetap'])->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('users')){
            Schema::table('users', function(Blueprint $table){
                $table->dropColumn('jabatan');
                $table->dropColumn('telepon');
                $table->dropColumn('no_rek');
                $table->dropColumn('tipe_bank');
                $table->dropColumn('asuransi');
                $table->dropColumn('tanggal_mulai_bekerja');
                $table->dropColumn('status_karyawan');
            });
        }
    }
}
