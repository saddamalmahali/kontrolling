<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('penandatangan')->nullable();
            $table->bigInteger('client_id')->nullable();
            $table->string('nomor')->nullable();
            $table->string('perihal')->nullable();
            $table->date('tanggal')->nullable();
            $table->enum('status', ['aktif', 'cetak', 'lunas'])->nullable();
            $table->timestamps();
        });

        Schema::create('biaya_operasional_invoice', function(Blueprint $table){
            $table->bigInteger('invoice_id');
            $table->bigInteger('biaya_operasional_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
        Schema::dropIfExists('biaya_operasional_invoice');
    }
}
