<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableKendaraanV2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('kendaraan')){
            Schema::table('kendaraan', function(Blueprint $table){
               if(!Schema::hasColumn('kendaraan', 'stnk')) $table->string('stnk')->nullable();
               if(!Schema::hasColumn('kendaraan', 'bpkb')) $table->string('bpkb')->nullable();
               if(!Schema::hasColumn('kendaraan', 'kir')) $table->string('kir')->nullable();
               if(!Schema::hasColumn('kendaraan', 'no_mesin')) $table->string('no_mesin')->nullable();
               if(!Schema::hasColumn('kendaraan', 'no_rangka')) $table->string('no_rangka')->nullable();
               if(!Schema::hasColumn('kendaraan', 'tahun_buat')) $table->integer('tahun_buat')->nullable();

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('kendaraan')){
            Schema::table('kendaraan', function(Blueprint $table){
                if(Schema::hasColumn('kendaraan', 'stnk')) $table->dropColumn('stnk');
                if(Schema::hasColumn('kendaraan', 'bpkb')) $table->dropColumn('bpkb');
                if(Schema::hasColumn('kendaraan', 'kir')) $table->dropColumn('kir');
                if(Schema::hasColumn('kendaraan', 'no_mesin')) $table->dropColumn('no_mesin');
                if(Schema::hasColumn('kendaraan', 'no_rangka')) $table->dropColumn('no_rangka');
                if(Schema::hasColumn('kendaraan', 'tahun_buat')) $table->dropColumn('tahun_buat');

            });
        }
    }
}
