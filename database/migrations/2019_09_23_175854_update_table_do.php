<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableDo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Update Option DO status
        \DB::statement("ALTER TABLE kontrolling_delivery_orders CHANGE COLUMN status status ENUM('aktif', 'proses', 'selesai', 'kirim') NOT NULL DEFAULT 'aktif'");

        if(Schema::hasTable('delivery_orders')){
            Schema::table('delivery_orders', function(Blueprint $table){
                $table->string('address_coordinate')->nullable();
                $table->string('start_position')->nullable();
                $table->string('end_position')->nullable();
                $table->text('track_location')->nullable();
                $table->dateTime('start_delivery_time')->nullable();
                $table->dateTime('end_delivery_time')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('delivery_orders')){
            Schema::table('delivery_orders', function(Blueprint $table){
                $table->dropColumn('address_coordinate');
                $table->dropColumn('start_position');
                $table->dropColumn('end_position');
                $table->dropColumn('track_location');
                $table->dropColumn('start_delivery_time');
                $table->dropColumn('end_delivery_time');
            });
        }
    }
}
