<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTabelDoV4 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        if(Schema::hasTable('delivery_orders')){
//            Schema::table('delivery_orders', function(Blueprint $table){
//                if(!Schema::hasColumn('delivery_orders', 'no_ref')){
//                    $table->string('no_ref')->nullable();
//                }
//            });
//        }

        if(Schema::hasTable('detile_do')){
            Schema::table('detile_do', function (Blueprint $table){
                if(!Schema::hasColumn('detile_do', 'no_do')){
                    $table->string('no_do')->nullable();
                }
                if(!Schema::hasColumn('detile_do', 'no_ref')){
                    $table->string('no_ref')->nullable();
                }
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        if(Schema::hasTable('delivery_orders')){
//            Schema::table('delivery_orders', function(Blueprint $table){
//                if(Schema::hasColumn('delivery_orders', 'no_ref')){
//                    $table->dropColumn('no_ref');
//                }
//            });
//        }
        if(Schema::hasTable('detile_do')){
            Schema::table('detile_do', function (Blueprint $table){
                if(Schema::hasColumn('detile_do', 'no_do')){
                    $table->dropColumn('no_do');
                }
                if(Schema::hasColumn('detile_do', 'no_ref')){
                    $table->dropColumn('no_ref');
                }
            });
        }
    }
}
