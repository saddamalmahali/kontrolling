<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKendaraansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kendaraan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode')->unique()->nullable();
            $table->string('seri')->unique()->nullable();
            $table->string('no_plat')->unique()->nullable();
            $table->string('jenis_kendaraan')->nullable();
            $table->string('pabrikan')->nullable();
            $table->enum('status', ['aktif', 'tidak_aktif', 'dipakai'])->default('aktif');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kendaraan');
    }
}
