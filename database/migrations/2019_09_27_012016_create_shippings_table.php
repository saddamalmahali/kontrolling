<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shippings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('do_id');
            $table->string('nomor')->nullable();
            $table->date('tanggal')->nullable();
            $table->integer('tarif_id')->nullable();
            $table->integer('biaya_supir')->nullable();
            // Update Versi V3
            $table->integer('kuantitas')->nullable();
            $table->integer('pecah')->nullable();
            $table->integer('subsidi_pecah')->nullable();
            $table->double('total', 20, 2)->nullable();
            // End of Update Versi V3
            $table->double('tarif')->nullable();
            $table->double('bop')->nullable();
            $table->enum('status', ['ditagihkan', 'belum_ditagih'])->default('belum_ditagih');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shippings');
    }
}
