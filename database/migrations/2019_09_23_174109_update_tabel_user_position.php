<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTabelUserPosition extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('users')){
            if(!Schema::hasColumn('users', 'last_location')){
                Schema::table('users', function(Blueprint $table){
                    $table->string('last_position')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('users')){
            if(Schema::hasColumn('users', 'last_location')){
                Schema::table('users', function(Blueprint $table){
                    $table->removeColumn('last_position');
                });
            }
        }
    }
}
