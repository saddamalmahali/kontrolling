<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('clients')){
            Schema::create('clients', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('kode')->nullable();
                $table->string('nama')->nullable();
                $table->text('alamat')->nullable();
                $table->bigInteger('kuota')->nullable();
                $table->enum('status', ['aktif', 'nonaktif', 'blokir'])->default('nonaktif');
                $table->string('logo')->nullable()->default(null);
                $table->timestamps();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
