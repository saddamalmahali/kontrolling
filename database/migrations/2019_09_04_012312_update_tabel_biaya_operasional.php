<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTabelBiayaOperasional extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('biaya_operasionals')){
            Schema::table('biaya_operasionals', function(Blueprint $table){
                $table->dropColumn('biaya_satuan');
                $table->dropColumn('total');
                $table->double('biaya')->nullable();
                $table->string('tujuan')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('biaya_operasionals')){
            Schema::table('biaya_operasionals', function(Blueprint $table){
                $table->dropColumn('biaya');
                $table->dropColumn('tujuan');
                $table->double('biaya_satuan')->nullable();
                $table->double('total')->nullable();
            });
        }
    }
}
