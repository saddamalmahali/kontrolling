<?php

use Illuminate\Database\Seeder;

class OptionsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(!option_exists('min_kuota') && !option_exists('max_kuota')){
            option(['min_kuota' => 20, 'max_kuota'=>200]);
        }

        if(!option_exists('site_name')){
            option(['site_name'=>'Aplikasi Kontrolling']);
        }

        if(!option_exists('logo_text')){
            option(['logo_text'=>'KONTROLLING']);
        }


    }
}
