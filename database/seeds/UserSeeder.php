<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name'=>'Admin Aplikasi',
            'email'=>'admin@mail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('tahun2019'),
            'username'=>'admin',
            'kode'=>str_random(15),
            'type'=>1,
            'verified'=>true,
            'no_ktp'=>'3049230940932094',
            'provinsi'=>'Jawa Barat',
            'kabupaten'=>'Garut',
            'kecamatan'=>'Tarogong Kaler',
            'alamat'=>'Rancabango Estate',
            'status'=>'aktif'
        ]);
    }
}
