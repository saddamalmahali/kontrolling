<?php

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('Home', route('home'));
});

// Breadcrumbs Untuk Karyawan
Breadcrumbs::for('karyawan', function ($trail) {
    $trail->parent('home');
    $trail->push('Karyawan', route('karyawan'));
});
Breadcrumbs::for('karyawan.add', function ($trail) {
    $trail->parent('karyawan');
    $trail->push('Tambah Karyawan', route('karyawan.add'));
});

Breadcrumbs::for('karyawan.edit', function ($trail, $karyawan) {
    $trail->parent('karyawan');
    $trail->push('Edit Karyawan : '.strtoupper($karyawan->name).' ('.$karyawan->kode.')', route('karyawan.edit', ['id'=>$karyawan->id]));
});

// Breadcrumbs untuk client
Breadcrumbs::for('client', function ($trail) {
    $trail->parent('home');
    $trail->push('Klien', route('client'));
});
Breadcrumbs::for('client.add', function ($trail) {
    $trail->parent('client');
    $trail->push('Tambah Client', route('client.add'));
});

Breadcrumbs::for('client.edit', function ($trail, $client) {
    $trail->parent('client');
    $trail->push('Update Client : '.$client->kode, route('client.edit', ['id'=>$client->id]));
});

Breadcrumbs::for('driver', function ($trail) {
    $trail->parent('home');
    $trail->push('Driver', route('driver'));
});

Breadcrumbs::for('kendaraan', function ($trail) {
    $trail->parent('home');
    $trail->push('Kendaraan', route('kendaraan'));
});
Breadcrumbs::for('kendaraan.add', function ($trail) {
    $trail->parent('kendaraan');
    $trail->push('Tambah Kendaraan', route('kendaraan.add'));
});

Breadcrumbs::for('kendaraan.edit', function ($trail, $kendaraan) {
    $trail->parent('kendaraan');
    $trail->push('Edit Kendaraan : '.$kendaraan->no_plat, route('kendaraan.edit', ['id'=>$kendaraan->id]));
});

Breadcrumbs::for('driver.add', function ($trail) {
    $trail->parent('driver');
    $trail->push('Tambah Driver', route('driver.add'));
});

Breadcrumbs::for('driver.update', function ($trail, $driver) {
    $trail->parent('driver');
    $trail->push('Update Driver : '.$driver->kode, route('driver.edit', ['id'=>$driver->id]));
});

// Breadcrumb untuk Delivery Order
Breadcrumbs::for('do', function ($trail) {
    $trail->parent('home');
    $trail->push('Delivery Order', route('do'));
});
Breadcrumbs::for('do.add', function ($trail) {
    $trail->parent('do');
    $trail->push('Tambah DO', route('do.add'));
});

Breadcrumbs::for('do.edit_detile', function ($trail, $do) {
    $trail->parent('do');
    $trail->push('Edit Detile Barang DO : '.$do->nomor, route('do.edit', ['id'=>$do->id]));
});

Breadcrumbs::for('do.proses', function ($trail, $do) {
    $trail->parent('do');
    $trail->push('Proses DO : '.$do->nomor, route('do.proses', ['id'=>$do->id]));
});

Breadcrumbs::for('do.detail', function ($trail, $do) {
    $trail->parent('do');
    $trail->push('Invoice Order : '.$do->nomor, route('do.detail', ['id'=>$do->id]));
});

Breadcrumbs::for('do.stop', function ($trail, $do) {
    $trail->parent('do');
    $trail->push('Konfirmasi Pengiriman : '.$do->nomor, route('do.stop', ['id'=>$do->id]));
});

Breadcrumbs::for('setting', function ($trail) {
    $trail->parent('home');
    $trail->push('Setting Aplikasi', route('setting.app'));
});

// Tarif
Breadcrumbs::for('tarif', function ($trail) {

    $trail->parent('home');
    $trail->push('Data Tarif', route('tarif'));
});
Breadcrumbs::for('tarif.add', function ($trail) {
    $trail->parent('tarif');
    $trail->push('Tambah Tarif', route('tarif.add'));
});

Breadcrumbs::for('tarif.import', function ($trail) {
    $trail->parent('tarif');
    $trail->push('Import Data Tarif', route('tarif.import'));
});

Breadcrumbs::for('tarif.edit', function ($trail, $tarif) {
    $trail->parent('tarif');
    $trail->push('Rubah Tarif ', route('tarif.edit', ['id'=>$tarif->id]));
});
Breadcrumbs::for('laporan.do', function ($trail) {

    $trail->parent('home');
    $trail->push('Laporan DO', route('laporan.do'));
});
Breadcrumbs::for('laporan.bop', function ($trail) {

    $trail->parent('home');
    $trail->push('Laporan BOP', route('laporan.bop'));
});

Breadcrumbs::for('laporan.bop.detile', function ($trail, $user) {

    $trail->parent('laporan.bop');
    $trail->push('Laporan BOP Detile : '.$user->name, route('laporan.bop.detile', ['user'=>$user->id]));
});


Breadcrumbs::for('laporan.invoice', function ($trail) {

    $trail->parent('home');
    $trail->push('Laporan Invoice', route('laporan.invoice'));
});

Breadcrumbs::for('laporan.monitoring', function ($trail) {

    $trail->parent('home');
    $trail->push('Laporan Monitoring', route('laporan.monitoring'));
});

