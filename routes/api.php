<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Api\AuthController@login');
    Route::post('signup', 'Api\AuthController@signup');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'Api\AuthController@logout');
        Route::get('user', 'Api\AuthController@user');

    });
});

/**
 * DO Controller
 */
Route::group([
    'prefix'=>'do',
    'middleware'=>'auth:api'
], function(){
    Route::post('/get_no_do',       ['as'=> 'api.do.no_do', 'uses'=>'Api\DoController@getNoDo']);
    Route::post('/ajukan_do',       ['as'=> 'api.do.ajukan_do', 'uses'=>'Api\DoController@ajukanDo']);
    Route::post('/get_do',          ['as'=> 'api.do.get_do', 'uses'=>'Api\DoController@getDo']);
    Route::post('/start_track',     ['as'=> 'api.do.start_track', 'uses'=>'Api\DoController@startTrack']);
    Route::post('/stop_track',      ['as'=> 'api.do.stop_track', 'uses'=>'Api\GrController@simpanGr']);
});

Route::group([
    'prefix'=>'laporan',
    'middleware'=>'auth:api'
], function(){
    Route::get('/pengiriman',       ['as'=> 'api.laporan.pengiriman', 'uses'=>'Api\LaporanController@pengiriman']);

});

Route::group([
    'prefix'=>'good_receipt',
    'middleware'=>'auth:api'
], function(){
    Route::get('/get_no_gr',       ['as'=> 'api.do.no_do', 'uses'=>'Api\GrController@getNoGr']);

});

/**
 * Driver Controller
 */
Route::group([
    'prefix'=>'driver',
    'middleware'=>'auth:api'
], function(){
    Route::post('/list_driver', ['as'=> 'api.driver.list', 'uses'=>'Api\DriverController@listDriver']);

});

/**
 * Driver Controller
 */
Route::group([
    'prefix'=>'client',
    'middleware'=>'auth:api'
], function(){
    Route::post('/list_client', ['as'=> 'api.client.list', 'uses'=>'Api\ClientController@listClient']);

});


/**
 * User Constroller
 */
Route::group([
    'prefix'=>'user',
    'middleware'=>'auth:api'
], function(){
    Route::post('/update_current_location', ['as'=> 'api.user.update_current_location', 'uses'=>'Api\UserController@updateCurentLocation']);
});



