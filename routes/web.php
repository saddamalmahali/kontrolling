<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(!auth()->check()){
        return redirect()->route('login');
    }

    return redirect()->route('home');
});


Auth::routes();

    Route::group(['prefix'=>'debug'], function(){
        Route::get('/map_default',['as'=>'debug.map_default', 'uses'=>'DebugController@debugMap']);
    });


// Route untuk notifikasi
Route::get('/services/notifikasi/read/{notif}', 'ServicesController@read_notification')->name('notifications.read');

Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/karyawan', 'KaryawanController@index')->name('karyawan');
//
//Route::post('/karyawan', 'KaryawanController@index')->name('karyawan.data');


// Wrapper untuk Ajax Controller
Route::group(['prefix'=>'ajax'], function (){
    Route::get('/get_data_driver',                          ['as'=>'ajax.data_driver',          'uses'=>'AjaxController@dataDriver']);
    Route::get('/get_data_driver_kendaraan/{user}',         ['as'=>'ajax.data_driver',          'uses'=>'AjaxController@dataKendaraanDriver']);
    Route::get('/get_data_province',                        ['as'=>'ajax.data_province',          'uses'=>'AjaxController@dataDriver']);
    Route::get('/get_data_client',                          ['as'=>'ajax.data_client',          'uses'=>'AjaxController@dataClient']);
    Route::get('/get_data_kendaraan',                       ['as'=>'ajax.data_kendaraan',          'uses'=>'AjaxController@dataKendaraan']);
});

Route::group(['prefix'=>'karyawan'], function (){
    Route::get('/',                     ['as'=>'karyawan',                  'uses'=>'KaryawanController@index']);
    Route::get('/tambah',               ['as'=>'karyawan.add',              'uses'=>'KaryawanController@formAdd']);
    Route::get('/edit/{id}',            ['as'=>'karyawan.edit',             'uses'=>'KaryawanController@formEdit']);
    Route::post('/hapus',               ['as'=>'karyawan.hapus',            'uses'=>'KaryawanController@hapusData']);
    Route::post('/simpan',              ['as'=>'karyawan.simpan',           'uses'=>'KaryawanController@simpanData']);
});

Route::group(['prefix'=>'driver'], function (){
    Route::get('/',                         ['as'=>'driver',                'uses'=>'DriverController@index']);
    Route::get('/tambah',                   ['as'=>'driver.add',            'uses'=>'DriverController@formAdd']);
    Route::get('/edit/{id}',                ['as'=>'driver.edit',           'uses'=>'DriverController@formEdit']);
    Route::get('/hapus_kendaraan/{driver}', ['as'=>'driver.hapus_kendaraan','uses'=>'DriverController@hapusKendaraan']);
    Route::post('/hapus',                   ['as'=>'driver.hapus',          'uses'=>'DriverController@hapusData']);
    Route::post('/simpan',                  ['as'=>'driver.simpan',         'uses'=>'DriverController@simpanData']);
});

Route::group(['prefix'=>'kendaraan'], function (){
    Route::get('/',                     ['as'=>'kendaraan',                'uses'=>'KendaraanController@index']);
    Route::get('/tambah',               ['as'=>'kendaraan.add',            'uses'=>'KendaraanController@formAdd']);
    Route::get('/edit/{id}',            ['as'=>'kendaraan.edit',           'uses'=>'KendaraanController@formEdit']);
    Route::post('/hapus',               ['as'=>'kendaraan.hapus',         'uses'=>'KendaraanController@hapusData']);
    Route::post('/simpan',              ['as'=>'kendaraan.simpan',         'uses'=>'KendaraanController@simpanData']);
});

Route::group(['prefix'=>'client'], function (){
    Route::get('/',                         ['as'=>'client',                'uses'=>'ClientController@index']);
    Route::get('/tambah',                   ['as'=>'client.add',            'uses'=>'ClientController@formAdd']);
    Route::get('/edit/{id}',                ['as'=>'client.edit',           'uses'=>'ClientController@formEdit']);
    Route::post('/simpan',                  ['as'=>'client.simpan',         'uses'=>'ClientController@simpanData']);
    Route::post('/konfirmasi/{client}',     ['as'=>'client.konfirmasi',     'uses'=>'ClientController@konfirmasiClient']);
    Route::post('/hapus',                   ['as'=>'client.hapus',          'uses'=>'ClientController@hapusData']);
});

Route::group(['prefix'=>'tarif'], function (){
    Route::get('/',                     ['as'=>'tarif',                     'uses'=>'TarifController@index']);
    Route::get('/tambah',               ['as'=>'tarif.add',                 'uses'=>'TarifController@formAdd']);
    Route::get('/edit/{id}',            ['as'=>'tarif.edit',                'uses'=>'TarifController@formEdit']);
    Route::post('/simpan',              ['as'=>'tarif.simpan',              'uses'=>'TarifController@simpanData']);
    Route::post('/hapus',               ['as'=>'tarif.hapus',               'uses'=>'TarifController@hapusData']);
    Route::get('/import',               ['as'=>'tarif.import',              'uses'=>'TarifController@importData']);
    Route::get('/download_tujuan',      ['as'=>'tarif.download_tujuan',     'uses'=>'TarifController@downloadTujuan']);
    Route::post('/import',              ['as'=>'tarif.import',              'uses'=>'TarifController@actionImport']);
});
Route::group(['prefix'=>'laporan'], function (){
    Route::get('/monitoring',           ['as'=>'laporan.monitoring',            'uses'=>'LaporanController@monitoring']);
    Route::get('/do',                   ['as'=>'laporan.do',                    'uses'=>'LaporanController@do']);
    Route::get('/bop',                  ['as'=>'laporan.bop',                   'uses'=>'LaporanController@bop']);
    Route::get('/bop/{id}',             ['as'=>'laporan.bop.detile',             'uses'=>'LaporanController@bopDetile']);
    Route::get('/tagihan',              ['as'=>'laporan.tagihan',               'uses'=>'LaporanController@tagihan']);
    Route::get('/invoice',              ['as'=>'laporan.invoice',               'uses'=>'LaporanController@invoice']);
});

Route::group(['prefix'=>'do'], function (){
    Route::get('/',                         ['as'=>'do',                    'uses'  =>'DoController@index']);
    Route::get('/tambah',                   ['as'=>'do.add',                'uses'  =>'DoController@formAdd']);
    Route::get('/start_pengiriman/{do}',    ['as'=>'do.kirim',              'uses'  =>'DoController@startPengiriman']);
    Route::get('/stop_pengiriman/{do}',     ['as'=>'do.stop',               'uses'  =>'DoController@stopPengiriman']);
    Route::get('/edit/{id}',                ['as'=>'do.edit',               'uses'  =>'DoController@formEdit']);
    Route::post('/simpan',                  ['as'=>'do.simpan',             'uses'  =>'DoController@simpanData']);
    Route::post('/simpan_detile',           ['as'=>'do.simpan_detile',      'uses'  =>'DoController@simpanDetile']);
    Route::post('/simpan_konfirmasi',       ['as'=>'do.simpan_konfirmasi',  'uses'  =>'DoController@simpanKonfirmasi']);
    Route::get('/proses/{id}',              ['as'=>'do.proses',             'uses'  =>'DoController@proses']);
    Route::post('/proses_bop',              ['as'=>'do.simpan_bop',         'uses'  =>'DoController@simpanBop']);
    Route::post('/hapus',                   ['as'=>'do.hapus',              'uses'  =>'DoController@hapusData']);
    Route::get('/detail/{id}',              ['as'=>'do.detail',             'uses'  =>'DoController@detailDo']);
});

Route::group(['prefix'=>'bop'], function (){
    Route::get('/',                     ['as'=>'bop',                'uses'=>'BopController@index']);
    Route::post('/add_invoice',         ['as'=>'bop.add_invoice',    'uses'=>'BopController@addInvoice']);
});

Route::group(['prefix'=>'setting'], function() {
   Route::get('/',                      ['as'=>'setting.app',            'uses'  =>'SettingController@index']);
   Route::post('/simpan',               ['as'=>'setting.save',           'uses'  =>'SettingController@simpan']);
});

//Route::get('/do', 'HomeController@index')->name('do');
//Route::get('/bop', 'HomeController@index')->name('bop');
Route::get('/invoice', 'HomeController@index')->name('invoice');


//Verifikasi Token
Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');
