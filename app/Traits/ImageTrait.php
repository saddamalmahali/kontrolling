<?php
namespace App\Traits;

use App\Image;

trait ImageTrait {
    public function uploadImage($files) {

        // for save original image
        $ImageUpload = \Image::make($files);
        $originalPath = 'images/';
//        if (!file_exists($originalPath)) {
//            mkdir($originalPath, 666, true);
//        }
        $ImageUpload->save($originalPath.time().str_replace(' ', '', $files->getClientOriginalName()));

        // for save thumnail image
        $thumbnailPath = 'thumbnail/';
//        if (!file_exists($thumbnailPath)) {
//            mkdir($thumbnailPath, 666, true);
//        }
        $ImageUpload->resize(250,125);
        $ImageUpload = $ImageUpload->save($thumbnailPath.time().str_replace(' ', '', $files->getClientOriginalName()));
        $imageName = time().str_replace(' ', '', $files->getClientOriginalName());
        $photo = new Image();
        $photo->photo_name = $imageName;
        $photo->original_path = $originalPath.$imageName;
        $photo->thumbnail_path = $thumbnailPath.$imageName;
        return $photo;
//        if($photo->save()){
//            return $photo->id;
//        }
    }

    public function uploadImage2($files) {

        // for save original image
        $ImageUpload = \Image::make($files);
        $originalPath = 'images/';
        $imageName = time().uniqid().'.jpg';
//        if (!file_exists($originalPath)) {
//            mkdir($originalPath, 666, true);
//        }
        $ImageUpload->save($originalPath.$imageName);

        // for save thumnail image
        $thumbnailPath = 'thumbnail/';
//        if (!file_exists($thumbnailPath)) {
//            mkdir($thumbnailPath, 666, true);
//        }
        $ImageUpload->resize(250,125);
        $ImageUpload = $ImageUpload->save($thumbnailPath.$imageName);

        $photo = new Image();
        $photo->photo_name = $imageName;
        $photo->original_path = $originalPath.$imageName;
        $photo->thumbnail_path = $thumbnailPath.$imageName;
//        return $photo;
        if($photo->save()){
            return $photo->id;
        }
    }
}
