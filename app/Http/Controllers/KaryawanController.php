<?php

namespace App\Http\Controllers;

use App\Traits\ImageTrait;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class KaryawanController extends Controller
{
    use ImageTrait;
    protected $data_length = 10;
    protected $order_method = 'asc';
    protected $column_order = 'name';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        $list_data = new User();

        if($request->user()->type == USER_TYPE_ADMIN){
            $list_data = $list_data->where('type', '!=', USER_TYPE_OWNER);
        }

        if($request->has('data_length')){
            $this->data_length = $request->get('data_length');
        }

        if($request->has('order_by')){
            if($request->get('order_by')){
                $this->column_order = $request->get('order_by');
            }

        }

        if($request->has('order_method')){
            if($request->get('order_method')){
                $this->order_method = $request->get('order_method');
            }
        }

        if($request->has('kode_search')){
            $list_data = $list_data->where('kode', 'like', '%'.$request->get('kode_search').'%');
        }
        if($request->has('nama_search')){
            $list_data = $list_data->where('name', 'like', '%'.$request->get('nama_search').'%');
        }

        $list_data = $list_data->where('type', '=', USER_TYPE_PENGURUS);

        $list_data = $list_data->orderBy($this->column_order,  $this->order_method);

        $list_data = $list_data->paginate($this->data_length);


        return view('karyawan.index', compact('list_data'));
    }

    public function formAdd()
    {
        $generate_code = $this->getGenerateCode('KR');
        return view('karyawan.add', compact('generate_code'));
    }

    public function formEdit($id, Request $request)
    {
        $karyawan =  User::findOrFail($id);

        return view('karyawan.edit', compact('karyawan'));
    }

    public function simpanData(Request $request)
    {
        $validationData = $this->validationData();
        if($request->has('id')){
            unset($validationData['username']);
            unset($validationData['email']);
            if(!$request->get('password')){
                unset($validationData['password']);
            }
        }

        $this->validate($request, $validationData);

        $data = $this->buildData($request->all());
        $data ['type'] = USER_TYPE_PENGURUS;
        $data['status'] = USER_STATUS_AKTIF;
        $image_id = null;
        if($request->hasFile('foto')){
            $file_foto = $request->foto;
            $image_id = $this->uploadImage($file_foto);
        }

        $data['foto']=$image_id;

        if(auth()->user()->type == 1){
            $data['verified'] = 1;
        }


        if(isset($data['id'])){
            $data_simpan = User::findOrFail($data['id'])->update($data);
        }else{
            $data_simpan = User::create($data);
        }

        if($data_simpan){
            return redirect()->route('karyawan')->with('success', 'Berhasil Menyimpan Data Karyawan');
        }


    }

    public function hapusData(Request $request)
    {
        $driver = User::findOrFail($request->get('id'));

        if($driver->delete()){
            return redirect()->route('karyawan')->with('success', 'Sukses Menghapus Data Karyawan');
        }
    }

    private function buildData($request){
        // Build Data Driver
        $request = array_filter($request);
        $data = array();
        foreach ($request as $key => $value){
            if($key == '_token' || $key == 'password_confirmation') continue;

            if($key == 'password') $value = Hash::make($value);

            if($key == 'tanggal_mulai_bekerja') $value = date('Y-m-d', strtotime($value));

            $data[$key] = $value;
        }

        return $data;
    }

    public function validationData()
    {
        return [
            'name'              =>  'required|min:5',
            'no_ktp'            =>  'required|size:16',
            'provinsi'          =>  'required',
            'kabupaten'         =>  'required',
            'kecamatan'         =>  'required',
            'alamat'            =>  'required',
            'email'             =>  'required|email|unique:users',
            'username'          =>  'required|unique:users',
            'password'          =>  'required|confirmed'
        ];
    }
}
