<?php

namespace App\Http\Controllers;

use App\BiayaOperasional;
use App\DetileDelivery;
use App\GoodReceipt;
use App\Shipping;
use App\Tarif;
use App\Traits\ImageTrait;
use App\User;
use App\DeliveryOrder;
use Illuminate\Http\Request;
use DB;
class DoController extends Controller
{
    use ImageTrait;

    protected $data_length = 10;
    protected $order_method = 'asc';
    protected $column_order = 'nomor';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $list_data = new DeliveryOrder();

        // Jika Tipe User Login adalah Driver
        if(auth()->user()->type == USER_TYPE_DRIVER){
            $list_data = $list_data->where('driver_id', '=', auth()->user()->id);
        }

        if($request->has('data_length')){
            $this->data_length = $request->get('data_length');
        }

        if($request->has('order_by')){
            if($request->get('order_by')){
                $this->column_order = $request->get('order_by');
            }

        }

        if($request->has('order_method')){
            if($request->get('order_method')){
                $this->order_method = $request->get('order_method');
            }
        }

        /** Filtering Search */

        if($request->has('search_status')){
            if($request->search_status){
                $list_data = $list_data->where('status', '=', $request->get('search_status'));
            }
        }
//
        if($request->has('kode_search')){
            if($request->kode_search){
                $list_data = $list_data->where('nomor', 'like', '%'.$request->get('kode_search').'%');
            }
        }

        if($request->has('tanggal_search')){
            if($request->tanggal_search){
                $tanggal = date('Y-m-d', strtotime($request->tanggal_search));
                $list_data = $list_data->where('tanggal', '=', $tanggal);
            }
        }

        /** End Of Filtering Search */



//        $list_data = $list_data->where('type', '=', USER_TYPE_DRIVER);
        $list_data = $list_data->orderBy('created_at', 'desc');

        $list_data = $list_data->orderBy($this->column_order,  $this->order_method);

        $list_data = $list_data->paginate($this->data_length);


        return view('do.index', compact('list_data'));
    }

    public function  formAdd()
    {
        if(auth()->user()->type == USER_TYPE_DRIVER){
            if(auth()->user()->list_kendaraan()->count() < 1){
                return redirect()->back()->with('failed', 'Anda Belum memiliki pegangan kendaraan, silahkan hubungi tim admin untuk memastikan pegangan kendaraan!');
            }
        }
        $generate_code = $this->getGenerateCode('DO');
        return view('do.add', compact('generate_code'));
    }

    public function  formEdit($id)
    {
        $do = DeliveryOrder::findOrFail($id);
        $detile = $do->detile;
        return view('do.edit', compact('do', 'detile'));
    }

    public function simpanDetile(Request $request)
    {
        if($request->has('id') && $request->has('produk')){
            $produk = $request->get('produk');
            $do = DeliveryOrder::findOrFail($request->get('id'));
            $do->detile()->delete();

            foreach ($produk as $index => $value){

                $detile = new DetileDelivery();
                $detile->id_do      = $do->id;
                $detile->kode       = $value['kode'];
                $detile->nama       = $value['nama'];
                $detile->kuantitas  = $value['kuantitas'];

                $detile->save();
            }

            return redirect()->route('do')->with('sukses', 'Berhasil Merubah Data Produk Barang');

        }
    }

    public function simpanData(Request $request)
    {
//        echo '<pre>';
//        return  var_dump($request->all());
//        $this->validate($request,[
//            'coba'=>'required'
//        ]);
        // Inisialisasi Variabel untuk Penyimpanan
//        $this->validate($request, [
//
//        ]);
        $kode = $request->get('kode');
        $tanggal = date('Y-m-d');
        $driver = $request->get('driver');
        $no_kendaraan = $request->get('no_kendaraan');
        $client = $request->get('client');
        $penerima = $request->get('penerima');
        $provinsi = $request->get('provinsi');
        $kabupaten = $request->get('kabupaten');
        $kecamatan = $request->get('kecamatan');
        $alamat = $request->get('alamat');
        $produk = $request->get('produk');

        DB::beginTransaction();

        try{
            $do = new DeliveryOrder();
            $do->nomor = strtoupper($kode);
            $do->tanggal = strtoupper($tanggal);
            $do->provinsi = strtoupper($provinsi);
            $do->kabupaten = strtoupper($kabupaten);
            $do->kecamatan = strtoupper($kecamatan);
            $do->alamat = strtoupper($alamat);
            $do->nama_penerima = strtoupper($penerima);
            $do->client_id = $client;
            // Total Kuantitas Belum dimasukan
            $driver = User::findOrFail($driver);
            $do->nama_driver = strtoupper($driver->name);
            $do->driver_id = $driver->id;
            $do->no_kendaraan = strtoupper($no_kendaraan);
            if(auth()->user()->type != USER_TYPE_DRIVER){
                $do->approved_by = auth()->user()->id;
                $do->approved_date = date('Y-m-d');
                $do->status = 'aktif';
            }

            $save_do = $do->save();
//            echo $save_do;die();
            $save_detile = false;
            if($save_do){
                foreach ($produk as $index => $value){

                    $detile = new DetileDelivery();
                    $detile->id_do      = $do->id;
                    $detile->no_do      = strtoupper($value['no_do']);
                    $detile->no_ref     = array_key_exists('no_ref', $value) ? strtoupper($value['no_ref']) : '';
//                    $detile->kode       = strtoupper($value['kode']);
                    $detile->nama       = strtoupper($value['nama']);
                    $detile->kuantitas  = $value['kuantitas'];
                    if($detile->save()){
                        $save_detile = true;
                    }else{
                        $save_detile = false;
                    }
                }
            }

            if($save_do && $save_detile){
                DB::commit();
                return redirect()->route('do')->with('sukses', 'Berhasil Menyimpan Delivery Order!');
            }else{
                DB::rollback();
                return redirect()->route('do')->with('failed', 'Terjadi Kesalahan Dalam Menyimpan Data Order!');
            }
        }catch (Exception $e){
//            \Log::error($e);
            DB::rollback();
            return redirect()->route('do')->with('failed', 'Terjadi Kesalahan Dalam Menyimpan Data Order!');
        }




    }

    public function proses($id, Request $request) {
        $do = DeliveryOrder::findOrFail($id);
        $generate_code = $this->getGenerateCode('OP');
        $list_driver =  User::where('type', '=', USER_TYPE_DRIVER)->get();
        $list_tarif = DB::table('tarifs as t')
            ->select([
                DB::raw('(select kic.name from '.DB::getTablePrefix().'indonesia_cities kic where kic.id='.DB::getTablePrefix().'t.asal_id) as asal'),
                DB::raw('(select kic.name from '.DB::getTablePrefix().'indonesia_cities kic where kic.id='.DB::getTablePrefix().'t.tujuan_id) as tujuan'),
                'tarif', 'bop', 'id'
            ]);
        $tarif_select = [];
        foreach ($list_tarif->get() as $tarif){
            $opsi = [
                'id'=>$tarif->id,
                'text'=>$tarif->asal.' - '.$tarif->tujuan.' ( Rp. '.number_format($tarif->bop, 0, ',', '.').' )',
                'tarif'=>$tarif->tarif
            ];
            array_push($tarif_select, $opsi);
        }



//        return $tarif_select;
        return view('do.proses', compact(['do', 'generate_code', 'list_driver', 'tarif_select']));
    }

    public function simpanBop(Request $request) {
        $this->validate($request, [
            'biaya'=>'required|numeric',
            'bukti'=>'required|file',
            'tarif_id'=>'required'
        ]);
        DB::beginTransaction();
        try{

            $bukti = $request->file('bukti');
            $do  = DeliveryOrder::findOrFail($request->get('do_id'));

            $bop  = new BiayaOperasional();
            $bop->nomor = $request->get('nomor');
            $bop->do_id = $do->id;
            $bop->tanggal = date('Y-m-d');
            $bop->penerima_id = $do->driver_id;
            $bop->kuantitas = $do->detile()->sum('kuantitas');
            $bop->satuan = 'kg';
            $bop->jenis_pembayaran = 'cash';
            $image_id = null;
            if($request->hasFile('bukti')){
                $bukti = $request->file('bukti');
                $image_id = $this->uploadImage($bukti);
            }
            $bop->bukti = $image_id;
            $bop->status = BOP_STATUS_PENDING;

            $bop->tujuan = $do->kabupaten;

            $save_bop = $bop->save();
            if($save_bop){
                $bop->image()->save($image_id);
                $do->status = 'proses';
                $do_update_save = $do->save();
                if($do_update_save){
                    $tarif_id = $request->get('tarif_id');
                    /**
                     * Crete Shipping Data
                     */
                    $tarif = Tarif::findOrFail($tarif_id);
                    $total_kuantitas = $do->detile()->sum('kuantitas');
                    $shipping = new Shipping();
                    $shipping->do_id        = $do->id;
                    $shipping->nomor        = $this->getGenerateCode('SH');
                    $shipping->tanggal      = date('Y-m-d');
                    $shipping->biaya_supir  = $request->get('biaya');
                    $shipping->tarif_id     = $tarif->id;
                    $shipping->tarif        = $request->get('tarif');
                    $shipping->bop          = $tarif->bop;
                    $shipping->kuantitas    = $total_kuantitas;
                    $shipping->total        = $total_kuantitas*$tarif->tarif;
                    $shipping->status       = SHIPPING_STATUS_BELUM_DITAGIH;

                    $shipping_save = $shipping->save();

                    if ( $shipping_save ) {
                        DB::commit();
                        return redirect('do')->with('sukses', 'Berhasil Menggenerate BOP untuk Transakso DO : '.$do->nomor);
                    } else {
                        DB::rollback();
                        return redirect()->back()->with('failed', 'Gagal Dalam Generate Shipping Untuk Transaksi DO : '.$do->nomor);
                    }


                } else {
                    DB::rollback();
                    return redirect()->back()->with('failed', 'Gagal Update DO Untuk Transaksi DO : '.$do->nomor);
                }

                return $image_id;

            }else{
                DB::rollback();
                return redirect()->back()->with('failed', 'Gagal Generate BOP Untuk Transaksi DO : '.$do->nomor);
            }


        }catch (Exception $e){
            DB::rollback();
        }

    }

    public function startPengiriman(DeliveryOrder $do)
    {
        $user = $do->driver;

        if(!$user) {
            return redirect()->back()->with('failed', 'Gagal dalam memulai pengiriman, data driver tidak ditemukan');
        }

        if(!$user->list_kendaraan){
            return redirect()->back()->with('failed', 'Gagal dalam memulai pengiriman, Driver belum mempunyai pegangan kendaraan');
        }

//        if($user->list_kendaraan[0]->status == 'dipakai'){
//            return redirect()->back()->with('failed', 'Gagal dalam memulai pengiriman, Kendaraan Sedang dipakai mengirim barang oleh driver');
//        }

        $datetime = date('Y-m-d H:i:s');
        $do->start_delivery_time = $datetime;
        $do->status = DO_STATUS_KIRIM;
        if($do->save()){
            return redirect()->back()->with('success', 'Berhasil Memulai Pengiriman Barang');
        }
    }

    public function simpanKonfirmasi(Request $request)
    {
        $this->validate($request, [
           'nomor'=>'required',
           'tanggal'=>'required',
           'bukti'=>'required|file'
        ]);

        $goodReceipt = new GoodReceipt();

        $goodReceipt->nomor = $request->nomor;
        $goodReceipt->shipping_id = $request->shipping_id;
        $goodReceipt->tanggal = date('Y-m-d', strtotime($request->tanggal));

        if($request->hasFile('bukti')){
            $bukti = $request->file('bukti');
            $image_id = $this->uploadImage($bukti);
        }
        $goodReceipt->bukti = $image_id;
        $goodReceipt->pecah = $request->pecah;

        if($goodReceipt->save()){
            $goodReceipt->image()->save($image_id);
            return redirect()->route('do')->with('success', 'Berhasil Menyelesaikan Delivery!');
        }
    }

    public function stopPengiriman(DeliveryOrder $do)
    {
        $generated = $this->getGenerateCode('GR');
        return view('do.good_received', ['do'=>$do, 'generate_code'=>$generated]);
    }

    public function detailDo($id)
    {
        $do = DeliveryOrder::findOrFail($id);
        return view('do.detail', compact('do'));
    }



}
