<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\User;
use Illuminate\Http\Request;
use DB;
class LaporanController extends Controller
{
    protected $data_length = 10;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function monitoring()
    {
//        select  ku.kode, ku.name, ku.username, kdo.nomor as nomor_order,
//        (select getNoDo(kdo.id)) as no_do,
//        (select name from kontrolling_indonesia_cities kic where kic.id = kt.asal_id) as asal,
//        (select name from kontrolling_indonesia_cities kic where kic.id = kt.tujuan_id) as tujuan,
//        kdo.start_delivery_time as waktu_dimulai
//        from kontrolling_users ku
//        right join kontrolling_delivery_orders kdo on kdo.driver_id = ku.id
//        right join kontrolling_shippings ks on kdo.id = ks.do_id
//        right join kontrolling_tarifs kt on ks.tarif_id = kt.id
//        where ku.type = 4 and kdo.status = 'kirim';

        $list_monitoring = DB::table('users as ku')
            ->select([
                'ku.kode', 'ku.name', 'ku.username', 'kdo.nomor as nomor_order',
                DB::raw('(select getNoDo('.DB::getTablePrefix().'kdo.id)) as no_do'),
                DB::raw('(select name from '.DB::getTablePrefix().'indonesia_cities where '.DB::getTablePrefix().'indonesia_cities.id = '.DB::getTablePrefix().'kt.asal_id) as asal'),
                DB::raw('(select name from '.DB::getTablePrefix().'indonesia_cities where '.DB::getTablePrefix().'indonesia_cities.id = '.DB::getTablePrefix().'kt.tujuan_id) as tujuan'),
                'kdo.start_delivery_time as waktu_dimulai'
            ])
            ->join('delivery_orders as kdo', 'kdo.driver_id', '=', 'ku.id')
            ->join('shippings as ks', 'ks.do_id', '=', 'kdo.id')
            ->join('tarifs as kt', 'kt.id', '=', 'ks.tarif_id');

        $list_monitoring = $list_monitoring->where([
            ['ku.type', '=', 4],
            ['kdo.status', '=', DO_STATUS_KIRIM]
        ]);

        $list_monitoring = $list_monitoring->paginate(10);

//        return $list_monitoring->get();

        return view('laporan.monitoring.index', [
            'list_data'=>$list_monitoring
        ]);

    }

    public function do()
    {
//        select kdo.nomor, kdo.tanggal, kc.nama as 'distibutor', kdo.kabupaten as 'tujuan',
//        kk.no_plat as 'no_trul',
//        (select sum(kdd.kuantitas) from kontrolling_detile_do kdd where kdd.id_do = kdo.id) as 'tonase',
//        ks.tarif as 'ongkos',
//        ((select sum(kdd.kuantitas) from kontrolling_detile_do kdd where kdd.id_do = kdo.id)*ks.tarif) as 'total'
//        from kontrolling_delivery_orders kdo
//        inner join kontrolling_clients kc on kc.id = kdo.client_id
//        inner join kontrolling_kendaraan kk on kk.id like kdo.no_kendaraan
//        inner join kontrolling_shippings ks on ks.do_id = kdo.id
//
//                -- select kdo.nomor from kontrolling_delivery_orders kdo
        $list_do = DB::table('delivery_orders as kdo')->select([
            'kdo.nomor',  'kdo.tanggal', 'kc.nama as distributor', 'kdo.kabupaten as tujuan', 'kk.no_plat as no_truk',
            DB::raw('(select sum(kdd.kuantitas) from '.DB::getTablePrefix().'detile_do kdd where kdd.id_do = '.DB::getTablePrefix().'kdo.id) as tonase'),
            'ks.tarif as ongkos',
            'ks.biaya_supir as kas_supir',
            DB::raw('((select sum(kdd.kuantitas) from '.DB::getTablePrefix().'detile_do kdd where kdd.id_do = '.DB::getTablePrefix().'kdo.id) * '.DB::getTablePrefix().'ks.tarif) as total')
        ])->join('clients as kc', 'kc.id', '=', 'kdo.client_id')
        ->join('kendaraan as kk', 'kk.no_plat', '=', 'kdo.no_kendaraan')
        ->join('shippings as ks', 'ks.do_id', '=', 'kdo.id');
//        return $list_do->get();
        return view('laporan.do.index', ['list_data'=>$list_do->paginate($this->data_length)]);
    }

    public function bop(Request $request)
    {
        if($request->has('data_length')){
            $this->data_length = $request->get('data_length');
        }
        $list_data = DB::table('users as ku')
                ->select([
                    'ku.id',
                    'ku.kode', 'ku.no_ktp', 'ku.name',
                    DB::raw('(select getTotalIncomeBop('.DB::getTablePrefix().'ku.id)) as total_income'),
                    DB::raw('(select getDoNoTransactions('.DB::getTablePrefix().'ku.id)) as do')
                ])->where('ku.type', '=', 4);

        $list_data = $list_data->paginate($this->data_length);
        return view('laporan.bop.index', compact(['list_data']));
    }

    public function bopDetile($id, Request $request)
    {

        if($request->has('data_length')){
            $this->data_length = $request->get('data_length');
        }
        $driver = User::findOrFail($id);
        /**
         * no_bop, tanggal_bop, no_shipping, biaya_supir, tarif, kuantitas, total_tarif, selisih, no_do, kota_tujuan, kuantitas, kode, name
         */
        $list_data = DB::table('users as ku')->select([
            'kbo.nomor as no_bop', 'kbo.tanggal as tanggal_bop',
            'ks.nomor as no_shipping', 'ks.tanggal as tanggal_shipping', 'ks.biaya_supir', 'ks.tarif', 'kbo.kuantitas',
            DB::raw('('.DB::getTablePrefix().'ks.tarif * '.DB::getTablePrefix().'kbo.kuantitas) as total_tarif'),
            DB::raw('(('.DB::getTablePrefix().'ks.tarif * '.DB::getTablePrefix().'kbo.kuantitas) - '.DB::getTablePrefix().'ks.biaya_supir) as selisih'),
            'kdo.nomor as no_do',  'kdo.tanggal as tanggal_do', 'kdo.kabupaten as kota_tujuan', 'kbo.kuantitas', 'ku.kode as no_do', 'ku.name as driver_name'
        ])
            ->join('biaya_operasionals as kbo', 'kbo.penerima_id', '=', 'ku.id' )
            ->join('delivery_orders as kdo', 'kdo.id' , '=', 'kbo.do_id')
            ->join('shippings as ks', 'ks.do_id', '=', 'kdo.id');

        $list_data = $list_data->where([['ku.type', '=', 4], ['kdo.status', '=', DO_STATUS_SELESAI], ['ku.id', '=', $driver->id]]);

        $list_data = $list_data->paginate($this->data_length);
//        return json_encode($driver);

        return view('laporan.bop.detile', compact(['driver', 'list_data']));
    }


    public function invoice()
    {
        $list_data = new Invoice();
        $list_data = $list_data->with('client')->select(['nomor', 'tanggal', 'client_id', DB::raw('(select getNoDoFromInvoice('.DB::getTablePrefix().'invoices.id)) as data_tagihan')]);
        $list_data = $list_data->paginate($this->data_length);

//        $data_tagihan = str_replace(chr(0), '', $list_data[0]->data_tagihan);
//        $data_tagihan = ($data_tagihan);
//        $data_tagihan = utf8_encode($data_tagihan);
//        $data_tagihan = $this->jsonDecode($data_tagihan, true);
//        $data_tagihan = $this->jsonDecode($data_tagihan[0]);
//        return $data_tagihan->no_bop;

        return view('laporan.invoice.index', compact('list_data'));
    }

    function jsonDecode($json, $assoc = false)
    {
        $ret = json_decode($json, $assoc);
        if ($error = json_last_error())
        {
            $errorReference = [
                JSON_ERROR_DEPTH => 'The maximum stack depth has been exceeded.',
                JSON_ERROR_STATE_MISMATCH => 'Invalid or malformed JSON.',
                JSON_ERROR_CTRL_CHAR => 'Control character error, possibly incorrectly encoded.',
                JSON_ERROR_SYNTAX => 'Syntax error.',
                JSON_ERROR_UTF8 => 'Malformed UTF-8 characters, possibly incorrectly encoded.',
                JSON_ERROR_RECURSION => 'One or more recursive references in the value to be encoded.',
                JSON_ERROR_INF_OR_NAN => 'One or more NAN or INF values in the value to be encoded.',
                JSON_ERROR_UNSUPPORTED_TYPE => 'A value of a type that cannot be encoded was given.',
            ];
            $errStr = isset($errorReference[$error]) ? $errorReference[$error] : "Unknown error ($error)";
            throw new \Exception("JSON decode error ($error): $errStr");
        }
        return $ret;
    }
}
