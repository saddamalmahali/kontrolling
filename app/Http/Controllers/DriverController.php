<?php

namespace App\Http\Controllers;

use App\Kendaraan;
use App\Traits\ImageTrait;
use App\User;
use Illuminate\Foundation\Console\PackageDiscoverCommand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use DB;

class DriverController extends Controller
{
    protected $data_length = 10;
    protected $order_method = 'asc';
    protected $column_order = 'name';
    use ImageTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        $list_data = new User();

        if($request->user()->type == USER_TYPE_ADMIN){
            $list_data = $list_data->where('type', '!=', USER_TYPE_OWNER);
        }

        if($request->has('data_length')){
            $this->data_length = $request->get('data_length');
        }

        if($request->has('order_by')){
            if($request->get('order_by')){
                $this->column_order = $request->get('order_by');
            }

        }

        if($request->has('order_method')){
            if($request->get('order_method')){
                $this->order_method = $request->get('order_method');
            }
        }

        if($request->has('kode_search')){
            $list_data = $list_data->where('kode', 'like', '%'.$request->get('kode_search').'%');
        }
        if($request->has('nama_search')){
            $list_data = $list_data->where('name', 'like', '%'.$request->get('nama_search').'%');
        }

        $list_data = $list_data->where('type', '=', USER_TYPE_DRIVER);

        $list_data = $list_data->orderBy($this->column_order,  $this->order_method);

        $list_data = $list_data->paginate($this->data_length);


        return view('driver.index', compact('list_data'));
    }

    public function formAdd()
    {
        $generate_code = $this->getGenerateCode('DR');
        $kendaraanNotUsed = DB::table('kendaraan_user')->pluck('kendaraan_id')->toArray();
        $list_kendaraan = Kendaraan::whereNotIn('id', $kendaraanNotUsed)->get();

        if(count($list_kendaraan) < 1) return redirect()->back()->with('success', 'Tidak Terdapat Kendaraan yang tersedia');
//        echo '<pre>';
//        return var_dump($list_kendaraan);
        return view('driver.add', compact(['generate_code', 'list_kendaraan']));
    }

    public function formEdit($id, Request $request)
    {
        $driver =  User::findOrFail($id);

        $kendaraanNotUsed = DB::table('kendaraan_user')->pluck('kendaraan_id')->toArray();
        $list_kendaraan = Kendaraan::whereNotIn('id', $kendaraanNotUsed)->get();

        if(count($list_kendaraan) < 1) return redirect()->back()->with('success', 'Tidak Terdapat Kendaraan yang tersedia');

        return view('driver.edit', compact(['driver', 'list_kendaraan']));
    }

    public function simpanData(Request $request)
    {
        $validationData = $this->validationData();
        if($request->has('id')){
            unset($validationData['username']);
            unset($validationData['email']);
            if(!$request->get('password')){
                unset($validationData['password']);
            }

        }

        $this->validate($request, $validationData);

        $data = $this->buildData($request->all());
        $data ['type'] = USER_TYPE_DRIVER;
        $data['status'] = USER_STATUS_AKTIF;

        $image_id = null;
        if($request->hasFile('foto')){
            $file_foto = $request->foto;
            $image_id = $this->uploadImage($file_foto);
        }

        $data['foto']=$image_id;

        if(auth()->user()->type == 1){
            $data['verified'] = 1;
        }


        if(isset($data['id'])){
            $data_simpan = User::findOrFail($data['id'])->update($data);
        }else{
            $data_simpan = User::create($data);
        }

        if($data_simpan){
            if($data['id']){
                $data_simpan = User::findOrFail($data['id']);
            }
//            return json_encode($data_simpan);
            // Remove Relation of Kendaraan
            if(count($data_simpan->list_kendaraan)){
                $data_simpan->list_kendaraan()->detach();
            }
            // Add Relation to Kendaraan
            $data_simpan->list_kendaraan()->attach($request->get('kendaraan'));
            return redirect()->route('driver')->with('success', 'Berhasil Menyimpan Data Driver');
        }


    }

    public function hapusData(Request $request)
    {
        $driver = User::findOrFail($request->get('id'));

        if($driver->delete()){
            return redirect()->route('driver')->with('success', 'Sukses Menghapus Data Driver');
        }
    }

    private function buildData($request){
        // Build Data Driver
        $request = array_filter($request);
        $data = array();
        foreach ($request as $key => $value){
            if($key == '_token' || $key == 'password_confirmation' || $key == 'kendaraan') continue;

            if($key == 'password') $value = Hash::make($value);

            if($key == 'tanggal_mulai_bekerja') $value = date('Y-m-d', strtotime($value));

            $data[$key] = $value;
        }

        return $data;
    }

    public function hapusKendaraan(User $driver){
        if($driver->list_kendaraan()->detach()){
            return redirect()->back()->with('success', 'Berhasil Menghapus kendaraan pada driver : '.$driver->name);
        }
    }

    public function validationData()
    {
        return [
            'name'              =>  'required|min:5',
            'no_ktp'            =>  'required|min:5',
            'kendaraan'         =>  'required',
            'provinsi'          =>  'required',
            'kabupaten'         =>  'required',
            'kecamatan'         =>  'required',
            'alamat'            =>  'required',
            'email'             =>  'required|email|unique:users',
            'username'          =>  'required|unique:users',
            'password'          =>  'required|confirmed'
        ];
    }

}
