<?php

namespace App\Http\Controllers;

use App\Client;
use App\Traits\ImageTrait;
use Illuminate\Http\Request;

class ClientController extends Controller
{

    protected $data_length = 10;
    protected $order_method = 'asc';
    protected $column_order = 'nama';
    use ImageTrait;
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $list_data = new Client();

        if($request->has('data_length')){
            $this->data_length = $request->get('data_length');
        }

        if($request->has('order_by')){
            if($request->get('order_by')){
                $this->column_order = $request->get('order_by');
            }

        }

        if($request->has('order_method')){
            if($request->get('order_method')){
                $this->order_method = $request->get('order_method');
            }
        }

        if($request->has('status_search')){
            if($request->get('status_search')){
                $list_data = $list_data->where('status', '=', $request->get('status_search'));
            }
        }

        $list_data = $list_data->orderBy($this->column_order,  $this->order_method);

        $list_data = $list_data->paginate($this->data_length);


        return view('client.index', compact('list_data'));

    }

    public function formAdd()
    {
        $generate_code = $this->getGenerateCode('CL');
        return view('client.add', compact('generate_code'));
    }

    public function formEdit($id, Request $request)
    {
        $client = Client::findOrFail($id);

        $generate_code = $this->getGenerateCode('CL');
        return view('client.edit', compact(['generate_code', 'client']));
    }

    public function simpanData(Request $request)
    {
        $minKuota = option(MIN_KUOTA);
        $maxKuota = option(MAX_KUOTA);
        $dataValidation['min_kuota'] = $minKuota;
        $dataValidation['max_kuota'] = $maxKuota;
//        return $min_kuota;
        $this->validate($request, $this->validationData($dataValidation), $this->messagesValidate());

        $client = new Client();

        if($request->has('id')){
            $client = Client::findOrFail($request->get('id'));
        }

        $client->kode           = $request->get('kode');
        $client->nama           = $request->get('nama');
        $client->alamat         = $request->get('alamat');
        $client->kuota          = $request->get('kuota');
        $client->no_rek         = $request->get('no_rek');
        $client->tipe_bank      = $request->get('tipe_bank');
        $client->email          = $request->get('email');
        $client->telepon        = $request->get('telepon');
        $client->npwp           = $request->get('npwp');
        $image_id = null;

        if($request->hasFile('foto')){
            $foto = $request->file('foto');
            $image_id = $this->uploadImage($foto);
        }

        $client->foto           = $image_id;

        if(auth()->user()->type != USER_TYPE_DRIVER){
            $client->status         = KLIEN_STATUS_AKTIF;
        }else{
            $client->status         = KLIEN_STATUS_NONAKTIF;
        }


        if($request->has('status')){
            if($request->get('status')){
                $client->status = $request->get('status');
            }
        }

        if($client->save()){
            $client->image()->save($image_id);
            return redirect()->route('client')->with('success', 'Berhasil Menyimpan Data User');
        }

    }

    public function hapusData(Request $request){
        if($request->has('id')){
            $data = Client::findOrFail($request->get('id'));

            if($data->delete()){
                return redirect()->route('client')->with('success', 'Berhasil Menghapus Data Client');
            }
        }else{
            return redirect()->route('client')->with('failed', 'Gagal Menghapus Data, Data Tidak Ditemukan');
        }

    }

    public function konfirmasiClient(Client $client)
    {
        $client->status = USER_STATUS_AKTIF;
        if($client->save()){
            return redirect()->back()->with('success', 'Berhasil Mengaktifkan Client');
        }
    }

    public function validationData($data = array())
    {
        $minKuota = isset($data['min_kuota']) ? $data['min_kuota'] : 10;
        $maxKuota = isset($data['max_kuota']) ? $data['max_kuota'] : 100;
        return [
            'nama'      =>  'required|min:5',
            'alamat'    =>  'required|min:10',
            'kuota'     =>  "required|numeric|min:{$minKuota}"
        ];
    }

    public function messagesValidate()
    {
        return [
            'kode.required'         =>'Kolom Nama Perusahaan Tidak Boleh Kosong',
            'kode.min'              =>'Kolom Nama Minimal mengandung 5 karakter',
            'kuota.required'        =>'Kolom Kuota Perusahaan Tidak Boleh Kosong',
            'kuota.min'             =>'Kolom Kuota Minimal Harus :min',
            'kuota.digit_between'   =>'Kolom Kuota Minimal Harus :min dan Maksimal :max',
            'kuota.numeric'         =>'Kolom Kuota Harus Berisikan Numerik',
            'alamat.required'       =>'Kolom Alamat Tidak Boleh Kosong',
            'alamat.min'            =>'Kolom Kuota Minimal Harus :min Karakter'
        ];
    }


}
