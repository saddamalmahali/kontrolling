<?php

namespace App\Http\Controllers;

use App\BiayaOperasional;
use App\DeliveryOrder;
use App\Kendaraan;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {

        $total_kendaraan = Kendaraan::count();
        $total_driver = User::where('type', '=', USER_TYPE_DRIVER)->count();
        $total_do = DeliveryOrder::count();
        $total_bop = BiayaOperasional::sum('biaya');

        $data['total_kendaraan'] = $total_kendaraan;
        $data['total_driver'] = $total_driver;
        $data['total_do'] = $total_do;
        $data['total_bop'] = $total_bop;
//        return ['token' => $token];
        return view('home', ['data' => $data]);
    }
}
