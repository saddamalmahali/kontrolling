<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {

        return view('setting.index');
    }

    public function simpan(Request $request)
    {
        $this->validate($request, [
            'site_name'=>'required',
            'company_name'=>'required',
            'address'=>'required',
            'kecamatan'=>'required',
            'kabupaten'=>'required',
            'provinsi'=>'required',
            'telp'=>'required',
            'email'=>'required',
        ]);

        option([
            'site_name' => $request->get('site_name'),
            'company_name' => $request->get('company_name'),
            'address' => $request->get('address'),
            'kecamatan' => $request->get('kecamatan'),
            'kabupaten' => $request->get('kabupaten'),
            'provinsi' => $request->get('provinsi'),
            'telp' => $request->get('telp'),
            'email' => $request->get('email'),
        ]);

        return redirect()->back()->with('sukses', 'Berhasil Menyimpan Setting Aplikasi');

    }
}
