<?php

namespace App\Http\Controllers;

use App\Client;
use App\Exports\DataTujuan;
use App\Imports\DataTujuanImport;
use App\Tarif;
use App\Traits\ImageTrait;
use Illuminate\Http\Request;
use DB;
class TarifController extends Controller
{
    protected $data_length = 10;
    protected $order_method = 'asc';
    protected $column_order = 'asal_id';
    use ImageTrait;
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $list_data = DB::table('tarifs as t')
            ->select([
                DB::raw('(select kic.name from '.DB::getTablePrefix().'indonesia_cities kic where kic.id='.DB::getTablePrefix().'t.asal_id) as asal'),
                DB::raw('(select kic.name from '.DB::getTablePrefix().'indonesia_cities kic where kic.id='.DB::getTablePrefix().'t.tujuan_id) as tujuan'),
                'tarif', 'bop', 'id'
            ]);

        if($request->has('data_length')){
            $this->data_length = $request->get('data_length');
        }

        if($request->has('order_by')){
            if($request->get('order_by')){
                $this->column_order = $request->get('order_by');
            }

        }

        if($request->has('order_method')){
            if($request->get('order_method')){
                $this->order_method = $request->get('order_method');
            }
        }

        if($request->has('status_search')){
            if($request->get('status_search')){
                $list_data = $list_data->where('status', '=', $request->get('status_search'));
            }
        }


        $list_data = $list_data->orderBy($this->column_order,  $this->order_method);
//
        $list_data = $list_data->paginate($this->data_length);
//
//
        return view('tarif.index', compact('list_data'));

    }

    public function formAdd()
    {
        $generate_code = $this->getGenerateCode('CL');
        $list_kota = \Indonesia::allCities();
        return view('tarif.add', compact(['generate_code', 'list_kota']));
    }

    public function formEdit($id, Request $request)
    {
        $tarif = Tarif::findOrFail($id);
        $list_kota = \Indonesia::allCities();
        $generate_code = $this->getGenerateCode('CL');
        return view('tarif.edit', compact(['generate_code', 'tarif', 'list_kota']));
    }

    public function simpanData(Request $request)
    {
        $minKuota = option(MIN_KUOTA);
        $maxKuota = option(MAX_KUOTA);
        $dataValidation['min_kuota'] = $minKuota;
        $dataValidation['max_kuota'] = $maxKuota;
//        return $min_kuota;
        $this->validate($request, $this->validationData($dataValidation), $this->messagesValidate());

        $tarif = new Tarif();

        if($request->has('id')){
            $tarif = Tarif::findOrFail($request->get('id'));
        }

        $tarif->asal_id             = $request->get('kota_asal');
        $tarif->tujuan_id           = $request->get('kota_tujuan');
        $tarif->tarif               = $request->get('tarif');
        $tarif->bop                 = $request->get('bop');


        if($tarif->save()){
            return redirect()->route('tarif')->with('success', 'Berhasil Menyimpan Data Tarif');
        }

    }

    public function hapusData(Request $request){
        if($request->has('id')){
            $data = Tarif::findOrFail($request->get('id'));

            if($data->delete()){
                return redirect()->route('tarif')->with('success', 'Berhasil Menghapus Data Client');
            }
        }else{
            return redirect()->route('tarif')->with('failed', 'Gagal Menghapus Data, Data Tidak Ditemukan');
        }

    }

    public function importData (Request $request) {
        return view('tarif.import');
    }

    public function actionImport(Request $request) {
        $this->validate($request, [
            'file_import'=>'required|file|mimes:xlsx,xls'
        ]);

        $collection = (new DataTujuanImport)->toArray($request->file_import);
        if(is_array($collection)){
            if(count($collection[0]) > 1){
                if (ob_get_level() == 0) ob_start();

                $total = count($collection[0])-1;
                echo "Total Data : ".$total.'<br />';
                ob_flush();
                flush();
                $berhasil = 0;
                $gagal = 0;
                foreach ($collection[0] as $key => $data){
                    if($key == 0) continue;
                    ob_flush();
                    flush();

                    $tarif = new Tarif();
                    $tarif->asal_id = $data[0] ? $data[0] : 0;
                    $tarif->tujuan_id = $data[1] ? $data[1] : 0;
                    $tarif->tarif = $data[2] ? $data[2] : 0;
                    $tarif->bop = $data[3] ? $data[3] : 0;
                    $save = $tarif->save();

                    if($save){
                        echo 'Data ke '.$key.' Berhasil di Import : '.json_encode($data).'<br />';
                        echo str_pad('',4096)."\n";
                        $berhasil++;
                        ob_flush();
                        flush();
                        sleep(2);
                    }else{
                        echo 'Data ke '.$key.'  Gagal diimport <br />';
                        echo str_pad('',4096)."\n";
                        $gagal++;
                        ob_flush();
                        flush();
                        sleep(2);
                    }

                }

                echo ' Import Data Selesai! sistem akan mengarahkan ke halaman sebelumnya...';
                echo str_pad('',4096)."\n";
                ob_flush();
                flush();
                ob_end_flush();

                sleep(10);
                return redirect()->back()->with('success', 'Data Berhasil diimport : '.$berhasil.', Data Gagal diimport : '.$gagal);
            }
        }

    }

    public function validationData($data = array())
    {
        $minKuota = isset($data['min_kuota']) ? $data['min_kuota'] : 10;
        $maxKuota = isset($data['max_kuota']) ? $data['max_kuota'] : 100;
        return [
            'kota_asal'         =>  'required',
            'kota_tujuan'       =>  'required',
            'tarif'             =>  "required",
            'bop'               =>  "required"
        ];
    }

    public function downloadTujuan(Request $request)
    {
        return \Excel::download(new DataTujuan, 'data_tujuan.xlsx');
    }

    public function messagesValidate()
    {
        return [
            'kode.required'         =>'Kolom Nama Perusahaan Tidak Boleh Kosong',
            'kode.min'              =>'Kolom Nama Minimal mengandung 5 karakter',
            'kuota.required'        =>'Kolom Kuota Perusahaan Tidak Boleh Kosong',
            'kuota.min'             =>'Kolom Kuota Minimal Harus :min',
            'kuota.digit_between'   =>'Kolom Kuota Minimal Harus :min dan Maksimal :max',
            'kuota.numeric'         =>'Kolom Kuota Harus Berisikan Numerik',
            'alamat.required'       =>'Kolom Alamat Tidak Boleh Kosong',
            'alamat.min'            =>'Kolom Kuota Minimal Harus :min Karakter'
        ];
    }
}
