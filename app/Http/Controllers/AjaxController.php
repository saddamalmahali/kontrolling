<?php

namespace App\Http\Controllers;

use App\Client;
use App\Kendaraan;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\User;
use DB;

class AjaxController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dataDriver(Request $request)
    {
        $query = $request->get('q');
        if(!$query) return [];
        $list_id = DB::table('kendaraan_user')->pluck('user_id')->toArray();
//return $list_id;
        $driver = User::where('type', '=', USER_TYPE_DRIVER)
                        ->where('name', 'like', "%{$query}%")
                        ->whereHas('list_kendaraan', function (Builder $query) {
                            $query->where('status', 'like', '%aktif%');
                        })
                        ->whereIn('id', $list_id)->get(['id', 'name as text']);

        return json_decode($driver);
    }

    public function dataKendaraanDriver(Request $request, User $user){
        $id_driver = $request->get('id_driver');

//        $driver = User::findOrFail($id);

        return $user->list_kendaraan;
    }

    public function dataClient(Request $request)
    {
        $query = $request->get('q');
        if(!$query) return [];

        $data_res = Client::where('status', '=', KLIEN_STATUS_AKTIF)->where('kode', 'like', "%{$query}%")
                    ->orWhere('nama', 'like', "%{$query}%")->get(['id', 'nama as text']);

        return json_encode($data_res);
    }

    public function dataKendaraan(Request $request) {
        $query = $request->get('q');
        if(!$query) return [];

        $data_res = Kendaraan::where('status', '=', 'aktif')->where('no_plat', 'like', "%{$query}%")
            ->orWhere('seri', 'like', "%{$query}%")->get(['id', 'no_plat as text']);

        return json_encode($data_res);
    }
}
