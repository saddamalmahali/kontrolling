<?php

namespace App\Http\Controllers;

use App\Kendaraan;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class KendaraanController extends Controller
{
    protected $data_length = 10;
    protected $order_method = 'asc';
    protected $column_order = 'seri';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        $list_data = new Kendaraan();


        if($request->has('data_length')){
            $this->data_length = $request->get('data_length');
        }

        if($request->has('order_by')){
            if($request->get('order_by')){
                $this->column_order = $request->get('order_by');
            }

        }

        if($request->has('order_method')){
            if($request->get('order_method')){
                $this->order_method = $request->get('order_method');
            }
        }

        if($request->has('kode_search')){
            $list_data = $list_data->where('kode', 'like', '%'.$request->get('kode_search').'%');
        }
        if($request->has('seri_search')){
            $list_data = $list_data->where('seri', 'like', '%'.$request->get('seri_search').'%');
        }
        if($request->has('no_plat_search')){
            $list_data = $list_data->where('no_plat', 'like', '%'.$request->get('no_plat_search').'%');
        }
        if($request->has('jenis_kendaraan_search')){
            $list_data = $list_data->where('jenis_kendaraan', 'like', '%'.$request->get('jenis_kendaraan_search').'%');
        }
        if($request->has('pabrikan_search')){
            $list_data = $list_data->where('pabrikan', 'like', '%'.$request->get('pabrikan_search').'%');
        }
        if($request->has('status_search')){
            $list_data = $list_data->where('status', '=', $request->get('status_search'));
        }


        $list_data = $list_data->orderBy($this->column_order,  $this->order_method);

        $list_data = $list_data->paginate($this->data_length);


        return view('kendaraan.index', compact('list_data'));
    }

    public function formAdd()
    {
        $generate_code = $this->getGenerateCode('KD');
        return view('kendaraan.add', compact('generate_code'));
    }

    public function formEdit($id, Request $request)
    {
        $kendaraan =  Kendaraan::findOrFail($id);

        return view('kendaraan.edit', compact('kendaraan'));
    }

    public function simpanData(Request $request)
    {
        $validationData = $this->validationData();

        $this->validate($request, $validationData);

        $data = $this->buildData($request->all());
        $data ['type'] = USER_TYPE_DRIVER;
        $data['status'] = USER_STATUS_AKTIF;

        if(auth()->user()->type == 1){
            $data['verified'] = 1;
        }


        if(isset($data['id'])){
            $data_simpan = Kendaraan::findOrFail($data['id'])->update($data);
        }else{
            $data_simpan = Kendaraan::create($data);
        }

        if($data_simpan){
            return redirect()->route('kendaraan')->with('success', 'Berhasil Menyimpan Data Driver');
        }


    }

    public function hapusData(Request $request)
    {
        $kendaraan = Kendaraan::findOrFail($request->get('id'));

        if($kendaraan->delete()){
            return redirect()->route('kendaraan')->with('success', 'Sukses Menghapus Data Driver');
        }
    }

    private function buildData($request){
        // Build Data Driver
        $request = array_filter($request);
        $data = array();
        foreach ($request as $key => $value){
            if($key == '_token' || $key == 'password_confirmation') continue;

            if($key == 'password') $value = Hash::make($value);

            $data[$key] = $value;
        }

        return $data;
    }

    public function validationData()
    {
        $validation = [
            'seri'              =>  'required|min:3',

            'kode'              =>  'required',
            'jenis_kendaraan'   =>  'required',
            'pabrikan'          =>  'required'
        ];

        if(!\request()->has('id')){
           $validation['no_plat'] = 'required|unique:kendaraan';
        }
        return $validation;
    }
}
