<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;

class ServicesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function read_notification($id, Request $request){
        $notif = Notification::findOrFail($id);

        if(!$notif){
            return redirect()->back()->with('failed', 'Gagal Mengambil Data Notifikasi');
        }
        $dateread = date('Y-m-d h:i:s');
        $notif->read_at = $dateread;

        if($notif->save()){
            return redirect()->back()->with('success', 'Berhasil Menyimpan Data Notifikasi');
        }
//        var_dump($notif);die();
    }
}
