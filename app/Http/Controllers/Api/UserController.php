<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function updateCurentLocation(Request $request)
    {
        \Log::info('Running Update Current Location at'. time());
        $user = $request->user();

        $user->last_position = $request->get('current_location');

        if($user->save()){
            return response()->json([
                'status'=>'success',
                'user'=>$user,
                'message'=>'Berhasil Merubah Current Location'
            ]);
        }
    }
}
