<?php

namespace App\Http\Controllers\Api;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientController extends Controller
{
    public function listClient(Request $request)
    {
        $list_client = Client::where('status', '=', KLIEN_STATUS_AKTIF)->get();
        return response()->json(['status'=>'sukses', 'list_client'=>$list_client]);
    }
}
