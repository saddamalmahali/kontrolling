<?php

namespace App\Http\Controllers\Api;

use App\Kendaraan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DriverController extends Controller
{
    public function listDriver(Request $request)
    {
        $list_driver = Kendaraan::all();
        return response()->json(['status'=>'sukses', 'list_driver'=>$list_driver]);
    }
}
