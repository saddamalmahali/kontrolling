<?php

namespace App\Http\Controllers\Api;

use App\DeliveryOrder;
use App\DetileDelivery;
use App\Traits\ImageTrait;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DoController extends Controller
{
    use ImageTrait;

    public function index() {

    }

    public function getDo(Request $request)
    {
        $user = $request->user();
        $list_do = $user->listdo()->where('status', '!=', 'selesai')->orderBy('created_at', 'desc')->get();


        return response()->json([
            'list_do'=>$list_do
        ], 200);
    }

    /**
     * @param position
     * @param do_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function startTrack(Request $request)
    {
        $position = $request->get('position');
        $do = DeliveryOrder::findOrFail($request->get('do_id'));

        $do->start_position = $position;
        $do->status = DO_STATUS_KIRIM;
        $do->start_delivery_time = date('Y-m-d H:i:s');

        if($do->save()){

            return response()->json([
                'status'=>'success',
                'user'=>$do,
                'message'=>'Berhasil Memulai Pekerjaan DO'
            ]);
        }

    }

    public function stopTrack(Request $request)
    {

        $user = $request->user();
        $position = $request->get('position');
        $tracking = $request->tracking;
        $do = DeliveryOrder::findOrFail($request->get('do_id'));

        $do->end_position = $position;
        $do->track_location = $tracking;
        $do->end_delivery_time = date('Y-m-d H:i:s');

        $do->status = 'selesai';
        if($do->save()){
//
            return response()->json([
                'status'=>'success',
                'user'=>$do,
                'message'=>'Berhasil Menyelesaikan Pekerjaan DO'
            ]);
        }

    }

    public function getNoDo(Request $request)
    {
        $no_do = $this->getGenerateCode('DO');
        $user = $request->user();
        return response()->json(['status'=>'success', 'user'=>$user, 'no_do'=>$no_do]);
    }

    public function ajukanDo(Request $request)
    {
//        return response()->json(['data'=>$request->all()]);
        $kode = $request->kode;
        $tanggal = date('Y-m-d');
        $driver = $request->user()->id;
        $no_kendaraan = $request->no_kendaraan;
        $client = $request->client;
        $penerima = $request->penerima;
        $provinsi = $request->provinsi;
        $kabupaten = $request->kabupaten;
        $kecamatan = $request->kecamatan;
        $alamat = $request->alamat;
        $address_coordinate = $request->alamat_koordinat;
        $produk = json_decode($request->produk);
        $image_id = '';

        if($request->has('bukti')){
            $bukti = $request->bukti;
            $data = explode(',', $bukti);
            $content = base64_decode($data[1]);
            $localfilename = base_path('public/tmp/'.uniqid().'.jpg');
            if(file_put_contents($localfilename, $content)){
                $image_id = $this->uploadImage2($localfilename);
            }
        }

        $do = new DeliveryOrder();
        $do->nomor = $kode;
        $do->tanggal = $tanggal;
        $do->provinsi = $provinsi;
        $do->kabupaten = $kabupaten;
        $do->kecamatan = $kecamatan;
        $do->alamat = $alamat;
        $do->address_coordinate = $address_coordinate;
        $do->nama_penerima = $penerima;
        $do->client_id = $client;
        // Total Kuantitas Belum dimasukan
        $driver = User::findOrFail($driver);
        $do->nama_driver = $driver->name;
        $do->driver_id = $driver->id;
        $do->no_kendaraan = $no_kendaraan;
        if($image_id){
            $do->bukti = $image_id;
        }

//        if($request->user()->type != USER_TYPE_DRIVER){
//            $do->approved_by = auth()->user()->id;
//            $do->approved_date = date('Y-m-d');
//            $do->status = 'aktif';
//        }

        if($save_do = $do->save()){
            foreach ($produk as $index => $value){
                if(!$value) continue;
                $detile = new DetileDelivery();
                $detile->id_do      = $do->id;
                $detile->kode       = $value->kode;
                $detile->nama       = $value->nama;
                $detile->kuantitas  = $value->kuantitas;
                $detile->save();
            }

            return response()->json(['status'=> 'success', 'data'=>$do, 'message'=>'Berhasil Mengajukan DO'], 200);
        }

    }

}
