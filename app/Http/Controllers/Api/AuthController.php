<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Create User
     *
     * @param [string] name
     * @param [string] email
     * @param [string] password
     * @param [string] password_confirmation
     * @param [string] message
     *
     */
    public function signup(Request $request){

    }

    /**
     * Login User and create token
     *
     * @param [string] email
     * @param [string] password
     * @param [boolean] remember_me
     * @param [string] access_token
     * @param [string] token_type
     * @param [string]  expires_at
     *
     */
    public function login(Request $request){
        $request->validate([
           'username'=>'required|string',
           'password'=>'required|string',
           'remember_me'=>'boolean'
        ]);

        $credentials = request(['username', 'password']);
        $credentials['type']= USER_TYPE_DRIVER;
        if(!Auth::attempt($credentials))
            return response()->json([
                'status'=>401,
                'message'=>'Data Autentikasi Salah'
            ], 200);

        $user = $request->user();

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;

        if($request->remember_me)
            $token->expires_at = Carbon::now()->addWeek(1);

        $token->save();

        return response()->json([
            'status'=>200,
            'access_token'=>$tokenResult->accessToken,
            'token_type'=> 'Bearer',
            'data_user'=>$user,
            'expires_at'=> Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }

    /**
     * Logout User (Revoke the token)
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'status'=>200,
            'message'=>'Successfully Logged out'
        ]);
    }

    /**
     * Get the authenticated User
     * @return [json] User Object
     */
    public function user(Request $request)
    {
        $user = $request->user();
        return response()->json(['user'=>$user]);
    }

}
