<?php

namespace App\Http\Controllers\Api;

use App\DeliveryOrder;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class LaporanController extends Controller
{
    public function pengiriman(Request $request)
    {
        $user = $request->user();
        $list_do = User::getShippingList($user->id, DO_STATUS_SELESAI);

        return response()->json(['status'=>'sukses', 'data'=>$list_do]);
    }
}
