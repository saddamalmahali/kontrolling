<?php

namespace App\Http\Controllers\Api;

use App\DeliveryOrder;
use App\DetileDelivery;
use App\GoodReceipt;
use App\Traits\ImageTrait;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GrController extends Controller
{
    use ImageTrait;
    public function getNoGr()
    {
        $no_gr = $this->getGenerateCode('GR');

        return response()->json(['status'=>'success', 'no_gr'=>strtoupper($no_gr)]);
    }

    public function simpanGr(Request $request)
    {

        $shipping_id    =   $request->shipping_id;
        $nomor          =   $request->nomor;
        $pecah          =   $request->pecah;
        $image_id       =   '';
        if($request->has('bukti')){
            $bukti = $request->bukti;
            $data = explode(',', $bukti);
            $content = base64_decode($data[1]);
            $localfilename = base_path('public/tmp/'.uniqid().'.jpg');
            if(file_put_contents($localfilename, $content)){
                $image_id = $this->uploadImage2($localfilename);
            }
        }

        $gr = new GoodReceipt();
        $gr->shipping_id    =   $shipping_id;
        $gr->nomor          =   $nomor;
        $gr->tanggal        =   date('Y-m-d');
        $gr->bukti          =   $image_id;
        $gr->pecah          =   $pecah;

        if($gr->save()){
            $user = $request->user();
            $position = $request->get('position');
            $tracking = $request->tracking;
            $do = DeliveryOrder::findOrFail($request->get('do_id'));

            $do->end_position = $position;
            $do->track_location = $tracking;
            $do->end_delivery_time = date('Y-m-d H:i:s');

            $do->status = 'selesai';
            if($do->save()){
//
                return response()->json([
                    'status'=>'success',
                    'user'=>$do,
                    'message'=>'Berhasil Menyelesaikan Pekerjaan DO'
                ]);
            }
        }


    }
}
