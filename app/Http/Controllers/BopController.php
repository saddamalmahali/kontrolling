<?php

namespace App\Http\Controllers;

use App\BiayaOperasional;
use App\Traits\ImageTrait;
use Illuminate\Http\Request;

class BopController extends Controller
{
    use ImageTrait;

    protected $data_length = 10;
    protected $order_method = 'asc';
    protected $column_order = 'nomor';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $list_data = new BiayaOperasional();

        if(auth()->user()->type == USER_TYPE_DRIVER){
            $list_data = $list_data->where('penerima_id', '=', auth()->user()->id);
        }

        if($request->has('data_length')){
            $this->data_length = $request->get('data_length');
        }

        if($request->has('order_by')){
            if($request->get('order_by')){
                $this->column_order = $request->get('order_by');
            }
        }

        if($request->has('order_method')){
            if($request->get('order_method')){
                $this->order_method = $request->get('order_method');
            }
        }

        /** Filtering Search */


        /** End Of Filtering Search */


        $list_data = $list_data->orderBy($this->column_order,  $this->order_method);

        $list_data = $list_data->paginate($this->data_length);


        return view('bop.index', compact('list_data'));
    }

    public function addInvoice(Request $request)
    {
        return $request->all();
//        $this->validate($request, [
//            'bop'=>'required|array'
//        ]);
    }
}
