<?php

namespace App\Http\Middleware;

use App\DeliveryOrder;
use App\User;
use Closure;

class MobileAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check()){
            $user = auth()->user();
            $list_do = DeliveryOrder::where('driver_id', '=', $user->id)->where('status', '=', DO_STATUS_AKTIF)->get();
            $user->list_do = $list_do;
            $user->shippings = User::getShippingList($user->id);
            $request->merge(array("user" => $user));
        }
        return $next($request);
    }
}
