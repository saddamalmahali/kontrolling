<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Cmgmyr\Messenger\Traits\Messagable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable, Messagable, SoftDeletes;



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username',
        'kode', 'type', 'verified', 'no_ktp', 'provinsi',
        'kabupaten', 'kecamatan', 'alamat', 'foto', 'status',
        'tanggal_mulai_bekerja', 'status_karyawan', 'no_rek', 'tipe_bank',
        'telepon', 'jabatan', 'asuransi', ''
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $with = ['list_kendaraan'];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];



    public function verifyUser()
    {
        return $this->hasOne('App\VerifyUser');
    }

    public function notifications()
    {
        return $this->morphMany('App\Notification', 'notifiable');
    }

    public function listdo(){
        return $this->hasMany('App\DeliveryOrder', 'driver_id', 'id');
    }

    public function list_kendaraan()
    {
        return $this->belongsToMany('App\Kendaraan');
    }

    static function getShippingList($driver_id = 0, $status = 'aktif')
    {
        if(!$driver_id) return [];
//        select ks.nomor, ks.tanggal,
//        (select name from kontrolling_users ku where ku.id = kdo.driver_id) as driver,
//        ks.total, ks.tarif, ks.kuantitas, ks.biaya_supir,
//        (select name from kontrolling_indonesia_cities kic where kic.id = kt.asal_id) as asal,
//        (select name from kontrolling_indonesia_cities kic where kic.id = kt.tujuan_id) as tujuan
//        from kontrolling_shippings ks
//        inner join kontrolling_tarifs kt on kt.id = ks.tarif_id
//        inner join kontrolling_delivery_orders kdo on kdo.id=ks.do_id
//        where kdo.driver_id = 5
        $shipping = DB::table('shippings as ks')
                ->join('tarifs as kt', 'kt.id', '=', 'ks.tarif_id')
                ->join('delivery_orders as kdo', 'kdo.id', '=', 'ks.do_id')
                ->join('good_receipts as kgr', 'kgr.shipping_id', '=', 'ks.id')
                ->select([
                    DB::raw('UPPER('.DB::getTablePrefix().'ks.nomor) as nomor'), 'ks.tanggal',
                    DB::raw('UPPER('.DB::getTablePrefix().'kdo.nomor) as nomor_do'),
                    'kdo.nama_penerima', 'kgr.nomor as nomor_gr',
                    DB::raw('(select name from '.DB::getTablePrefix().'users ku where ku.id = '.DB::getTablePrefix().'kdo.driver_id) as driver'),
                    'ks.total', 'ks.tarif', 'ks.kuantitas', 'ks.biaya_supir',
                    DB::raw('(select name from '.DB::getTablePrefix().'indonesia_cities kic where kic.id = '.DB::getTablePrefix().'kt.asal_id) as asal'),
                    DB::raw('(select name from '.DB::getTablePrefix().'indonesia_cities kic where kic.id = '.DB::getTablePrefix().'kt.tujuan_id) as tujuan'),
                ]);

        $shipping = $shipping->where('kdo.status', $status);
        $shipping = $shipping->where('kdo.driver_id', '=', $driver_id);
        $shipping = $shipping->orderBy('kdo.created_at', 'desc');
        return $shipping->get();
    }
}
