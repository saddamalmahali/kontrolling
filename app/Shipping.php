<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    protected $fillable = [
        'do_id', 'nomor', 'tanggal', 'tarif_id', 'biaya_supir', 'status', 'total', 'kuantitas'
    ];

//    protected $with =['tarif'];

    public function do()
    {
        return $this->hasOne('App\DeliveryOrder', 'id', 'do_id');
    }



}
