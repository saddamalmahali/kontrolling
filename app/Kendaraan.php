<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kendaraan extends Model
{
    // STNK, KIR, BPKB, NO MESIN, NO RANGKA, TAHUN PEMBUATAN
    protected $table = 'kendaraan';
    protected $fillable = [
        'kode', 'seri', 'no_plat', 'jenis_kendaraan',
        'pabrikan', 'status', 'stnk', 'kir', 'bpkb', 'no_mesin', 'no_rangka', 'tahun_buat'
    ];

    public function driver()
    {
        return $this->belongsToMany('App\User');
    }
}
