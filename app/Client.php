<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'clients';
    protected $fillable = [
        'nama', 'kode', 'alamat', 'kuota', 'telepon', 'email', 'npwp', 'no_rek', 'tipe_bank', 'foto', 'jatuh_tempo'
    ];

    public function image()
    {
        return $this->morphOne('App\Image', 'imageable');
    }
}
