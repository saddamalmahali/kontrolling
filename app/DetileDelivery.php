<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetileDelivery extends Model
{
    protected $table = 'detile_do';

    protected $fillable = [
      'kode', 'nama', 'kuantitas'
    ];


}
