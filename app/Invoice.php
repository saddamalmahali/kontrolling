<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'nomor', 'penandatangan', 'perihal', 'tanggal', 'status', 'client_id'
    ];

    public function bop()
    {
        return $this->belongsToMany('App\BiayaOperasional');
    }

    public function client()
    {
        return $this->hasOne('App\Client', 'id', 'client_id');
    }
}
