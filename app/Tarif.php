<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarif extends Model
{
    protected $fillable =[
      'asal_id', 'tujuan_id', 'tarif', 'bop'
    ];


}
