<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BiayaOperasional extends Model
{

    protected $fillable = [
        'nomor', 'do_id', 'tanggal', 'penerima_id', 'kuantitas',
        'satuan', 'biaya_satuan', 'total', 'jenis_pembayaran', 'status', 'bukti'
    ];

    protected $with = ['image'];

    public function do()
    {
        return $this->hasOne('App\DeliveryOrder', 'id', 'do_id');
    }

    public function penerima()
    {
        return $this->hasOne('App\User', 'id', 'penerima_id');
    }

    public function image() {
        return $this->morphOne('App\Image', 'imageable');
    }

    public function invoice(){
        return $this->belongsToMany('App\Invoice');
    }
}
