<?php

namespace App\Imports;

use App\Tarif;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;

class DataTujuanImport implements ToModel
{
    use Importable;
    public function model(array $row)
    {
        return new Tarif([
            'asal_id'=>(int) $row[0],
            'tujuan_id'=>(int) $row[1],
            'tarif'=>(int) $row[2],
            'bop'=>(int) $row[3]
        ]);
    }
}
