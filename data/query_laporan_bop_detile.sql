select
#Seksi DO
kbo.nomor as no_bop,  kbo.tanggal as tanggal_bop,
#Seksi Tarif
ks.nomor as no_shipping, ks.biaya_supir, ks.tarif,
(ks.tarif*kbo.kuantitas) as total_tarif, ((ks.tarif*kbo.kuantitas) - ks.biaya_supir) as selisih,
#Seksi DO
kdo.nomor as no_do, kdo.kabupaten as kota_tujuan, kdo.kuantitas,
ku.kode, ku.name
from kontrolling_users ku
right join kontrolling_biaya_operasionals kbo on kbo.penerima_id = ku.id
right join kontrolling_delivery_orders kdo on kdo.id = kbo.do_id
right join kontrolling_shippings ks on ks.do_id = kdo.id

where ku.type = 4;
