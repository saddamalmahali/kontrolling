select  ku.kode, ku.name, ku.username, kdo.nomor as nomor_order,
(select getNoDo(kdo.id)) as no_do,
(select name from kontrolling_indonesia_cities kic where kic.id = kt.asal_id) as asal,
(select name from kontrolling_indonesia_cities kic where kic.id = kt.tujuan_id) as tujuan,
kdo.start_delivery_time as waktu_dimulai
from kontrolling_users ku
right join kontrolling_delivery_orders kdo on kdo.driver_id = ku.id
right join kontrolling_shippings ks on kdo.id = ks.do_id
right join kontrolling_tarifs kt on ks.tarif_id = kt.id
where ku.type = 4 and kdo.status = 'kirim';
