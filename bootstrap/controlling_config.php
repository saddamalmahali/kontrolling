<?php

//Unit Setting
define('UNIT_SATUAN_KG',            'kg');
define('UNIT_SATUAN_TON',           'ton');


// Type user
define('USER_TYPE_OWNER',           0);
define('USER_TYPE_ADMIN',           1);
define('USER_TYPE_KOORDINATOR',     2);
define('USER_TYPE_PENGURUS',        3);
define('USER_TYPE_DRIVER',          4);

// User Status
define('USER_STATUS_AKTIF', 'aktif');
define('USER_STATUS_NONAKTIF', 'nonaktif');
define('USER_STATUS_BLOKIR', 'blokir');

// Type Notification
define('DO_ADD',                    0);
define('DO_SENDING_START',          1);
define('DO_SENDING_STOP',           2);
define('DO_SENDING_FINISH',         3);

// Notifialble Type
define('NOTIFIABLE_USER',           'App\User');


// Kuota Minimal & Maksimal
define('MIN_KUOTA',                 'min_kuota');
define('MAX_KUOTA',                 'min_kuota');

define('KLIEN_STATUS_AKTIF',        'aktif');
define('KLIEN_STATUS_NONAKTIF',     'nonaktif');
define('KLIEN_STATUS_BLOKIR',       'blokir');

//do
define('DO_STATUS_AKTIF',           'aktif');
define('DO_STATUS_PROSES',          'proses');
define('DO_STATUS_SELESAI',         'selesai');
define('DO_STATUS_KIRIM',           'kirim');

//BOP
define('BOP_STATUS_KIRIM',          'kirim');
define('BOP_STATUS_PENDING',        'pending');
define('BOP_JENIS_PEMBAYARAN_CASH', 'cash');
define('BOP_JENIS_PEMBAYARAN_LOAN', 'loan');

//Jenis Kendaraan
define('JENIS_KENDARAAN_TRUK',      'Truk');
define('JENIS_KENDARAAN_FUSO',      'Fuso');
define('JENIS_KENDARAAN_PICKUP',    'Pickup');


// Shipping
define('SHIPPING_STATUS_DITAGIHKAN',    'ditagihkan');
define('SHIPPING_STATUS_BELUM_DITAGIH', 'belum_ditagih');


