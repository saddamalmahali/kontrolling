<?php
return [
    'type_member'=>[
        USER_TYPE_OWNER           => 'Owner',
        USER_TYPE_ADMIN           => 'Admin',
        USER_TYPE_KOORDINATOR     => 'Koordinator',
        USER_TYPE_PENGURUS        => 'Pengurus',
        USER_TYPE_DRIVER          => 'Driver'

    ]
];