$('#ui-view').ajaxLoad();

$(document).ajaxComplete(function() {
    Pace.restart()

});

$(function(){

    $('.mask_money').mask('000.000.000.000.000', {reverse: true});
    $('.mask_npwp').mask('00.000.000.0-000.000', {reverse: true});

    $('#form_bop').submit(function(){
        $('.mask_money').unmask();
        // $(this).submit();
    });

    $('#date_picker').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'dd-mm-yyyy',
        modal: true
    });

    $('.select_kendaraan').on('change', function () {
        const kendaraan = $(this).find(':selected').data('kendaraan');
        if(kendaraan){
            $('#kendaraan_pabrikan').val(kendaraan.pabrikan ? kendaraan.pabrikan.toUpperCase() : '-');
            $('#kendaraan_no_seri').val(kendaraan.seri ? kendaraan.seri.toUpperCase() : '-');
            $('#kendaraan_no_mesin').val(kendaraan.no_mesin ? kendaraan.no_mesin.toUpperCase() : '-');
            $('#kendaraan_no_rangka').val(kendaraan.no_rangka ? kendaraan.no_rangka.toUpperCase() : '-');
            $('.info_kendaraan').removeClass('d-none');
        }else{
            $('.info_kendaraan').addClass('d-none');
        }
    });

    $('form').submit(function(){
        $('.mask_money').unmask();
        $('.mask_npwp').unmask();
        // $(this).submit();
    });
    $('.select2_search').select2();

    var data_select_asal = $('.select2_search_asal').data('select');

    if(data_select_asal){
        $('.select2_search_asal').select2();
        $('.select2_search_asal').val(data_select_asal).trigger('change');

    }else{
        $('.select2_search_asal').select2();
    }


    var data_select_tujuan = $('.select2_search_tujuan').data('select');

    if(data_select_tujuan){
        $('.select2_search_tujuan').select2();
        $('.select2_search_tujuan').val(data_select_tujuan).trigger('change');

    }else{
        $('.select2_search_tujuan').select2();
    }

    $('body').delegate('#tarif_id', 'change', function(){
        var data_tarif = $(this).find(':selected').data('tarif');
        if(data_tarif.tarif > 0){
            $('#tarif').val(data_tarif.tarif);
            $('#tarif').trigger('keyup');
        }else{
            $('#tarif').val(0);
            $('#tarif').trigger('keyup');
        }
    });

    $('body').delegate('#tarif', 'keyup', function(){
        var total_kuantitas = parseInt($('#kuantitas_barang').val());
        var tarif_convert = $(this).val();
        tarif_convert = tarif_convert.replace('.', '');
        console.log('Konvert Tarif', tarif_convert);
        var tarif = parseInt(tarif_convert);
        var total_biaya = total_kuantitas*tarif;
        if((total_kuantitas > 0) && (tarif > 0)){
            $('#info_pemasukan').show(500);
        }
        var total_formated = 'Rp. '+total_biaya.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $('#total_biaya').text(total_formated);
    });


});
