var ModulKaryawan = function() {

    var initTableKaryawan =  function () {

        gridTable($('data_table_karyawan'),false);
    }

    var gridTable = function(el, action=false, target='') {
        var url     = el.data('url');
        var grid    = new Datatable();
        var tgt     = ( target!="" ? target : [ -1, 0 ] );

        grid.init({
            src: el,
            onSuccess: function(grid) {},
            onError: function(grid) {},
            dataTable: {
                "aLengthMenu": [
                    [10, 20, 50, 100, -1],
                    [10, 20, 50, 100, "All"]                        // change per page values here
                ],
                "iDisplayLength": 10,                               // default record count per page
                "bServerSide": true,                                // server side processing
                "sAjaxSource": url,       // ajax source
                "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': tgt }
                ]
            }
        });

        if( action ){
            gridExport( grid, '.table-export-excel' );
        }
    }

    return {
        init: function(){
            initTableKaryawan();
        }
    }
}();

// export default ModulKaryawan;