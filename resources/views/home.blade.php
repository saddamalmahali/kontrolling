@extends('layouts.admin.main')

@section('content')
    <?php
        $app_cfg = config('kontrollingapp.type_member');
    ?>
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-6 col-lg-3">
                <div class="card">
                    <div class="card-body p-3 d-flex align-items-center">
                        <i class="fa fa-car bg-primary p-3 font-2xl mr-3"></i>
                        <div>
                            <div class="text-value-sm text-primary">{{$data['total_kendaraan']}}</div>
                            <div class="text-muted text-uppercase font-weight-bold small">Total Kendaraan</div>
                        </div>
                    </div>
                    <div class="card-footer px-3 py-2">
                        <a class="btn-block text-muted d-flex justify-content-between align-items-center" href="{{route('kendaraan')}}">
                            <span class="small font-weight-bold">View More</span>
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-6 col-lg-3">
                <div class="card">
                    <div class="card-body p-3 d-flex align-items-center">
                        <i class="fa fa-address-card bg-info p-3 font-2xl mr-3"></i>
                        <div>
                            <div class="text-value-sm text-info">{{$data['total_driver']}}</div>
                            <div class="text-muted text-uppercase font-weight-bold small">Total Driver</div>
                        </div>
                    </div>
                    <div class="card-footer px-3 py-2">
                        <a class="btn-block text-muted d-flex justify-content-between align-items-center" href="{{route('driver')}}">
                            <span class="small font-weight-bold">View More</span>
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            @if(auth()->user()->type != USER_TYPE_DRIVER)
            <div class="col-6 col-lg-3">
                <div class="card">
                    <div class="card-body p-3 d-flex align-items-center">
                        <i class="fa fa-cubes bg-warning p-3 font-2xl mr-3"></i>
                        <div>
                            <div class="text-value-sm text-warning">{{$data['total_do']}}</div>
                            <div class="text-muted text-uppercase font-weight-bold small">Total DO</div>
                        </div>
                    </div>
                    <div class="card-footer px-3 py-2">
                        <a class="btn-block text-muted d-flex justify-content-between align-items-center" href="{{route('do')}}">
                            <span class="small font-weight-bold">View More</span>
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-6 col-lg-3">
                <div class="card">
                    <div class="card-body p-3 d-flex align-items-center">
                        <i class="fa fa-money bg-danger p-3 font-2xl mr-3"></i>
                        <div>
                            <div class="text-value-sm text-danger">Rp. {{number_format($data['total_bop'])}}</div>
                            <div class="text-muted text-uppercase font-weight-bold small">Total BOP</div>
                        </div>
                    </div>
                    <div class="card-footer px-3 py-2">
                        <a class="btn-block text-muted d-flex justify-content-between align-items-center" href="#">
                            <span class="small font-weight-bold">View More</span>
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
                @endif
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card text-white bg-primary text-center">
                    <div class="card-body">
                        <blockquote class="card-bodyquote">
                            <p>
                                <h3>Selamat Datang <strong>{{auth()->user()->name}}</strong>!</h3>
                            </p>
                            <p>

                            </p>
                        </blockquote>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@section('breadcrumb')
    {{ Breadcrumbs::render('home') }}
@endsection
