@extends('layouts.admin.main')

@section('content')
    <?php
    $app_cfg = config('kontrollingapp.type_member');
    ?>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Laporan Delivery Order
                                <div class="float-right"></div>
                            </h5>
                        </div>

                        <div class="card-body">

                            @if (session('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('success') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            @if (session('failed'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('failed') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            <form action="" id="form_option_karyawan">
                                <div class="row">

                                    <div class="col-md-3 mb-2">
                                        <div class="form-group row mb-0">
                                            <label for="data_length" class="col-md-3 col-form-label-sm mr-0 pr-0">Tampil :</label>
                                            <div class="col-md-9 ml-0 pl-0">
                                                <select name="data_length" onchange="document.getElementById('form_option_karyawan').submit()" class="form-control form-control-sm" id="data_length">
                                                    <option value="10" @if(request()->has('data_length')) {{request('data_length') == 10 ? 'selected' : ''}} @endif>10</option>
                                                    <option value="20" @if(request()->has('data_length')) {{request('data_length') == 20 ? 'selected' : ''}} @endif>20</option>
                                                    <option value="50" @if(request()->has('data_length')) {{request('data_length') == 50 ? 'selected' : ''}} @endif>50</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 mb-2">
                                        <div class="form-group row mb-0">
                                            <label for="data_length" class="col-md-3 col-form-label-sm mr-0 pr-0">Order :</label>
                                            <div class="col-md-9 ml-0 pl-0">
                                                <select name="order_by" onchange="document.getElementById('form_option_karyawan').submit()" class="form-control form-control-sm" id="data_length">
                                                    <option value="">Pilih Kolom</option>
                                                    <option value="email" @if(request()->has('order_by')) {{request('order_by') == 'email' ? 'selected' : ''}} @endif>Email</option>
                                                    <option value="name" @if(request()->has('order_by')) {{request('order_by') == 'name' ? 'selected' : ''}} @endif>Nama</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <?php
                                                $noUrut = ($list_data->currentpage()-1)* $list_data->perpage() + 1;


                                            ?>
                                            <table class="table table-bordered table-fix dataTable" id="data_table_karyawan">
                                                <thead>
                                                <tr>
                                                    <th class="text-center text-uppercase" style="min-width: 80px;">No</th>
                                                    <th class="text-center text-uppercase" style="min-width: 150px;">Nomor</th>
                                                    <th class="text-center text-uppercase" style="min-width: 200px;">Tanggal</th>
                                                    <th class="text-center text-uppercase" style="min-width: 200px;">Klien</th>
                                                    <th class="text-center text-uppercase" style="min-width: 200px;">BOP</th>
                                                    <th class="text-center text-uppercase" style="min-width: 200px;">Shipping</th>
                                                    <th class="text-center text-uppercase" style="min-width: 200px;">Total</th>
                                                    <th class="text-center text-uppercase" style="min-width: 100px;">Opsi</th>
                                                </tr>
                                                <tr>
                                                    <form type="GET">
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                            <center>
                                                                <button type="submit" class="btn btn-primary btn-sm btn-block mb-1">Search</button>
                                                                <a href="{{route('bop')}}" class="btn btn-danger btn-block btn-sm">Reset</a>
                                                            </center>
                                                        </td>
                                                    </form>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                $noUrut = ($list_data->currentpage()-1)* $list_data->perpage() + 1;


//                                                        return $data_tagihan->no_bop;
                                                ?>
                                                @forelse($list_data as $data)
                                                    <?php
                                                        $data_tagihan = str_replace(chr(0), '', $data->data_tagihan);
                                                        $data_tagihan = utf8_encode($data_tagihan);
                                                        $data_tagihan = json_decode($data_tagihan, true);
                                                        $data_tagihan = json_decode($data_tagihan[0]);
                                                    ?>
                                                    <tr>
                                                        <td class="text-center">{{$noUrut}}</td>
                                                        <td class="text-center">{{$data->nomor}}</td>
                                                        <td class="text-center">{{date('d-m-Y', strtotime($data->tanggal))}}</td>
                                                        <td class="text-center">{{$data->client->nama}}</td>
                                                        <td class="text-center">{{$data_tagihan->no_bop}}</td>
                                                        <td class="text-center">{{$data_tagihan->no_shipping}}</td>
                                                        <td class="text-right">Rp. {{number_format($data_tagihan->total_tagihan, 0, ',', '.')}}</td>
                                                        <td class="text-center"></td>
                                                    </tr>
                                                    <?php $noUrut++; ?>
                                                @empty
                                                    <tr>
                                                        <td colspan="8"><center>Tidak Terdapat Data Invoice</center></td>
                                                    </tr>
                                                @endforelse

                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="float-left">Total Record : {{$list_data->total()}} Data Delivery Order</div>
                                                <div class="float-right">{{$list_data->appends($_GET)->links()}}</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('breadcrumb')
    {{ Breadcrumbs::render('laporan.invoice') }}
@endsection
