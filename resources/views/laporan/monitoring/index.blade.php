@extends('layouts.admin.main')

@section('content')
    <?php
    $app_cfg = config('kontrollingapp.type_member');

    ?>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Laporan Monitoring Driver
                                <div class="float-right"></div>
                            </h5>
                        </div>

                        <div class="card-body">
                            @if (session('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('success') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            @if (session('failed'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('failed') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <?php
                                        $noUrut = ($list_data->currentpage()-1)* $list_data->perpage() + 1;


                                        ?>
                                        <table class="table table-bordered table-fix dataTable" id="data_table_karyawan">
                                            <thead class="table-primary">
                                            <tr>
                                                <th class="text-center text-uppercase" style="width: 8%;">No</th>
                                                <th class="text-center text-uppercase" style="width: 10%;">Nama Driver</th>
                                                <th class="text-center text-uppercase" style="width: 15%;">Nomor Order</th>
                                                <th class="text-center text-uppercase" style="width: 20%;">Nomor DO</th>
                                                <th class="text-center text-uppercase" style="width: 15%;">Kota Asal</th>
                                                <th class="text-center text-uppercase" style="width: 15%;">Kota Tujuan</th>
                                                <th class="text-center text-uppercase">Waktu</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @forelse($list_data as $data)
                                                    <tr>
                                                        <td>{{$noUrut}}</td>
                                                        <td>{{$data->name}}</td>
                                                        <td>{{$data->nomor_order}}</td>
                                                        <td>{{$data->no_do}}</td>
                                                        <td>{{$data->asal}}</td>
                                                        <td>{{$data->tujuan}}</td>
                                                        <td>{{\Carbon\Carbon::parse($data->waktu_dimulai)->diffForHumans()}}</td>

                                                    </tr>
                                                    <?php $noUrut++; ?>
                                                @empty
                                                    <tr>
                                                        <td colspan="8" class="text-center">Tidak Terdapat Data Order yang Aktif</td>
                                                    </tr>
                                                @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="float-left">Total Record : {{$list_data->total()}} Data Delivery Order</div>
                                            <div class="float-right">{{$list_data->appends($_GET)->links()}}</div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('breadcrumb')
    {{ Breadcrumbs::render('laporan.monitoring') }}
@endsection
