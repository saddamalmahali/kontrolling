@extends('layouts.admin.main')

@section('content')
    <?php
    $app_cfg = config('kontrollingapp.type_member');
    ?>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Laporan Akumulasi BOP Driver
                                <div class="float-right"></div>
                            </h5>
                        </div>

                        <div class="card-body">

                            @if (session('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('success') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            @if (session('failed'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('failed') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            <form action="" id="form_option_karyawan">
                                <div class="row">

                                    <div class="col-md-3 mb-2">
                                        <div class="form-group row mb-0">
                                            <label for="data_length" class="col-md-3 col-form-label-sm mr-0 pr-0">Tampil :</label>
                                            <div class="col-md-9 ml-0 pl-0">
                                                <select name="data_length" onchange="document.getElementById('form_option_karyawan').submit()" class="form-control form-control-sm" id="data_length">
                                                    <option value="10" @if(request()->has('data_length')) {{request('data_length') == 10 ? 'selected' : ''}} @endif>10</option>
                                                    <option value="20" @if(request()->has('data_length')) {{request('data_length') == 20 ? 'selected' : ''}} @endif>20</option>
                                                    <option value="50" @if(request()->has('data_length')) {{request('data_length') == 50 ? 'selected' : ''}} @endif>50</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 mb-2">
                                        <div class="form-group row mb-0">
                                            <label for="data_length" class="col-md-3 col-form-label-sm mr-0 pr-0">Order :</label>
                                            <div class="col-md-9 ml-0 pl-0">
                                                <select name="order_by" onchange="document.getElementById('form_option_karyawan').submit()" class="form-control form-control-sm" id="data_length">
                                                    <option value="">Pilih Kolom</option>
                                                    <option value="email" @if(request()->has('order_by')) {{request('order_by') == 'email' ? 'selected' : ''}} @endif>Email</option>
                                                    <option value="name" @if(request()->has('order_by')) {{request('order_by') == 'name' ? 'selected' : ''}} @endif>Nama</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <?php
                                            $noUrut = ($list_data->currentpage()-1)* $list_data->perpage() + 1;


                                        ?>
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-fix dataTable" id="data_table_karyawan">
                                                <thead>
                                                <tr>
                                                    <th class="text-center text-uppercase" style="min-width: 80px;">No</th>
                                                    <th class="text-center text-uppercase" style="min-width: 200px;">Kode Driver</th>
                                                    <th class="text-center text-uppercase" style="min-width: 200px;">Nama Lengkap</th>
                                                    <th class="text-center text-uppercase" style="min-width: 200px;">Total BOP</th>
                                                    <th class="text-center text-uppercase" style="min-width: 200px;">No DO</th>
                                                    <th class="text-center text-uppercase" style="min-width: 100px;">Opsi</th>
                                                </tr>
                                                <tr>
                                                    <form type="GET">

                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                            <center>
                                                                <button type="submit" class="btn btn-primary btn-sm btn-block mb-1">Search</button>
                                                                <a href="{{route('bop')}}" class="btn btn-danger btn-block btn-sm">Reset</a>
                                                            </center>
                                                        </td>
                                                    </form>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @forelse($list_data as $data)
                                                    <tr>
                                                        <td class="text-center">{{$noUrut}}</td>
                                                        <td class="text-center">{{strtoupper($data->kode)}}</td>
                                                        <td><a href="{{route('laporan.bop.detile', ['id'=>$data->id])}}"><i class="fa fa-user"></i>&nbsp;&nbsp;{{strtoupper($data->name)}}</a></td>

                                                        <td class="text-right">Rp. {{number_format($data->total_income, 0, ',', '.')}}</td>
                                                        <td class="text-right"><strong>{{$data->do ? $data->do : '-'}}</strong></td>
                                                        <td></td>
                                                    </tr>
                                                    <?php $noUrut++; ?>
                                                @empty
                                                    <tr>
                                                        <td colspan="6"><center>Tidak Terdapat Data Delivery Order</center></td>
                                                    </tr>
                                                @endforelse
                                                @if($list_data->total() > 0)
                                                    <tr>
                                                        <td class="text-right" colspan="3"><strong>TOTAL</strong></td>
                                                        <td class="text-right"><strong>Rp. {{number_format($list_data->sum('total_income'), 0, ',', '.')}}</strong></td>
                                                        <td class="text-right"></td>
                                                        <td></td>
                                                    </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="float-left">Total Record : {{$list_data->total()}} Data Laporan</div>
                                                <div class="float-right">{{$list_data->appends($_GET)->links()}}</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('breadcrumb')
    {{ Breadcrumbs::render('laporan.bop') }}
@endsection
