@extends('layouts.admin.main')

@section('content')
    <?php
    $app_cfg = config('kontrollingapp.type_member');
    ?>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Data Driver
                                <div class="float-right"><a href="{{route('driver.add')}}" class="btn btn-primary"><span class="icon-user-follow"></span>&nbsp;&nbsp; Tambah Driver</a></div>
                            </h5>

                        </div>

                        <div class="card-body">

                            @if (session('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('success') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            @if (session('failed'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('failed') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            <form action="" id="form_option_driver">
                                <div class="row">

                                    <div class="col-md-3 mb-2">
                                        <div class="form-group row mb-0">
                                            <label for="data_length" class="col-md-3 col-form-label-sm mr-0 pr-0">Tampil :</label>
                                            <div class="col-md-9 ml-0 pl-0">
                                                <select name="data_length" onchange="document.getElementById('form_option_driver').submit()" class="form-control form-control-sm" id="data_length">
                                                    <option value="10" @if(request()->has('data_length')) {{request('data_length') == 10 ? 'selected' : ''}} @endif>10</option>
                                                    <option value="20" @if(request()->has('data_length')) {{request('data_length') == 20 ? 'selected' : ''}} @endif>20</option>
                                                    <option value="50" @if(request()->has('data_length')) {{request('data_length') == 50 ? 'selected' : ''}} @endif>50</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 mb-2">
                                        <div class="form-group row mb-0">
                                            <label for="data_length" class="col-md-3 col-form-label-sm mr-0 pr-0">Order :</label>
                                            <div class="col-md-9 ml-0 pl-0">
                                                <select name="order_by" onchange="document.getElementById('form_option_driver').submit()" class="form-control form-control-sm" id="data_length">
                                                    <option value="">Pilih Kolom</option>
                                                    <option value="email" @if(request()->has('order_by')) {{request('order_by') == 'email' ? 'selected' : ''}} @endif>Email</option>
                                                    <option value="name" @if(request()->has('order_by')) {{request('order_by') == 'name' ? 'selected' : ''}} @endif>Nama</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered dataTable" id="data_table_driver">
                                                <thead>
                                                <tr>
                                                    <th class="text-center text-uppercase" width="20%">Kode</th>
                                                    <th class="text-center text-uppercase">Nama</th>
                                                    <th class="text-center text-uppercase" width="200">Alamat</th>
                                                    <th class="text-center text-uppercase">Provinsi</th>
                                                    <th class="text-center text-uppercase">Kabupaten</th>
                                                    <th class="text-center text-uppercase">Kecamatan</th>
                                                    <th class="text-center" width="20%">Opsi</th>
                                                </tr>
                                                <tr>
                                                    <form type="GET">
                                                        <td><input type="text" class="form-control" value="{{request()->has('kode_search') ? request('kode_search') : ''}}" name="kode_search"></td>
                                                        <td><input type="text" class="form-control" value="{{request()->has('nama_search') ? request('nama_search') : ''}}" name="nama_search"></td>
                                                        <td><input type="text" class="form-control" value="{{request()->has('alamat_search') ? request('alamat_search') : ''}}" name="alamat_search"></td>
                                                        <td><input type="text" class="form-control" value="{{request()->has('provinsi_search') ? request('provinsi_search') : ''}}" name="provinsi_search"></td>
                                                        <td><input type="text" class="form-control" value="{{request()->has('kabupaten_search') ? request('kabupaten_search') : ''}}" name="kabupaten_search"></td>
                                                        <td><input type="text" class="form-control" value="{{request()->has('kecamatan_search') ? request('kecamatan_search') : ''}}" name="kecamatan_search"></td>
                                                        <td>
                                                            <center>
                                                                <button type="submit" class="btn btn-primary btn-sm btn-block mb-1">Search</button>
                                                                <a href="{{route('driver')}}" class="btn btn-danger btn-block btn-sm">Reset</a>
                                                            </center>
                                                        </td>
                                                    </form>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @forelse($list_data as $data)
                                                    <tr>
                                                        <td class="text-center text-uppercase">{{$data->kode}}</td>
                                                        <td class="text-uppercase">{{$data->name}}</td>
                                                        <td class="text-uppercase">{{$data->alamat}}</td>
                                                        <td class="text-uppercase">{{$data->provinsi}}</td>
                                                        <td class="text-uppercase">{{$data->kabupaten}}</td>
                                                        <td class="text-uppercase">{{$data->kecamatan}}</td>
                                                        <td class="text-center">
                                                            <a href="{{route('driver.edit', ['id'=>$data->id])}}" class="btn btn-sm btn-brand btn-behance mb-1 text-white">
                                                                <i class="fa fa-pencil"></i><span>Edit</span>
                                                            </a>

                                                            @if($data->list_kendaraan()->count() > 0)
                                                                <a href="{{route('driver.hapus_kendaraan', ['id'=>$data->id])}}" class="btn btn-sm btn-brand btn-stack-overflow mb-1 text-white">
                                                                    <i class="fa fa-pencil"></i><span>Hapus Kendaraan</span>
                                                                </a>
                                                            @endif

                                                            <button type="button" onclick="if(confirm('Apakah Anda yakin Akan Menghapus Data ini?')){document.getElementById('form_hapus_{{$data->kode}}').submit()}" class="btn btn-sm btn-brand btn-youtube mb-1">
                                                                <i class="fa fa-trash"></i><span>Hapus</span>
                                                            </button>

                                                            <form id="form_hapus_{{$data->kode}}" method="post" action="{{route('driver.hapus', ['kode'=>$data->kode])}}">
                                                                {{csrf_field()}}
                                                                <input type="hidden" name="id" value="{{$data->id}}">
                                                            </form>

                                                        </td>
                                                    </tr>
                                                @empty
                                                    <tr>
                                                        <td colspan="7"><center>Tidak Terdapat Data Driver</center></td>
                                                    </tr>
                                                @endforelse
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th class="text-center">Kode</th>
                                                    <th class="text-center">Nama</th>
                                                    <th class="text-center">Alamat</th>
                                                    <th class="text-center">Provinsi</th>
                                                    <th class="text-center">Kabupaten</th>
                                                    <th class="text-center">Kecamatan</th>
                                                    <th class="text-center">Opsi</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="float-left">Total Record : {{$list_data->total()}} Data Driver</div>
                                                <div class="float-right">{{$list_data->appends($_GET)->links()}}</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('breadcrumb')
    {{ Breadcrumbs::render('driver') }}
@endsection
