@extends('layouts.admin.main')

@section('content')
    <?php
    $app_cfg = config('kontrollingapp.type_member');
    ?>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <form class="form-horizontal" action="{{route('driver.simpan')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" value="{{$driver->id}}" name="id">
                            <div class="card-header">
                                <strong>Form Tambah</strong> Driver
                            </div>
                            <div class="card-body">
                                <h4><i class="fa fa-info-circle"></i>&nbsp;&nbsp;Data General</h4>
                                <hr>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="kode">Kode Driver</label>
                                    <div class="col-md-9">
                                        <input class="form-control" id="kode" type="text" readonly value="{{strtoupper($driver->kode)}}" name="kode" placeholder="Text">
                                        <span class="help-block info"><small>Kode Ini Digenerate Otomatis Oleh Sistem</small></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="ktp">No KTP</label>
                                    <div class="col-md-9">
                                        <input class="form-control text-uppercase {{$errors->has('no_ktp') ? 'is-invalid' : ''}}" value="{{$driver->no_ktp}}" id="ktp" type="text" name="no_ktp" placeholder="Masukan Nomor KTP">
                                        @if($errors->has('no_ktp'))
                                            <span class="invalid-feedback">{{$errors->first('no_ktp')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="name">Nama Lengkap</label>
                                    <div class="col-md-9">
                                        <input class="form-control text-uppercase {{$errors->has('name') ? 'is-invalid' : ''}}" value="{{$driver->name}}" id="name" type="text" name="name" placeholder="Masukan Nama Lengkap">
                                        @if($errors->has('name'))
                                            <span class="invalid-feedback">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="tanggal_mulai_bekerja">Tanggal Mulai Bekerja</label>
                                    <div class="col-md-9">
                                        <input  class="form-control text-uppercase {{$errors->has('tanggal_mulai_bekerja') ? 'is-invalid' : ''}}" value="{{date('Y-m-d', strtotime($driver->tanggal_mulai_bekerja))}}" id="tanggal_mulai_bekerja" type="date" name="tanggal_mulai_bekerja" placeholder="Masukan Nama Asuransi">
                                        @if($errors->has('tanggal_mulai_bekerja'))
                                            <span class="invalid-feedback">{{$errors->first('tanggal_mulai_bekerja')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="foto">Foto</label>
                                    <div class="col-md-9">
                                        <?php
                                        $foto = $driver->foto;

                                        ?>
                                        @if($foto)
                                            <?php
                                            $foto = json_decode($foto);
                                            ?>
                                            <img src="{{'/'.$foto->thumbnail_path}}" class="img img-rounded" role="image" width="100px" height="100px" style="margin-bottom: 20px" />
                                        @else
                                            <img src="{{url('/images/team.png')}}" class="img img-rounded" role="image" width="100px" height="100px" style="margin-bottom: 20px" />

                                        @endif

                                        <input  class="form-control text-uppercase {{$errors->has('foto') ? 'is-invalid' : ''}}" value="{{old('foto')}}" id="foto" type="file" name="foto" >
                                        <small class="help-block">Kosongkan Jika Tidak Mau Merubah Foto</small>
                                        @if($errors->has('foto'))
                                            <span class="invalid-feedback">{{$errors->first('foto')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <br>
                                <h4> <i class="fa fa-car"></i>&nbsp;&nbsp;Data Kendaraan</h4>
                                <hr>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="kendaraan">Pilih Kendaraan</label>
                                    <div class="col-md-9">
                                        <select class="form-control select_kendaraan" name="kendaraan" id="kendaraan">
                                            <option value="">Pilih Kendaraan</option>
                                            @if(count($driver->list_kendaraan) > 0)
                                                <option value="{{$driver->list_kendaraan[0]->id}}" selected>{{strtoupper($driver->list_kendaraan[0]->no_plat)}}</option>
                                            @endif
                                            @forelse($list_kendaraan as $kendaraan)
                                                <option value="{{$kendaraan->id}}" data-kendaraan="{{$kendaraan}}">{{strtoupper($kendaraan->no_plat)}}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                        @if($errors->has('kendaraan'))
                                            <span class="invalid-feedback">{{$errors->first('kendaraan')}}</span>
                                        @endif

                                        <div class="p-3 mt-2 bg-info {{count($driver->list_kendaraan) > 0 ? '' : 'd-none' }} info_kendaraan">
                                            <div class="form-group-sm">
                                                <label for="kendaraan_pabrikan">Pabrikan</label>
                                                <input type="text" class="form-control form-control-sm" value="{{count($driver->list_kendaraan) > 0 ? $driver->list_kendaraan[0]->pabrikan : ''}}" id="kendaraan_pabrikan" readonly placeholder="Pabrikan">
                                            </div>
                                            <div class="form-group-sm mt-1">
                                                <label for="kendaraan_no_seri">No Seri</label>
                                                <input type="text" class="form-control form-control-sm mt-2" value="{{count($driver->list_kendaraan) > 0 ? $driver->list_kendaraan[0]->seri : ''}}" id="kendaraan_no_seri" name="kendaraan_no_seri" readonly placeholder="Nomor Seri">
                                            </div>
                                            <div class="form-group-sm mt-1">
                                                <label for="kendaraan_no_mesin">No Mesin</label>
                                                <input type="text" class="form-control form-control-sm mt-2" value="{{count($driver->list_kendaraan) > 0 ? $driver->list_kendaraan[0]->no_mesin : ''}}" id="kendaraan_no_mesin" name="kendaraan_no_mesin" readonly placeholder="Nomor Mesin">
                                            </div>
                                            <div class="form-group-sm mt-1">
                                                <label for="kendaraan_no_rangka">No Rangka</label>
                                                <input type="text" class="form-control form-control-sm mt-2" value="{{count($driver->list_kendaraan) > 0 ? $driver->list_kendaraan[0]->no_rangka : ''}}" id="kendaraan_no_rangka" name="kendaraan_no_rangka" readonly placeholder="Nomor Rangka">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <h4> <i class="fa fa-address-book"></i>&nbsp;&nbsp;Data Kontak</h4>
                                <hr>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="telepon">No. Telepon</label>
                                    <div class="col-md-9">
                                        <input class="form-control text-uppercase {{$errors->has('telepon') ? 'is-invalid' : ''}}" value="{{$driver->telepon}}" id="telepon" type="number" name="telepon" placeholder="Nomor Telepon">
                                        @if($errors->has('telepon'))
                                            <span class="invalid-feedback">{{$errors->first('telepon')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="email">Email</label>
                                    <div class="col-md-9">
                                        <input class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" readonly value="{{$driver->email}}" id="email" type="email" name="email" placeholder="Masukan Email" autocomplete="email">
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <br>
                                <h4> <i class="fa fa-credit-card"></i>&nbsp;&nbsp;Rekening</h4>
                                <hr>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="no_rek">No. Rekening</label>
                                    <div class="col-md-9">
                                        <input class="form-control text-uppercase {{$errors->has('no_rek') ? 'is-invalid' : ''}}" value="{{$driver->no_rek}}" id="no_rek" type="number" name="no_rek" placeholder="Nomor Rekening">
                                        @if($errors->has('no_rek'))
                                            <span class="invalid-feedback">{{$errors->first('no_rek')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="tipe_bank">Nama Bank</label>
                                    <div class="col-md-9">
                                        <input class="form-control text-uppercase {{$errors->has('tipe_bank') ? 'is-invalid' : ''}}" value="{{$driver->tipe_bank}}" id="tipe_bank" type="text" name="tipe_bank" placeholder="Nama Bank">
                                        @if($errors->has('tipe_bank'))
                                            <span class="invalid-feedback">{{$errors->first('tipe_bank')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <br />
                                <h4> <i class="fa fa-map"></i>&nbsp;&nbsp;Data Alamat</h4>
                                <hr>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="provinsi">Provinsi</label>
                                    <div class="col-md-9">
                                        <input class="form-control text-uppercase {{$errors->has('provinsi') ? 'is-invalid' : ''}}" value="{{$driver->provinsi}}" id="provinsi" type="text" name="provinsi" placeholder="Masukan Provinsi">
                                        @if($errors->has('provinsi'))
                                            <span class="invalid-feedback">{{$errors->first('provinsi')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="kabupaten">Kabupaten</label>
                                    <div class="col-md-9">
                                        <input class="form-control text-uppercase {{$errors->has('kabupaten') ? 'is-invalid' : ''}}" value="{{$driver->provinsi}}" id="kabupaten" type="text" name="kabupaten" placeholder="Masukan Kabupaten">
                                        @if($errors->has('kabupaten'))
                                            <span class="invalid-feedback">{{$errors->first('kabupaten')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="kecamatan">Kecamatan</label>
                                    <div class="col-md-9">
                                        <input class="form-control text-uppercase {{$errors->has('kecamatan') ? 'is-invalid' : ''}}" value="{{$driver->kecamatan}}" id="kecamatan" type="text" name="kecamatan" placeholder="Masukan Kecamatan">
                                        @if($errors->has('kecamatan'))
                                            <span class="invalid-feedback">{{$errors->first('kecamatan')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="alamat">Alamat</label>
                                    <div class="col-md-9">
                                        <textarea class="form-control text-uppercase {{$errors->has('alamat') ? 'is-invalid' : ''}}" id="alamat" rows="4" type="text" name="alamat" placeholder="Masukan alamat">{{$driver->alamat}}</textarea>
                                        @if($errors->has('alamat'))
                                            <span class="invalid-feedback">{{$errors->first('alamat')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <br>

                                <h4> <i class="fa fa-key"></i>&nbsp;&nbsp;Data Autentikasi</h4>
                                <hr>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="email">Email</label>
                                    <div class="col-md-9">
                                        <input class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" readonly value="{{$driver->email}}" id="email" type="email" name="email" placeholder="Masukan Email" autocomplete="email">
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="username">Username</label>
                                    <div class="col-md-9">
                                        <input class="form-control text-uppercase {{$errors->has('username') ? 'is-invalid' : ''}}" readonly value="{{$driver->username}}" id="username" type="text" name="username" placeholder="Masukan Username">
                                        @if($errors->has('username'))
                                            <span class="invalid-feedback">{{$errors->first('username')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="password">Password</label>
                                    <div class="col-md-9">
                                        <input class="form-control" id="password" type="password" name="password" placeholder="Password" value="{{old('password')}}" autocomplete="password">
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="password_confirmation">Konfirmasi Password</label>
                                    <div class="col-md-9">
                                        <input class="form-control {{$errors->has('password_confirmation') ? 'is-invalid' : ''}}" id="password_confirmation" type="password" name="password_confirmation" placeholder="Konfirmasi Password" autocomplete="new-password">
                                        @if($errors->has('password_confirmation'))
                                            <span class="invalid-feedback">{{$errors->first('password_confirmation')}}</span>
                                        @endif
                                    </div>
                                </div>

                            </div>
                            <div class="card-footer">
                                <button class="btn btn-sm btn-primary" type="submit">
                                    <i class="fa fa-dot-circle-o"></i> Submit</button>
                                <button class="btn btn-sm btn-danger" type="reset">
                                    <i class="fa fa-ban"></i> Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script>

    </script>
@endsection

@section('breadcrumb')
    {{ Breadcrumbs::render('driver.add', $driver) }}
@endsection
