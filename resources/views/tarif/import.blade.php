@extends('layouts.admin.main')

@section('content')
    <?php
    $app_cfg = config('kontrollingapp.type_member');
    ?>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Import Data</h5>

                        </div>

                        <div class="card-body">

                            @if (session('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('success') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            @if (session('failed'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('failed') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            <div class="alert alert-info">
                                <p>
                                    Sebelum melakukan import, pastikan format data sudah sama dengan format data dari contoh File Import dari Sistem,
                                </p>
                                <p>
                                    <a href="{{url('/data/contoh_import_tarif.xlsx')}}" class="btn btn-success">Download Contoh Data Import</a>
                                </p>
                            </div>
                                <form action="{{route('tarif.import')}}" method="post" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label for="file_import">Masukan File</label>
                                        <input type="file" name="file_import"  class="form-control {{$errors->has('file_import') ? 'is-invalid' : ''}}" id="file_import">
                                        @if($errors->has('file_import'))
                                            <span class="invalid-feedback">
                                                    {{$errors->first('file_import')}}
                                                </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary" type="submit">Import Data</button>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('breadcrumb')
    {{ Breadcrumbs::render('tarif.import') }}
@endsection
