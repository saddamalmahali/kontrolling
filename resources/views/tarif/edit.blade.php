@extends('layouts.admin.main')

@section('content')
    <?php
    $app_cfg = config('kontrollingapp.type_member');
    ?>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <form class="form-horizontal" action="{{route('tarif.simpan')}}" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="id" value="{{$tarif->id}}">
                            <div class="card-header">
                                <strong>Form Edit Tarif</strong>
                            </div>
                            <div class="card-body">
                                {{csrf_field()}}
                                <h4> <i class="fa fa-info"></i>&nbsp;&nbsp;Informasi Tarif</h4>
                                <hr>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="kota_asal">Kota Asal</label>
                                    <div class="col-md-9">
                                        <select name="kota_asal" id="kota_asal" data-select="{{$tarif->asal_id}}" class="form-control select2_search_asal">
                                            <option value="">Cari Kota Asal</option>
                                            @forelse($list_kota as $kota)
                                                <option value="{{$kota->id}}" >{{$kota->name}}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                        @if($errors->has('kota_asal'))
                                            <span class="invalid-feedback">
                                                {{$errors->first('kota_asal')}}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="kota_tujuan">Kota Tujuan</label>
                                    <div class="col-md-9">
                                        <select name="kota_tujuan" id="kota_tujuan" data-select="{{$tarif->tujuan_id}}" class="form-control select2_search_tujuan">
                                            <option value="">Cari Kota Tujuan</option>
                                            @forelse($list_kota as $kota)
                                                <option value="{{$kota->id}}" >{{$kota->name}}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                        @if($errors->has('kota_asal'))
                                            <span class="invalid-feedback">
                                                {{$errors->first('kota_asal')}}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="tarif">Tarif</label>
                                    <div class="col-md-9">
                                        <input class="form-control mask_money {{$errors->has('tarif') ? 'is-invalid' : ''}}" id="tarif" type="text" name="tarif" value="{{$tarif->tarif}}" placeholder="Masukan Tarif">
                                        @if($errors->has('tarif'))
                                            <span class="invalid-feedback">
                                                    {{$errors->first('tarif')}}
                                                </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="bop">BOP</label>
                                    <div class="col-md-9">
                                        <input class="form-control mask_money {{$errors->has('bop') ? 'is-invalid' : ''}}" id="bop" type="text" name="bop" value="{{$tarif->bop}}" placeholder="Masukan BOP">
                                        @if($errors->has('bop'))
                                            <span class="invalid-feedback">
                                                    {{$errors->first('bop')}}
                                                </span>
                                        @endif
                                    </div>
                                </div>

                            </div>
                            <div class="card-footer">
                                <button class="btn btn-sm btn-primary" type="submit">
                                    <i class="fa fa-dot-circle-o"></i> Submit</button>
                                <button class="btn btn-sm btn-danger" type="reset">
                                    <i class="fa fa-ban"></i> Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection


@section('breadcrumb')
    {{ Breadcrumbs::render('tarif.edit', $tarif) }}
@endsection
