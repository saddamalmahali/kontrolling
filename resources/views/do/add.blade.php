@extends('layouts.admin.main')

@section('content')
    @if(auth()->user()->type != USER_TYPE_DRIVER)
        <do-add
            code="{{strtoupper($generate_code)}}"
            token="{{csrf_token()}}"
            data-url="{{route('do.simpan')}}"
        ></do-add>
    @else

    @endif
@endsection
@section('breadcrumb')
    {{ Breadcrumbs::render('do.add') }}
@endsection
