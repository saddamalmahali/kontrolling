@extends('layouts.admin.main')

@section('content')
    <?php
    $app_cfg = config('kontrollingapp.type_member');
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5>Data Delivery Order
                        <div class="float-right"><a href="{{route('do.add')}}" class="btn btn-primary"><span class="icon-user-follow"></span>&nbsp;&nbsp; Tambah Delivery Order</a></div>
                    </h5>
                </div>

                <div class="card-body">

                    @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('success') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    @if (session('failed'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('failed') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <form action="" id="form_option_karyawan">
                        <div class="row">

                            <div class="col-md-3 mb-2">
                                <div class="form-group row mb-0">
                                    <label for="data_length" class="col-md-3 col-form-label-sm mr-0 pr-0">Tampil :</label>
                                    <div class="col-md-9 ml-0 pl-0">
                                        <select name="data_length" onchange="document.getElementById('form_option_karyawan').submit()" class="form-control form-control-sm" id="data_length">
                                            <option value="10" @if(request()->has('data_length')) {{request('data_length') == 10 ? 'selected' : ''}} @endif>10</option>
                                            <option value="20" @if(request()->has('data_length')) {{request('data_length') == 20 ? 'selected' : ''}} @endif>20</option>
                                            <option value="50" @if(request()->has('data_length')) {{request('data_length') == 50 ? 'selected' : ''}} @endif>50</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 mb-2">
                                <div class="form-group row mb-0">
                                    <label for="data_length" class="col-md-3 col-form-label-sm mr-0 pr-0">Order :</label>
                                    <div class="col-md-9 ml-0 pl-0">
                                        <select name="order_by" onchange="document.getElementById('form_option_karyawan').submit()" class="form-control form-control-sm" id="data_length">
                                            <option value="">Pilih Kolom</option>
                                            <option value="email" @if(request()->has('order_by')) {{request('order_by') == 'email' ? 'selected' : ''}} @endif>Email</option>
                                            <option value="name" @if(request()->has('order_by')) {{request('order_by') == 'name' ? 'selected' : ''}} @endif>Nama</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <?php
                                    $noUrut = ($list_data->currentpage()-1)* $list_data->perpage() + 1;


                                    ?>
                                    <table class="table table-bordered table-fix dataTable" id="data_table_karyawan">
                                        <thead>
                                        <tr>
                                            <th class="text-center text-uppercase" style="min-width: 100px;">No</th>
                                            <th class="text-center text-uppercase" style="min-width: 200px;">No DO</th>
                                            <th class="text-center text-uppercase" style="min-width: 200px;">Tanggal</th>
                                            <th class="text-center text-uppercase" style="min-width: 250px;">Alamat Tujuan</th>
                                            <th class="text-center text-uppercase" style="min-width: 200px;">Pengirim</th>
                                            <th class="text-center text-uppercase" style="min-width: 200px;">Kendaraan</th>
                                            <th class="text-center text-uppercase" style="min-width: 150px;">Penerima</th>
                                            <th class="text-center text-uppercase" style="min-width: 100px;">Kuantitas</th>
                                            <th class="text-center text-uppercase" style="min-width: 150px;">Status</th>
                                            <th class="text-center text-uppercase" style="min-width: 200px;">Opsi</th>
                                        </tr>
                                        <tr>
                                            <form type="GET">
                                                <td></td>
                                                <td></td>

                                                <td><input type="text" class="form-control" id="date_picker" value="{{request()->has('tanggal_search') ? request('tanggal_search') : ''}}" name="tanggal_search"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                    <select name="search_status" class="form-control" id="search_status">
                                                        <option value="">Pilih Status</option>
                                                        <option value="{{DO_STATUS_AKTIF}}" {{request()->get('search_status') === DO_STATUS_AKTIF ? 'selected' : ''}}>Aktif</option>
                                                        <option value="{{DO_STATUS_PROSES}}" {{request()->get('search_status') === DO_STATUS_PROSES ? 'selected' : ''}}>Proses</option>
                                                        <option value="{{DO_STATUS_KIRIM}}" {{request()->get('search_status') === DO_STATUS_KIRIM ? 'selected' : ''}}>Kirim</option>
                                                        <option value="{{DO_STATUS_SELESAI}}" {{request()->get('search_status') === DO_STATUS_SELESAI ? 'selected' : ''}}>Selesai</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <center>
                                                        <button type="submit" class="btn btn-primary btn-sm btn-block mb-1">Search</button>
                                                        <a href="{{route('do')}}" class="btn btn-danger btn-block btn-sm">Reset</a>
                                                    </center>
                                                </td>
                                            </form>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($list_data as $data)
                                            <tr>
                                                <td class="text-uppercase text-center">{{$noUrut}}</td>
                                                <td class="text-uppercase">
                                                    <?php
                                                        $length_list = $data->detile()->count();
                                                    ?>
                                                    <?php if($length_list > 0): ?>
                                                        <?php
                                                            $noDO = '';
                                                            $indexDo = 1;
                                                            foreach ($data->detile as $detile){
                                                                if($detile->no_do){
                                                                    $noDO .= $detile->no_do;
                                                                    if($length_list> $indexDo){
                                                                        $noDO .= ', ';
                                                                    }
                                                                }

                                                                $indexDo++;
                                                            }
                                                        ?>

                                                        <?= $noDO ?>
                                                    <?php else: ?>
                                                        -
                                                    <?php endif; ?>
                                                </td>
                                                <td class="text-uppercase">{{date('d-m-Y', strtotime($data->tanggal))}}</td>
                                                <td class="text-uppercase">
                                                    <strong>Provinsi : {{strtoupper($data->provinsi)}}</strong><br/>
                                                    <strong>Kabupaten : {{strtoupper($data->kabupaten)}}</strong><br/>
                                                    <strong>Kecamatan : {{strtoupper($data->kecamatan)}}</strong><br/>
                                                    <strong>Alamat : {{strtoupper($data->alamat)}}</strong><br/>
                                                </td>
                                                <td class="text-uppercase">{{strtoupper($data->driver ? $data->driver->name : '-')}}</td>
                                                <td class="text-uppercase">
                                                    No Plat : {{$data->kendaraan ? $data->kendaraan->no_plat : '-'}} <br>
                                                    No STNK : {{$data->kendaraan ? $data->kendaraan->stnk : '-'}} <br>
                                                </td>
                                                <td class="text-uppercase text-center">{{strtoupper($data->nama_penerima)}}</td>
                                                <td class="text-uppercase text-center">
                                                    <?php
                                                    $berat = $data->detile()->sum('kuantitas');

                                                    if($berat<1000){
                                                        echo $berat.' Kg';
                                                    }else{
                                                        $berat = $berat/1000;
                                                        echo $berat.' Ton';
                                                    }

                                                    ?>
                                                </td>
                                                <td class="text-uppercase text-center">
                                                    <?php
                                                    switch ($data->status){
                                                        case 'aktif' :
                                                            echo '<span class="badge badge-info">Aktif</span>';
                                                            break;
                                                        case 'proses' :
                                                            echo '<span class="badge badge-primary">Proses</span>';
                                                            break;
                                                        case 'selesai' :
                                                            echo '<span class="badge badge-success">Selesai</span>';
                                                            break;
                                                        case 'kirim' :
                                                            echo '<span class="badge badge-warning">Proses Pengiriman</span>';

                                                    }
                                                    ?>
                                                    <span class="badge-info"></span>
                                                </td>
                                                <td class="text-center">
                                                    <a href="{{route('do.edit', ['id'=>$data->id])}}" class="btn btn-sm btn-brand btn-behance mb-1 text-white">
                                                        <i class="fa fa-pencil"></i><span>Edit Barang</span>
                                                    </a>

                                                    @if(auth()->user()->type == USER_TYPE_ADMIN && $data->status == DO_STATUS_AKTIF)
                                                        <a href="{{route('do.proses', ['id'=>$data->id])}}" class="btn btn-sm btn-brand btn-yahoo mb-1 text-white"><i class="fa fa-code-fork"></i><span>Proses Biaya</span></a>
                                                    @endif

                                                    @if($data->status == DO_STATUS_PROSES)
                                                        <a href="{{route('do.kirim', ['do'=>$data->id])}}" class="btn btn-sm btn-brand btn-flickr mb-1 text-white"><i class="fa fa-bolt"></i><span>Start Pengiriman</span></a>
                                                    @elseif($data->status == DO_STATUS_KIRIM)
                                                        <a href="{{route('do.stop', ['do'=>$data->id])}}" class="btn btn-sm btn-brand btn-youtube mb-1 text-white"><i class="fa fa-info-circle"></i><span>Stop Pengiriman</span></a>
                                                    @endif

                                                    <a href="{{route('do.detail', ['id'=>$data->id])}}" class="btn btn-sm btn-brand mb-1 text-white btn-foursquare" style="margin-bottom: 4px">
                                                        <i class="fa fa-search"></i>
                                                        <span>Detail</span>
                                                    </a>

                                                </td>
                                            </tr>
                                            <?php $noUrut++; ?>
                                        @empty
                                            <tr>
                                                <td colspan="10"><center>Tidak Terdapat Data Delivery Order</center></td>
                                            </tr>
                                        @endforelse
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th class="text-center text-uppercase">No</th>
                                            <th class="text-center text-uppercase">No DO</th>
                                            <th class="text-center text-uppercase">Tanggal</th>
                                            <th class="text-center text-uppercase">Alamat</th>
                                            <th class="text-center text-uppercase">Pengirim</th>
                                            <th class="text-center text-uppercase">Kendaraan</th>
                                            <th class="text-center text-uppercase">Penerima</th>
                                            <th class="text-center text-uppercase">Kuantitas</th>
                                            <th class="text-center text-uppercase">Status</th>
                                            <th class="text-center text-uppercase">Opsi</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="float-left">Total Record : {{$list_data->total()}} Data Delivery Order</div>
                                        <div class="float-right">{{$list_data->appends($_GET)->links()}}</div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('breadcrumb')
    {{ Breadcrumbs::render('do') }}
@endsection
