@extends('layouts.admin.main')

@section('content')
    <div><div class="card">
            <div class="card-header">Nomor Order :
                <strong>#{{$do->nomor}}</strong>
                <a class="btn btn-sm btn-secondary float-right mr-1 d-print-none" href="#" onclick="javascript:window.print();">
                    <i class="fa fa-print"></i> Print</a>
                <a class="btn btn-sm btn-info float-right mr-1 d-print-none" href="{{route('do')}}">
                    <i class="fa fa-arrow-left"></i> Kembali</a>
            </div>
            <div class="card-body">
                <div class="row mb-4">
                    <div class="col-sm-4">
                        <h6 class="mb-3">From:</h6>
                        <div>
                            <strong>{{option_exists('company_name') ? option('company_name') : 'Nama Perusahaan'}}</strong>
                        </div>
                        <div>{{option_exists('address') ? option('address') : 'Alamat'}}</div>
                        <div>{{option_exists('kecamatan') ? option('kecamatan') : 'Kecamatan'}}, Kab. {{option_exists('kabupaten') ? option('kabupaten') : 'Kabupaten'}} - {{option_exists('provinsi') ? strtoupper(option('provinsi')) : 'Provinsi'}}</div>
                        <div>Email: {{option_exists('email') ? option('email') : 'example@mail.com'}}</div>
                        <div>Phone: {{option_exists('telp') ? option('telp') : '08122329387298'}}</div>
                    </div>

                    <div class="col-sm-4">
                        <h6 class="mb-3">To:</h6>
                        <div>
                            <strong>{{$do->nama_penerima}}</strong>
                        </div>
                        <div>{{$do->alamat}}</div>
                        <div>{{$do->kecamatan}}, Kab. {{$do->kabupaten}} - {{strtoupper($do->provinsi)}}</div>
                    </div>

                    <div class="col-sm-4">
                        <h6 class="mb-3">Details:</h6>
                        <div>Invoice
                            <strong>#{{$do->nomor}}</strong>
                        </div>
                        <div>{{date('d M Y', strtotime($do->tanggal))}}</div>
                        <?php
                            $total_berat = $do->detile()->sum('kuantitas');

                        ?>
                        <div>Total Berat:
                        <?php
                            if($total_berat >= 1000){
                                echo ($total_berat/1000). ' Ton';
                            }else{
                                echo $total_berat.' Kg';
                            }
                            ?>
                        </div>
                        <div>Total Biaya : Rp. {{$do->bop ? number_format($do->bop->biaya) : '0'}}</div>
                    </div>

                </div>

                <div class="table-responsive-sm">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th class="center">#</th>
                            <th>No DO</th>
                            <th>No Ref</th>
                            <th>Kode</th>
                            <th class="center">Nama</th>
                            <th class="right">Kuantitas</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            $noUrut = 1;
                        ?>
                        @forelse($do->detile as $detile)
                            <tr>
                                <td class="center">{{$noUrut}}</td>
                                <td class="left">{{$detile->no_do}}</td>
                                <td class="left">{{$detile->no_ref ? $detile->no_ref : '-'}}</td>
                                <td class="left">{{$detile->kode}}</td>
                                <td class="left">{{$detile->nama}}</td>
                                <td class="center">
                                    <?php
                                        if($detile->kuantitas > 1000){
                                            echo ($detile->kuantitas/1000).' Ton ';
                                        }else{
                                            echo $detile->kuantitas.' Kg';
                                        }
                                    ?>
                                </td>
                            </tr>
                            <?php $noUrut++; ?>
                        @empty
                            <tr>
                                <td class="center"></td>
                            </tr>
                        @endforelse

                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-sm-5">
                        DO yang belum menerbitkan BOP tidak akan terbit invoice.
                        <br>
                        <br>
                        <br>
                        <b><u>Manajemen {{option_exists('company_name') ? option('company_name') : 'Nama Perusahaan'}}</u></b>
                        <br>
                        <br>
                        <br>
                    </div>
                    <div class="col-lg-4 col-sm-5 ml-auto">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('breadcrumb')
    {{ Breadcrumbs::render('do.detail', $do) }}
@endsection
