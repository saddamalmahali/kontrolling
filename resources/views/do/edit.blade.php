@extends('layouts.admin.main')

@section('content')
    <do-edit
            code="{{$do->kode}}"
            do="{{json_encode($do)}}"
            detile="{{json_encode($detile)}}"
            token="{{csrf_token()}}"
            data-url="{{route('do.simpan_detile')}}"
    ></do-edit>
@endsection
@section('breadcrumb')
    {{ Breadcrumbs::render('do.edit_detile', $do) }}
@endsection