@extends('layouts.admin.main')

@section('content')
    <?php
    $app_cfg = config('kontrollingapp.type_member');
    ?>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <form class="form-horizontal" action="{{route('do.simpan_konfirmasi')}}" method="post" enctype="multipart/form-data">
                            <div class="card-header">
                                <strong>Form Konfirmasi Penerimaan</strong>
                            </div>
                            <div class="card-body">
                                {{csrf_field()}}
                                <input type="hidden" name="shipping_id" value="{{$do->shipping->id}}">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="text-input">Nomor</label>
                                    <div class="col-md-9">
                                        <input class="form-control" id="text-input" type="text" readonly value="{{strtoupper($generate_code)}}" name="nomor" placeholder="Text">
                                        <span class="help-block info"><small>Kode Ini Digenerate Otomatis Oleh Sistem</small></span>
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="tanggal">Tanggal</label>
                                    <div class="col-md-9">
                                        <input class="form-control {{$errors->has('tanggal') ? 'is-invalid' : ''}}" id="tanggal" type="date" name="tanggal" value="{{old('tanggal')}}" placeholder="Masukan tanggal Penerimaan">
                                        @if($errors->has('tanggal'))
                                            <span class="invalid-feedback">
                                                {{$errors->first('tanggal')}}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="pecah">Pecah</label>
                                    <div class="col-md-9">
                                        <input class="form-control {{$errors->has('pecah') ? 'is-invalid' : ''}}" id="pecah" type="number" name="pecah" value="{{old('pecah')}}" placeholder="Masukan Barang yang Rusak/Pecah">
                                        @if($errors->has('pecah'))
                                            <span class="invalid-feedback">
                                                {{$errors->first('pecah')}}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="bukti">Bukti</label>
                                    <div class="col-md-9">
                                        <input class="form-control {{$errors->has('bukti') ? 'is-invalid' : ''}}" id="bukti" type="file" name="bukti" value="{{old('bukti')}}">
                                        <span class="help-block info"><small>Bukti Berupa Foto ( Good Receipt )</small></span>
                                        @if($errors->has('bukti'))
                                            <span class="invalid-feedback">
                                                {{$errors->first('bukti')}}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <br />

                            </div>
                            <div class="card-footer">
                                <button class="btn btn-sm btn-primary" type="submit">
                                    <i class="fa fa-dot-circle-o"></i> Submit</button>
                                <button class="btn btn-sm btn-danger" type="reset">
                                    <i class="fa fa-ban"></i> Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('breadcrumb')
    {{ Breadcrumbs::render('do.stop', $do) }}
@endsection
