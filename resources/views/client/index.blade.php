@extends('layouts.admin.main')

@section('content')
    <?php
    $app_cfg = config('kontrollingapp.type_member');
    ?>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Data Client
                                <div class="float-right"><a href="{{route('client.add')}}" class="btn btn-primary"><span class="icon-user-follow"></span>&nbsp;&nbsp; Tambah Client</a></div>
                            </h5>

                        </div>

                        <div class="card-body">

                            @if (session('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('success') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            @if (session('failed'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('failed') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            <form action="" id="form_option_client">
                                <div class="row">

                                    <div class="col-md-3 mb-2">
                                        <div class="form-group row mb-0">
                                            <label for="data_length" class="col-md-3 col-form-label-sm mr-0 pr-0">Tampil :</label>
                                            <div class="col-md-9 ml-0 pl-0">
                                                <select name="data_length" onchange="document.getElementById('form_option_client').submit()" class="form-control form-control-sm" id="data_length">
                                                    <option value="10" @if(request()->has('data_length')) {{request('data_length') == 10 ? 'selected' : ''}} @endif>10</option>
                                                    <option value="20" @if(request()->has('data_length')) {{request('data_length') == 20 ? 'selected' : ''}} @endif>20</option>
                                                    <option value="50" @if(request()->has('data_length')) {{request('data_length') == 50 ? 'selected' : ''}} @endif>50</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 mb-2">
                                        <div class="form-group row mb-0">
                                            <label for="data_length" class="col-md-3 col-form-label-sm mr-0 pr-0">Order :</label>
                                            <div class="col-md-9 ml-0 pl-0">
                                                <select name="order_by" onchange="document.getElementById('form_option_client').submit()" class="form-control form-control-sm" id="data_length">
                                                    <option value="">Pilih Kolom</option>
                                                    <option value="kode" @if(request()->has('order_by')) {{request('order_by') == 'kode' ? 'selected' : ''}} @endif>Kode</option>
                                                    <option value="nama" @if(request()->has('order_by')) {{request('order_by') == 'nama' ? 'selected' : ''}} @endif>Nama</option>
                                                    <option value="kuota" @if(request()->has('order_by')) {{request('order_by') == 'kuota' ? 'selected' : ''}} @endif>Kuota</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered dataTable" id="data_table_client">
                                                <thead>
                                                <tr>
                                                    <th class="text-center" width="20%">Kode</th>
                                                    <th class="text-center">Nama</th>
                                                    <th class="text-center">Alamat</th>
                                                    <th class="text-center">Kuota</th>
                                                    <th class="text-center">Status</th>
                                                    <th class="text-center" width="20%">Opsi</th>
                                                </tr>
                                                <tr>
                                                    <form type="GET">
                                                        <td><input type="text" class="form-control" value="{{request()->has('kode_search') ? request('kode_search') : ''}}" name="kode_search"></td>
                                                        <td><input type="text" class="form-control" value="{{request()->has('nama_search') ? request('nama_search') : ''}}" name="nama_search"></td>
                                                        <td><input type="text" class="form-control" value="{{request()->has('alamat_search') ? request('alamat_search') : ''}}" name="alamat_search"></td>
                                                        <td></td>
                                                        <td>
                                                            <select name="status_search" onchange="document.getElementById('form_option_client').submit()" class="form-control form-control-sm" id="data_length">
                                                                <option value="">Pilih Kolom</option>
                                                                <option value="{{KLIEN_STATUS_AKTIF}}" @if(request()->has('status_search')) {{request('status_search') == KLIEN_STATUS_AKTIF ? 'selected' : ''}} @endif>Aktif</option>
                                                                <option value="{{KLIEN_STATUS_NONAKTIF}}" @if(request()->has('status_search')) {{request('status_search') == KLIEN_STATUS_NONAKTIF ? 'selected' : ''}} @endif>Nonaktif</option>
                                                                <option value="{{KLIEN_STATUS_BLOKIR}}" @if(request()->has('status_search')) {{request('status_search') == KLIEN_STATUS_BLOKIR ? 'selected' : ''}} @endif>Blokir</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <center>
                                                                <button type="submit" class="btn btn-primary btn-sm btn-block mb-1">Search</button>
                                                                <a href="{{route('client')}}" class="btn btn-danger btn-block btn-sm">Reset</a>
                                                            </center>
                                                        </td>
                                                    </form>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @forelse($list_data as $data)
                                                    <tr>
                                                        <td class="text-center">{{$data->kode}}</td>
                                                        <td>{{strtoupper($data->nama)}}</td>
                                                        <td class="">{{$data->alamat}}</td>
                                                        <td class="text-center">{{$data->kuota}}</td>
                                                        <td class="text-center">
                                                            <?php
                                                                switch ($data->status){
                                                                    case KLIEN_STATUS_AKTIF :
                                                                        echo '<span class="badge badge-info text-white">Aktif</span>';
                                                                        break;
                                                                    case KLIEN_STATUS_NONAKTIF :
                                                                        echo '<span class="badge badge-warning text-white">Nonaktif</span>';
                                                                        break;
                                                                    case KLIEN_STATUS_BLOKIR :
                                                                        echo '<span class="badge badge-danger text-white">Blokir</span>';
                                                                }
                                                            ?>
                                                        </td>

                                                        <td class="text-center">
                                                            @if(auth()->user()->type != USER_TYPE_DRIVER)
                                                                <a href="{{route('client.edit', ['id'=>$data->id])}}" class="btn btn-sm btn-brand btn-behance mb-1 text-white">
                                                                    <i class="fa fa-pencil"></i><span>Edit</span>
                                                                </a>
                                                                @if($data->status == USER_STATUS_NONAKTIF)
                                                                    <button type="button" onclick="if(confirm('Apakah Anda yakin Akan Mengaktivasi Client ini?')){document.getElementById('form_konfirmasi_{{$data->kode}}').submit()}" class="btn btn-sm btn-brand btn-twitter mb-1">
                                                                        <i class="fa fa-trash"></i><span>Konfirmasi</span>
                                                                    </button>
                                                                    <form id="form_konfirmasi_{{$data->kode}}" method="post" action="{{route('client.konfirmasi', ['client'=>$data->id])}}">
                                                                        {{csrf_field()}}
                                                                        <input type="hidden" name="id" value="{{$data->id}}">
                                                                    </form>
                                                                @endif
                                                                <button type="button" onclick="if(confirm('Apakah Anda yakin Akan Menghapus Data ini?')){document.getElementById('form_hapus_{{$data->kode}}').submit()}" class="btn btn-sm btn-brand btn-youtube mb-1">
                                                                    <i class="fa fa-trash"></i><span>Hapus</span>
                                                                </button>

                                                                <form id="form_hapus_{{$data->kode}}" method="post" action="{{route('client.hapus', ['kode'=>$data->kode])}}">
                                                                    {{csrf_field()}}
                                                                    <input type="hidden" name="id" value="{{$data->id}}">
                                                                </form>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @empty
                                                    <tr>
                                                        <td colspan="6"><center>Tidak Terdapat Data Client</center></td>
                                                    </tr>
                                                @endforelse
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th class="text-center">Kode</th>
                                                    <th class="text-center">Nama</th>
                                                    <th class="text-center">Alamat</th>
                                                    <th class="text-center">Kuota</th>
                                                    <th class="text-center">Status</th>
                                                    <th class="text-center">Opsi</th>
                                                </tr>
                                                </tfoot>
                                            </table>



                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="float-left">Total Record : {{$list_data->total()}} Data Driver</div>
                                                <div class="float-right">{{$list_data->appends($_GET)->links()}}</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('breadcrumb')
    {{ Breadcrumbs::render('client') }}
@endsection
