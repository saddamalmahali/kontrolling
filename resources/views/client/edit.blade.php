@extends('layouts.admin.main')

@section('content')
    <?php
    $app_cfg = config('kontrollingapp.type_member');
    ?>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <form class="form-horizontal" action="{{route('client.simpan')}}" method="post" enctype="multipart/form-data">
                            <div class="card-header">
                                <strong>Form Edit Client</strong>
                            </div>

                            <div class="card-body">
                                {{csrf_field()}}
                                <h4> <i class="fa fa-info"></i>&nbsp;&nbsp;Informasi Umum</h4>
                                <hr>
                                <input type="hidden" name="id" value="{{$client->id}}">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="text-input">Kode Client</label>
                                    <div class="col-md-9">
                                        <input class="form-control" id="text-input" type="text" readonly value="{{$client->kode}}" name="kode" placeholder="Text">
                                        <span class="help-block info"><small>Kode Ini Digenerate Otomatis Oleh Sistem</small></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="nama">Nama</label>
                                    <div class="col-md-9">
                                        <input class="form-control {{$errors->has('nama') ? 'is-invalid' : ''}}" id="nama" type="text" name="nama" value="{{$client->nama}}" placeholder="Masukan Nama Perusahaan">
                                        @if($errors->has('nama'))
                                            <span class="invalid-feedback">
                                                {{$errors->first('nama')}}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="npwp">NPWP</label>
                                    <div class="col-md-9">
                                        <input class="form-control {{$errors->has('npwp') ? 'is-invalid' : ''}}" id="npwp" type="text" name="npwp" value="{{$client->npwp}}" placeholder="Masukan NPWP Perusahaan">
                                        @if($errors->has('npwp'))
                                            <span class="invalid-feedback">
                                                    {{$errors->first('npwp')}}
                                                </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="alamat">Alamat</label>
                                    <div class="col-md-9">
                                        <textarea class="form-control" id="alamat" name="alamat" rows="9" placeholder="Masukan Alamat Lengkap Perusahaan..">{{$client->alamat}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="kuota">Kuota </label>
                                    <div class="col-md-9">
                                        <input class="form-control {{$errors->has('kuota') ? 'is-invalid' : ''}}" id="kuota" type="text" name="kuota" value="{{$client->kuota}}" placeholder="Masukan Kuota">
                                        @if($errors->has('kuota'))
                                            <span class="invalid-feedback">
                                                {{$errors->first('kuota')}}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="status">Status</label>
                                    <div class="col-md-9">
                                        <select class="form-control" id="status" name="status">
                                            <option value="{{KLIEN_STATUS_AKTIF}}" {{$client->status == KLIEN_STATUS_AKTIF ? 'selected' : ''}}>Aktif</option>
                                            <option value="{{KLIEN_STATUS_NONAKTIF}}" {{$client->status == KLIEN_STATUS_NONAKTIF ? 'selected' : ''}}>Nonaktif</option>
                                            <option value="{{KLIEN_STATUS_BLOKIR}}" {{$client->status == KLIEN_STATUS_BLOKIR ? 'selected' : ''}}>Blokir</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="foto">Foto</label>
                                    <div class="col-md-9">
                                        <?php
                                        $foto = $client->foto;

                                        ?>
                                        @if($foto)
                                            <?php
                                            $foto = json_decode($foto);
                                            ?>
                                            <img src="{{'/'.$foto->thumbnail_path}}" class="img img-rounded" role="image" width="100px" height="100px" style="margin-bottom: 20px" />
                                        @else
                                            <img src="{{url('/images/team.png')}}" class="img img-rounded" role="image" width="100px" height="100px" style="margin-bottom: 20px" />

                                        @endif

                                        <input  class="form-control text-uppercase {{$errors->has('foto') ? 'is-invalid' : ''}}" value="{{old('foto')}}" id="foto" type="file" name="foto" >
                                        <small class="help-block">Kosongkan Jika Tidak Mau Merubah Foto</small>
                                        @if($errors->has('foto'))
                                            <span class="invalid-feedback">{{$errors->first('foto')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <br />
                                <h4> <i class="fa fa-address-book"></i>&nbsp;&nbsp;Data Kontak</h4>
                                <hr>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="telepon">No. Telepon</label>
                                    <div class="col-md-9">
                                        <input class="form-control text-uppercase {{$errors->has('telepon') ? 'is-invalid' : ''}}" value="{{$client->telepon}}" id="telepon" type="number" name="telepon" placeholder="Nomor Telepon">
                                        @if($errors->has('telepon'))
                                            <span class="invalid-feedback">{{$errors->first('telepon')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="email">Email</label>
                                    <div class="col-md-9">
                                        <input class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" value="{{$client->email}}" id="email" type="email" name="email" placeholder="Masukan Email" autocomplete="email">
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <br>
                                <h4> <i class="fa fa-credit-card"></i>&nbsp;&nbsp;Informasi Rekening</h4>
                                <hr>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="no_rek">No. Rekening</label>
                                    <div class="col-md-9">
                                        <input class="form-control text-uppercase {{$errors->has('no_rek') ? 'is-invalid' : ''}}" value="{{$client->no_rek}}" id="no_rek" type="number" name="no_rek" placeholder="Nomor Rekening">
                                        @if($errors->has('no_rek'))
                                            <span class="invalid-feedback">{{$errors->first('no_rek')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="tipe_bank">Nama Bank</label>
                                    <div class="col-md-9">
                                        <input class="form-control text-uppercase {{$errors->has('tipe_bank') ? 'is-invalid' : ''}}" value="{{$client->tipe_bank}}" id="tipe_bank" type="text" name="tipe_bank" placeholder="Nama Bank">
                                        @if($errors->has('tipe_bank'))
                                            <span class="invalid-feedback">{{$errors->first('tipe_bank')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <br />
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-sm btn-primary" onclick="" type="submit">
                                    <i class="fa fa-dot-circle-o"></i> Submit
                                </button>

                                <a class="btn btn-sm btn-danger" href="{{route('client')}}">
                                    <i class="fa fa-ban"></i> Batal</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script>

    </script>
@endsection

@section('breadcrumb')
    {{ Breadcrumbs::render('client.edit', $client) }}
@endsection
