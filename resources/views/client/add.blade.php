@extends('layouts.admin.main')

@section('content')
    <?php
    $app_cfg = config('kontrollingapp.type_member');
    ?>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <form class="form-horizontal" action="{{route('client.simpan')}}" method="post" enctype="multipart/form-data">
                        <div class="card-header">
                            <strong>Form Tambah Client</strong>
                        </div>
                        <div class="card-body">
                                {{csrf_field()}}
                            <h4> <i class="fa fa-info"></i>&nbsp;&nbsp;Informasi Umum</h4>
                            <hr>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="text-input">Kode Client</label>
                                    <div class="col-md-9">
                                        <input class="form-control" id="text-input" type="text" readonly value="{{strtoupper($generate_code)}}" name="kode" placeholder="Text">
                                        <span class="help-block info"><small>Kode Ini Digenerate Otomatis Oleh Sistem</small></span>
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="nama">Nama</label>
                                    <div class="col-md-9">
                                        <input class="form-control {{$errors->has('nama') ? 'is-invalid' : ''}}" id="nama" type="text" name="nama" value="{{old('nama')}}" placeholder="Masukan Nama Perusahaan">
                                        @if($errors->has('nama'))
                                            <span class="invalid-feedback">
                                                {{$errors->first('nama')}}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="npwp">NPWP</label>
                                    <div class="col-md-9">
                                        <input class="form-control mask_npwp {{$errors->has('npwp') ? 'is-invalid' : ''}}" id="npwp" type="text" name="npwp" value="{{old('npwp')}}" placeholder="__.___.___._-___.___">
                                        @if($errors->has('npwp'))
                                            <span class="invalid-feedback">
                                                    {{$errors->first('npwp')}}
                                                </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="alamat">Alamat</label>
                                    <div class="col-md-9">
                                        <textarea class="form-control" id="alamat" name="alamat" rows="9" placeholder="Masukan Alamat Lengkap Perusahaan..">{{old('alamat')}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="kuota">Kuota </label>
                                    <div class="col-md-9">
                                        <input class="form-control {{$errors->has('kuota') ? 'is-invalid' : ''}}" id="kuota" type="text" name="kuota" value="{{old('kuota')}}" placeholder="Masukan Kuota">
                                        @if($errors->has('kuota'))
                                            <span class="invalid-feedback">
                                                {{$errors->first('kuota')}}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="foto">Foto Profil</label>
                                    <div class="col-md-9">
                                        <input class="form-control {{$errors->has('foto') ? 'is-invalid' : ''}}" id="foto" type="file" name="foto" value="{{old('foto')}}">
                                        @if($errors->has('foto'))
                                            <span class="invalid-feedback">
                                                {{$errors->first('foto')}}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            <br>

                            <h4> <i class="fa fa-address-book"></i>&nbsp;&nbsp;Data Kontak</h4>
                            <hr>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="telepon">No. Telepon</label>
                                <div class="col-md-9">
                                    <input class="form-control text-uppercase {{$errors->has('telepon') ? 'is-invalid' : ''}}" value="{{old('telepon')}}" id="telepon" type="number" name="telepon" placeholder="Nomor Telepon">
                                    @if($errors->has('telepon'))
                                        <span class="invalid-feedback">{{$errors->first('telepon')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="email">Email</label>
                                <div class="col-md-9">
                                    <input class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" value="{{old('email')}}" id="email" type="email" name="email" placeholder="Masukan Email" autocomplete="email">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                    @enderror
                                </div>
                            </div>
                            <br>
                            <h4> <i class="fa fa-credit-card"></i>&nbsp;&nbsp;Informasi Rekening</h4>
                            <hr>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="no_rek">No. Rekening</label>
                                <div class="col-md-9">
                                    <input class="form-control text-uppercase {{$errors->has('no_rek') ? 'is-invalid' : ''}}" value="{{old('no_rek')}}" id="no_rek" type="number" name="no_rek" placeholder="Nomor Rekening">
                                    @if($errors->has('no_rek'))
                                        <span class="invalid-feedback">{{$errors->first('no_rek')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="tipe_bank">Nama Bank</label>
                                <div class="col-md-9">
                                    <input class="form-control text-uppercase {{$errors->has('tipe_bank') ? 'is-invalid' : ''}}" value="{{old('tipe_bank')}}" id="tipe_bank" type="text" name="tipe_bank" placeholder="Nama Bank">
                                    @if($errors->has('tipe_bank'))
                                        <span class="invalid-feedback">{{$errors->first('tipe_bank')}}</span>
                                    @endif
                                </div>
                            </div>
                            <br />

                        </div>
                        <div class="card-footer">
                            <button class="btn btn-sm btn-primary" type="submit">
                                <i class="fa fa-dot-circle-o"></i> Submit</button>
                            <button class="btn btn-sm btn-danger" type="reset">
                                <i class="fa fa-ban"></i> Reset</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('breadcrumb')
    {{ Breadcrumbs::render('client.add') }}
@endsection
