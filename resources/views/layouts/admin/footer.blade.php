<footer class="app-footer">
    <div>
        <span>&copy; {{date('Y')}} PT. Williams International Jaya.</span>
    </div>
    <div class="ml-auto">
        <span>Powered by</span>
        <a href="https://mobidu.co.id">MOBIDU</a>
    </div>
</footer>