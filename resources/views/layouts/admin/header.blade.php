<header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">
        <h5 class="navbar-brand-full" >{{option_exists('logo_text') ? option('logo_text') : 'KONTROLLING'}}</h5>
        <h5 class="navbar-brand-minimized">KTRL</h5>
    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item dropdown d-md-down-none">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="icon-bell"></i>
                @if(auth()->user()->unreadNotifications->count() > 0 )
                <span class="badge badge-pill badge-danger">{{auth()->user()->unreadNotifications->count()}}</span>
                @endif
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg" >
                <div class="dropdown-header text-center">
                    <strong>You have {{auth()->user()->unreadNotifications->count()}} notifications</strong>
                </div>
                <div id="notif_list">
                    @forelse(auth()->user()->unreadNotifications as $notif)
                        {{--                    {{var_dump($notif)}}--}}
                        <?php
                        $data_notif = json_decode($notif->data, true);
                        ?>
                        <a class="dropdown-item" href="{{route('notifications.read', ['id'=>$notif->id])}}">
                            <div class="message">
                                <div>
                                    <small class="text-muted">{{$data_notif['username']}}</small>
                                    <small class="text-muted float-right mt-1">{{$notif->created_at->diffForHumans()}}</small>
                                </div>
                                <div class="text-truncate font-weight-bold">
                                    <?php
                                    switch ($notif->type){
                                        case DO_SENDING_START :
                                            echo 'Laporan Pengiriman';
                                            break;
                                        case DO_SENDING_STOP :
                                            echo 'Laporan Pengiriman Selesai';
                                            break;
                                        default : echo 'Laporan User';
                                    }
                                    ?>
                                    <span class="fa fa-exclamation text-danger"></span>

                                </div>
                                <div class="small text-muted text-truncate">{{$data_notif['pesan']}}</div>
                            </div>
                        </a>
                    @empty

                @endforelse
                </div>
            </div>
        </li>

        <li class="nav-item dropdown mr-5 ml-5">
            <a class="nav-link nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="icon-user"></i>&nbsp;&nbsp;
                {{auth()->check() ? auth()->user()->name : ''}}
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-header text-center">
                    <strong>Account</strong>
                </div>
                <a class="dropdown-item" href="#">
                    <i class="fa fa-envelope-o"></i> Messages
                    <span class="badge badge-success">42</span>
                </a>
                <div class="dropdown-header text-center">
                    <strong>Settings</strong>
                </div>
                <a class="dropdown-item" href="#">
                    <i class="fa fa-user"></i> Profile</a>
                <a class="dropdown-item" href="#">
                    <i class="fa fa-wrench"></i> Settings</a>

                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                    <i class="fa fa-lock"></i> Logout</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </ul>
</header>