<?php
$menuAdmin = [
    'dashboard'=>[
        'icon'=>'icon-speedometer',
        'name'=>'dashboard',
        'route'=>'home',
    ],
    'Master Data'=>[
        'menu'=>[
            'karyawan'=>[
                'icon'=>'icon-people',
                'name'=>'Karyawan',
                'route'=>'karyawan',
            ],

            'Client'=>[
                'icon'=>'icon-briefcase',
                'name'=>'Client',
                'route'=>'client',
            ],
            'Kendaraan'=>[
                'icon'=>'icon-speedometer',
                'name'=>'Kendaraan',
                'route'=>'kendaraan',
            ],
            'Tarif Lokasi'=>[
                'icon'=>'icon-credit-card',
                'name'=>'Tarif Lokasi',
                'route'=>'tarif',
            ],
        ]
    ],
    'Driver'=>[
        'menu'=>[
            'Driver'=>[
                'icon'=>'icon-emotsmile',
                'name'=>'Driver',
                'route'=>'driver',
            ],
            'Tambah Driver'=>[
                'icon'=>'icon-plus',
                'name'=>'Driver',
                'route'=>'driver.add',
            ],
        ]
    ],
    'Transaksi'=>[
        'menu'=>[
            'do'=>[
                'icon'=>'icon-people',
                'name'=>'do',
                'route'=>'do',
            ],
            'BOP'=>[
                'icon'=>'icon-credit-card',
                'name'=>'BOP',
                'route'=>'bop',
            ],
        ]
    ],
    'LAPORAN'=>[
        'menu'=>[
            'MONITORING'=>[
                'icon'=>'icon-map',
                'name'=>'MONITORING',
                'route'=>'laporan.monitoring',
            ],
            'do'=>[
                'icon'=>'icon-chart',
                'name'=>'do',
                'route'=>'laporan.do',
            ],
            'BOP'=>[
                'icon'=>'icon-chart',
                'name'=>'BOP',
                'route'=>'laporan.bop',
            ],
            'INVOICE'=>[
                'icon'=>'icon-calculator',
                'name'=>'INVOICE',
                'route'=>'laporan.invoice',
            ],
        ]
    ],
    'Setting'=>[
        'menu'=>[
            'SETTING APP'=>[
                'icon'=>'icon-map',
                'name'=>'SETTING APP',
                'route'=>'setting.app',
            ],
        ]
    ],


];

$menuPengurus = [
    'dashboard'=>[
        'icon'=>'icon-speedometer',
        'name'=>'dashboard',
        'route'=>'home',
    ],
    'Transaksi'=>[
        'menu'=>[
            'do'=>[
                'icon'=>'icon-people',
                'name'=>'do',
                'route'=>'do',
            ],
            'BOP'=>[
                'icon'=>'icon-credit-card',
                'name'=>'BOP',
                'route'=>'bop',
            ],
        ]
    ],


];

$menuDriver = [
    'dashboard'=>[
        'icon'=>'icon-speedometer',
        'name'=>'dashboard',
        'route'=>'home',
    ],
    'Master Data'=>[
        'menu'=>[
            'Client'=>[
                'icon'=>'icon-briefcase',
                'name'=>'Client',
                'route'=>'client',
            ],
        ]
    ],
    'Transaksi'=>[
        'menu'=>[
            'do'=>[
                'icon'=>'icon-people',
                'name'=>'do',
                'route'=>'do',
            ],
            'BOP'=>[
                'icon'=>'icon-credit-card',
                'name'=>'BOP',
                'route'=>'bop',
            ],
        ]
    ],
];

$menu = [];

if(auth()->user()->type == USER_TYPE_ADMIN){
    $menu = $menuAdmin;
}else if(auth()->user()->type == USER_TYPE_OWNER){
    $menu = $menuAdmin;
}else if(auth()->user()->type == USER_TYPE_PENGURUS ){
    $mennu = $menuPengurus;
}else if(auth()->user()->type == USER_TYPE_DRIVER){
    $menu = $menuDriver;
}
?>
<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            @forelse($menu as $key => $value)
                @if(isset($value['menu']))
                    <li class="nav-title">{{strtoupper($key)}}</li>
                    @foreach($value['menu'] as  $k => $m)
                        <li class="nav-item">
                            <a class="nav-link {{request()->route()->getName() == $m['route'] ? 'active' : ''}}" href="{{route($m['route'])}}">
                                <i class="nav-icon {{$m['icon']}}"></i> {{strtoupper($k)}}
                            </a>
                        </li>
                    @endforeach
                @else
                    <li class="nav-item">
                        <a class="nav-link {{request()->route()->getName() == $value['route'] ? 'active' : ''}}" href="{{route($value['route'])}}">
                            <i class="nav-icon {{$value['icon']}}"></i> {{strtoupper($key)}}
                        </a>
                    </li>
                @endif


            @empty

            @endforelse

        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
