<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Aplikasi {{option_exists('logo_text') ? option('logo_text') : 'KONTROLLING'}}</title>


    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/blue/pace-theme-minimal.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <link href="{{ asset('css/icons.css') }}" rel="stylesheet">

</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
@include('layouts.admin.header')
<div class="app-body" id="app">
    @include('layouts.admin.sidebar')
    <main class="main">
        @include('layouts.admin.breadcrumb')
        <div class="container-fluid">
            <div class="animated fadeIn">
                @yield('content')
            </div>
        </div>
    </main>
</div>

@include('layouts.admin.footer')
@yield('script')
<!-- Scripts -->
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyrchHM_k8ks7WPSM3TXWt9NEFg2LPPDM&callback=initMap"
        type="text/javascript"></script>
</body>
</html>
