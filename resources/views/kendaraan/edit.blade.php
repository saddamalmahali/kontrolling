@extends('layouts.admin.main')

@section('content')
    <?php
    $app_cfg = config('kontrollingapp.type_member');
    ?>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <form class="form-horizontal" action="{{route('kendaraan.simpan')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="id" value="{{$kendaraan->id}}">
                            <div class="card-header">
                                <strong>Form Edit</strong> Kendaraan
                            </div>
                            <div class="card-body">
                                <h4><i class="fa fa-info-circle"></i>&nbsp;&nbsp;Data General</h4>
                                <hr>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="kode">Kode Kendaraan</label>
                                    <div class="col-md-9">
                                        <input class="form-control text-uppercase" id="kode" type="text" readonly value="{{$kendaraan->kode}}" name="kode" placeholder="Text">
                                        <span class="help-block info"><small>Kode Ini Digenerate Otomatis Oleh Sistem</small></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="ktp">No Seri</label>
                                    <div class="col-md-9">
                                        <input class="form-control text-uppercase {{$errors->has('seri') ? 'is-invalid' : ''}}" value="{{$kendaraan->seri}}" id="seri" type="text" name="seri" placeholder="Masukan Nomor Seri">
                                        @if($errors->has('seri'))
                                            <span class="invalid-feedback">{{$errors->first('seri')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="stnk">No. STNK</label>
                                    <div class="col-md-9">
                                        <input class="form-control text-uppercase {{$errors->has('stnk') ? 'is-invalid' : ''}}" value="{{$kendaraan->stnk}}" id="stnk" type="text" name="stnk" placeholder="Masukan Nomor STNK">
                                        @if($errors->has('stnk'))
                                            <span class="invalid-feedback">{{$errors->first('stnk')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="bpkb">No. BPKB</label>
                                    <div class="col-md-9">
                                        <input class="form-control text-uppercase {{$errors->has('bpkb') ? 'is-invalid' : ''}}" value="{{$kendaraan->bpkb}}" id="bpkb" type="text" name="bpkb" placeholder="Masukan Nomor BPKB">
                                        @if($errors->has('bpkb'))
                                            <span class="invalid-feedback">{{$errors->first('bpkb')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="kir">No. KIR</label>
                                    <div class="col-md-9">
                                        <input class="form-control text-uppercase {{$errors->has('kir') ? 'is-invalid' : ''}}" value="{{$kendaraan->kir}}" id="kir" type="text" name="kir" placeholder="Masukan Nomor KIR">
                                        @if($errors->has('kir'))
                                            <span class="invalid-feedback">{{$errors->first('kir')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="no_mesin">No. Mesin</label>
                                    <div class="col-md-9">
                                        <input class="form-control text-uppercase {{$errors->has('no_mesin') ? 'is-invalid' : ''}}" value="{{$kendaraan->no_mesin}}" id="no_mesin" type="text" name="no_mesin" placeholder="Masukan Nomor Mesin">
                                        @if($errors->has('no_mesin'))
                                            <span class="invalid-feedback">{{$errors->first('no_mesin')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="no_rangka">No. Rangka</label>
                                    <div class="col-md-9">
                                        <input class="form-control text-uppercase {{$errors->has('no_rangka') ? 'is-invalid' : ''}}" value="{{$kendaraan->no_rangka}}" id="no_rangka" type="text" name="no_rangka" placeholder="Masukan Nomor Rangka">
                                        @if($errors->has('no_rangka'))
                                            <span class="invalid-feedback">{{$errors->first('no_rangka')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="name">No Plat</label>
                                    <div class="col-md-9">
                                        <input class="form-control text-uppercase {{$errors->has('no_plat') ? 'is-invalid' : ''}}" value="{{$kendaraan->no_plat}}" id="no_plat" type="text" name="no_plat" placeholder="Masukan Nomor Plat">
                                        @if($errors->has('no_plat'))
                                            <span class="invalid-feedback">{{$errors->first('no_plat')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="name">Jenis Kendaraan</label>
                                    <div class="col-md-9">
                                        <select name="jenis_kendaraan" id="jenis_kendaraan" class="form-control">
                                            <option value="">Pilih Jenis</option>
                                            <option value="{{JENIS_KENDARAAN_PICKUP}}" {{$kendaraan->jenis_kendaraan == JENIS_KENDARAAN_PICKUP ? 'selected'  : ''}}>{{JENIS_KENDARAAN_PICKUP}}</option>
                                            <option value="{{JENIS_KENDARAAN_TRUK}}" {{$kendaraan->jenis_kendaraan == JENIS_KENDARAAN_TRUK ? 'selected'  : ''}}>{{JENIS_KENDARAAN_TRUK}}</option>
                                            <option value="{{JENIS_KENDARAAN_FUSO}}" {{$kendaraan->jenis_kendaraan == JENIS_KENDARAAN_FUSO ? 'selected'  : ''}}>{{JENIS_KENDARAAN_FUSO}}</option>
                                        </select>
                                        @if($errors->has('jenis_kendaraan'))
                                            <span class="invalid-feedback">{{$errors->first('jenis_kendaraan')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="name">Pabrikan</label>
                                    <div class="col-md-9">
                                        <input class="form-control text-uppercase {{$errors->has('pabrikan') ? 'is-invalid' : ''}}" value="{{$kendaraan->pabrikan}}" id="pabrikan" type="text" name="pabrikan" placeholder="Masukan Pabrikan">
                                        @if($errors->has('pabrikan'))
                                            <span class="invalid-feedback">{{$errors->first('pabrikan')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="tahun_buat">Tahun Buat</label>
                                    <div class="col-md-9">
                                        <input class="form-control text-uppercase {{$errors->has('tahun_buat') ? 'is-invalid' : ''}}" value="{{$kendaraan->tahun_buat}}" id="tahun_buat" type="number" name="tahun_buat" placeholder="Masukan Tahun Pembuatan">
                                        @if($errors->has('tahun_buat'))
                                            <span class="invalid-feedback">{{$errors->first('tahun_buat')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-sm btn-primary" type="submit">
                                    <i class="fa fa-dot-circle-o"></i> Submit</button>
                                <button class="btn btn-sm btn-danger" type="reset">
                                    <i class="fa fa-ban"></i> Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection

@section('breadcrumb')
    {{ Breadcrumbs::render('kendaraan.edit', $kendaraan) }}
@endsection
