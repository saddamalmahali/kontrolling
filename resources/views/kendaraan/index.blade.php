@extends('layouts.admin.main')

@section('content')
    <?php
    $app_cfg = config('kontrollingapp.type_member');
    ?>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Data Kendaraan
                                <div class="float-right"><a href="{{route('kendaraan.add')}}" class="btn btn-primary"><span class="icon-user-follow"></span>&nbsp;&nbsp; Tambah Kendaraan</a></div>
                            </h5>

                        </div>

                        <div class="card-body">

                            @if (session('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('success') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            @if (session('failed'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('failed') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            <form action="" id="form_option_kendaraan">
                                <div class="row">

                                    <div class="col-md-3 mb-2">
                                        <div class="form-group row mb-0">
                                            <label for="data_length" class="col-md-3 col-form-label-sm mr-0 pr-0">Tampil :</label>
                                            <div class="col-md-9 ml-0 pl-0">
                                                <select name="data_length" onchange="document.getElementById('form_option_kendaraan').submit()" class="form-control form-control-sm" id="data_length">
                                                    <option value="10" @if(request()->has('data_length')) {{request('data_length') == 10 ? 'selected' : ''}} @endif>10</option>
                                                    <option value="20" @if(request()->has('data_length')) {{request('data_length') == 20 ? 'selected' : ''}} @endif>20</option>
                                                    <option value="50" @if(request()->has('data_length')) {{request('data_length') == 50 ? 'selected' : ''}} @endif>50</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 mb-2">
                                        <div class="form-group row mb-0">
                                            <label for="data_length" class="col-md-3 col-form-label-sm mr-0 pr-0">Order :</label>
                                            <div class="col-md-9 ml-0 pl-0">
                                                <select name="order_by" onchange="document.getElementById('form_option_kendaraan').submit()" class="form-control form-control-sm" id="data_length">
                                                    <option value="">Pilih Kolom</option>
                                                    <option value="kode" @if(request()->has('order_by')) {{request('order_by') == 'kode' ? 'selected' : ''}} @endif>Kode</option>
                                                    <option value="seri" @if(request()->has('order_by')) {{request('order_by') == 'seri' ? 'selected' : ''}} @endif>Seri</option>
                                                    <option value="no_plat" @if(request()->has('order_by')) {{request('order_by') == 'no_plat' ? 'selected' : ''}} @endif>No Plat</option>
                                                    <option value="jenis_kendaraan" @if(request()->has('order_by')) {{request('order_by') == 'jenis_kendaraan' ? 'selected' : ''}} @endif>Jenis Kendaraan</option>
                                                    <option value="pabrikan" @if(request()->has('order_by')) {{request('order_by') == 'pabrikan' ? 'selected' : ''}} @endif>Pabrikan</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered dataTable" id="data_table_kendaraan">
                                                <thead>
                                                <tr>
                                                    <th class="text-center text-uppercase" width="20%">Kode</th>
                                                    <th class="text-center text-uppercase">Seri</th>
                                                    <th class="text-center text-uppercase" width="200">No Plat</th>
                                                    <th class="text-center text-uppercase">Jenis Kendaraan</th>
                                                    <th class="text-center text-uppercase">Pabrikan</th>
                                                    <th class="text-center text-uppercase">Status</th>
                                                    <th class="text-center" width="10%">Opsi</th>
                                                </tr>
                                                <tr>
                                                    <form type="GET">
                                                        <td><input type="text" class="form-control" value="{{request()->has('kode_search') ? request('kode_search') : ''}}" name="kode_search"></td>
                                                        <td><input type="text" class="form-control" value="{{request()->has('seri_search') ? request('seri_search') : ''}}" name="seri_search"></td>
                                                        <td><input type="text" class="form-control" value="{{request()->has('no_plat_search') ? request('no_plat_search') : ''}}" name="no_plat_search"></td>
                                                        <td><input type="text" class="form-control" value="{{request()->has('jenis_kendaraan_search') ? request('jenis_kendaraan_search') : ''}}" name="jenis_kendaraan"></td>
                                                        <td><input type="text" class="form-control" value="{{request()->has('pabrikan_search') ? request('pabrikan_search') : ''}}" name="pabrikan_search"></td>
                                                        <td>
                                                            <select class="form-control" name="status_search" id="status_search">
                                                                <option value="">Pilis Status</option>
                                                                <option value="aktif" {{request()->get('status_search') == 'aktif' ? 'selected' : ''}}>Aktif</option>
                                                                <option value="tidak_aktif" {{request()->get('status_search') == 'tidak_aktif' ? 'selected' : ''}}>Tidak Aktif</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <center>
                                                                <button type="submit" class="btn btn-primary btn-sm btn-block mb-1">Search</button>
                                                                <a href="{{route('kendaraan')}}" class="btn btn-danger btn-block btn-sm">Reset</a>
                                                            </center>
                                                        </td>
                                                    </form>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @forelse($list_data as $data)
                                                    <tr>
                                                        <td class="text-center text-uppercase">{{$data->kode}}</td>
                                                        <td class="text-uppercase">{{$data->seri}}</td>
                                                        <td class="text-uppercase text-center">{{$data->no_plat}}</td>
                                                        <td class="text-uppercase text-center">{{$data->jenis_kendaraan}}</td>
                                                        <td class="text-uppercase text-center">{{$data->pabrikan}}</td>
                                                        <td class="text-uppercase text-center">
                                                            <?php
                                                                echo $data->status == 'aktif' ? '<span class="badge badge-info">Aktif</span>'  : '<span class="badge badge-danger">Tidak Aktif</span>'
                                                            ?>
                                                        </td>
                                                        <td class="text-center">
                                                            <a href="{{route('kendaraan.edit', ['id'=>$data->id])}}" class="btn btn-sm btn-brand btn-behance mb-1 text-white">
                                                                <i class="fa fa-pencil"></i><span>Edit</span>
                                                            </a>
                                                            <button type="button" onclick="if(confirm('Apakah Anda yakin Akan Menghapus Data ini?')){document.getElementById('form_hapus_{{$data->kode}}').submit()}" class="btn btn-sm btn-brand btn-youtube mb-1">
                                                                <i class="fa fa-trash"></i><span>Hapus</span>
                                                            </button>

                                                            <form id="form_hapus_{{$data->kode}}" method="post" action="{{route('kendaraan.hapus', ['kode'=>$data->kode])}}">
                                                                {{csrf_field()}}
                                                                <input type="hidden" name="id" value="{{$data->id}}">
                                                            </form>

                                                        </td>
                                                    </tr>
                                                @empty
                                                    <tr>
                                                        <td colspan="7"><center>Tidak Terdapat Data Driver</center></td>
                                                    </tr>
                                                @endforelse
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="float-left">Total Record : {{$list_data->total()}} Data Kendaraan</div>
                                                <div class="float-right">{{$list_data->appends($_GET)->links()}}</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('breadcrumb')
    {{ Breadcrumbs::render('kendaraan') }}
@endsection
