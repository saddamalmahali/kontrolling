@extends('layouts.admin.main')

@section('content')
    <div>
        <div class="card">
            <div class="card-header">
                SETTING APLIKASI
            </div>
            <form action="{{route('setting.save')}}" method="post" enctype="multipart/form-data">
                <div class="card-body">

                    @if(session()->has('sukses'))
                        <div class="alert alert-primary">
                            {{session('sukses')}}
                        </div>
                    @endif

                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="site_name">Nama Situs</label>
                        <input type="text" class="form-control {{$errors->has('site_name') ? 'is-invalid' : ''}}" name="site_name" id="site_name" placeholder="Masukan Nama Situs" value="{{option_exists('site_name') ? option('site_name') : ''}}">
                        @if($errors->has('site_name'))
                            <span class="invalid-feedback">{{$errors->first('site_name')}}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="company_name">Nama Perusahaan</label>
                        <input type="text" class="form-control {{$errors->has('company_name') ? 'is-invalid' : ''}}" name="company_name" placeholder="Masukan Nama Perusahaan" value="{{option_exists('company_name') ? option('company_name') : ''}}">
                        @if($errors->has('company_name'))
                            <span class="invalid-feedback">{{$errors->first('company_name')}}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="address">Alamat</label>
                        <textarea name="address" id="address" rows="4" class="form-control {{$errors->has('address') ? 'is-invalid' : ''}}">{{option_exists('address') ? option('address') : ''}}</textarea>
                        @if($errors->has('address'))
                            <span class="invalid-feedback">{{$errors->first('address')}}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="kecamatan">Kecamatan</label>
                        <input type="text" class="form-control {{$errors->has('kecamatan') ? 'is-invalid' : ''}}" name="kecamatan" id="kecamatan" placeholder="Masukan Kecamatan" value="{{option_exists('kecamatan') ? option('kecamatan') : ''}}">
                        @if($errors->has('kecamatan'))
                            <span class="invalid-feedback">{{$errors->first('kecamatan')}}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="kabupaten">Kabupaten</label>
                        <input type="text" class="form-control {{$errors->has('kabupaten') ? 'is-invalid' : ''}}" name="kabupaten" id="kabupaten" placeholder="Masukan Kabupaten" value="{{option_exists('kabupaten') ? option('kabupaten') : ''}}">
                        @if($errors->has('kabupaten'))
                            <span class="invalid-feedback">{{$errors->first('kabupaten')}}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="provinsi">Provinsi</label>
                        <input type="text" class="form-control {{$errors->has('provinsi') ? 'is-invalid' : ''}}" name="provinsi" id="provinsi" placeholder="Masukan Provinsi" value="{{option_exists('provinsi') ? option('provinsi') : ''}}">
                        @if($errors->has('provinsi'))
                            <span class="invalid-feedback">{{$errors->first('provinsi')}}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="telp">No Telp</label>
                        <input type="text" class="form-control {{$errors->has('telp') ? 'is-invalid' : ''}}" name="telp" id="telp" placeholder="Masukan Nomor Telepon" value="{{option_exists('telp') ? option('telp') : ''}}">
                        @if($errors->has('telp'))
                            <span class="invalid-feedback">{{$errors->first('telp')}}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" name="email" id="email" placeholder="Masukan Alamat Email" value="{{option_exists('email') ? option('email') : ''}}">
                        @if($errors->has('email'))
                            <span class="invalid-feedback">{{$errors->first('email')}}</span>
                        @endif
                    </div>

                </div>
                <div class="card-footer">
                    <button class="btn btn-sm btn-primary" type="submit">
                        <i class="fa fa-dot-circle-o"></i> Submit</button>
                    <button class="btn btn-sm btn-danger" type="reset">
                        <i class="fa fa-ban"></i> Reset</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('breadcrumb')
    {{ Breadcrumbs::render('setting') }}
@endsection
