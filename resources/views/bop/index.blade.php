@extends('layouts.admin.main')

@section('content')
    <?php
    $app_cfg = config('kontrollingapp.type_member');
    ?>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Data Invoice
                            </h5>

                        </div>

                        <div class="card-body">

                            @if (session('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('success') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            @if (session('failed'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    {{ session('failed') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            @if(count($errors->all()) > 0)
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            <form action="" id="form_option_karyawan">
                                <div class="row">
                                    <div class="col-md-3 mb-2">
                                        <div class="form-group row mb-0">
                                            <label for="data_length" class="col-md-3 col-form-label-sm mr-0 pr-0">Tampil :</label>
                                            <div class="col-md-9 ml-0 pl-0">
                                                <select name="data_length" onchange="document.getElementById('form_option_karyawan').submit()" class="form-control form-control-sm" id="data_length">
                                                    <option value="10" @if(request()->has('data_length')) {{request('data_length') == 10 ? 'selected' : ''}} @endif>10</option>
                                                    <option value="20" @if(request()->has('data_length')) {{request('data_length') == 20 ? 'selected' : ''}} @endif>20</option>
                                                    <option value="50" @if(request()->has('data_length')) {{request('data_length') == 50 ? 'selected' : ''}} @endif>50</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 mb-2">
                                        <div class="form-group row mb-0">
                                            <label for="data_length" class="col-md-3 col-form-label-sm mr-0 pr-0">Order :</label>
                                            <div class="col-md-9 ml-0 pl-0">
                                                <select name="order_by" onchange="document.getElementById('form_option_karyawan').submit()" class="form-control form-control-sm" id="data_length">
                                                    <option value="">Pilih Kolom</option>
                                                    <option value="email" @if(request()->has('order_by')) {{request('order_by') == 'email' ? 'selected' : ''}} @endif>Email</option>
                                                    <option value="name" @if(request()->has('order_by')) {{request('order_by') == 'name' ? 'selected' : ''}} @endif>Nama</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="overflow-auto">
                                        <table class="table table-bordered table-fix dataTable" id="data_table_karyawan">
                                            <thead>
                                            <tr>
                                                <th class="text-center text-uppercase" style="min-width: 200px;">Kode BOP</th>
                                                <th class="text-center text-uppercase" style="min-width: 200px;">Nomor DO</th>
                                                <th class="text-center text-uppercase" style="min-width: 100px;">Tanggal</th>
                                                <th class="text-center text-uppercase" style="min-width: 200px;">Penerima</th>
                                                <th class="text-center text-uppercase" style="min-width: 100px;">Kuantitas</th>
                                                <th class="text-center text-uppercase" style="min-width: 200px;">Biaya</th>
                                                <th class="text-center text-uppercase" style="min-width: 250px;">Alamat Tujuan</th>
                                                <th class="text-center text-uppercase" style="min-width: 80px;">Status</th>
                                                <th class="text-center text-uppercase" style="min-width: 100px;">Opsi</th>
                                            </tr>
                                            <tr>
                                                <form type="GET">
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>
                                                        <center>
                                                            <button type="submit" class="btn btn-primary btn-sm btn-block mb-1">Search</button>
                                                            <a href="{{route('invoice')}}" class="btn btn-danger btn-block btn-sm">Reset</a>
                                                        </center>
                                                    </td>
                                                </form>

                                            </tr>
                                            </thead>


                                            <tbody>

                                                @forelse($list_data as $data)

                                                    <tr>
                                                        <td class="text-uppercase text-center">{{$data->nomor ? $data->nomor : '-'}}</td>
                                                        <td class="text-uppercase text-center">
                                                            @if($data->do)
                                                                <?php
                                                                    $do = $data->do;
                                                                    $length_list = $do->detile()->count();
                                                                ?>
                                                                <?php if($length_list > 0): ?>
                                                                    <?php
                                                                                $noDO = '';
                                                                                $indexDo = 1;
                                                                                foreach ($do->detile as $detile){
                                                                                    if($detile->no_do){
                                                                                        $noDO .= $detile->no_do;
                                                                                        if($length_list> $indexDo){
                                                                                            $noDO .= ', <br />';
                                                                                        }
                                                                                    }

                                                                                    $indexDo++;
                                                                                }
                                                                                ?>

                                                                    <?= $noDO ?>
                                                                <?php else: ?>
                                                                            -
                                                                <?php endif; ?>
{{--                                                                <a href="{{route('do.detail', ['id'=>$data->do->id])}}">{{$data->do->nomor}}</a>--}}
                                                            @else
                                                                -
                                                            @endif
                                                        </td>
                                                        <td class="text-uppercase text-center">{{date('d-m-Y', strtotime($data->tanggal))}}</td>
                                                        <td class="text-uppercase">
                                                            @if($data->penerima)
                                                                {{strtoupper($data->penerima->name)}}
                                                            @else
                                                                -
                                                            @endif

                                                        </td>
                                                        <td class="text-uppercase text-center">
                                                            <?php
                                                            if($data->kuantitas > 1000){
                                                                echo ($data->kuantitas/1000). ' Ton';
                                                            }else{
                                                                echo $data->kuantitas.' Kg';
                                                            }
                                                            ?>
                                                        </td>
                                                        <td class="text-uppercase text-center">
                                                            Rp. {{number_format($data->biaya)}}
                                                        </td>
                                                        <td class="text-uppercase text-center">
                                                            {{strtoupper($data->tujuan)}}
                                                        </td>
                                                        <td class="text-uppercase text-center">
                                                            <?php
                                                            switch ($data->status){
                                                                case BOP_STATUS_KIRIM :
                                                                    echo '<span class="badge badge-info">Sudah Lunas</span>';
                                                                    break;
                                                                case BOP_STATUS_PENDING :
                                                                    echo '<span class="badge badge-primary">Terkirim ke Driver</span>';
                                                                    break;

                                                            }
                                                            ?>
                                                            <span class="badge-info"></span>
                                                        </td>
                                                        <td class="text-center">


                                                        </td>
                                                    </tr>

                                                @empty
                                                    <tr>
                                                        <td colspan="9"><center>Tidak Terdapat Data Delivery Order</center></td>
                                                    </tr>
                                                @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="float-left">Total Record : {{$list_data->total()}} Data Delivery Order</div>
                                            <div class="float-right">{{$list_data->appends($_GET)->links()}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function onAddInvoice(){
            event.preventDefault();
            var form = document.getElementById('form_add_invoice').submit();
            // var coffee = document.forms[0];
            // console.log(coffee);
            // var txt = "";
            // var i;
            // for (i = 0; i < coffee.length; i++) {
            //     if (coffee[i].checked) {
            //         txt = txt + coffee[i].value + " ";
            //     }
            // }
            //
            // console.log(txt);
        }

        function onCheckboxClick(){
            var invoice = document.getElementsByName('invoice');
        }
    </script>
@endsection

@section('breadcrumb')
    {{ Breadcrumbs::render('do') }}
@endsection
