@extends('layouts.admin.main')

@section('content')
    <?php
    $app_cfg = config('kontrollingapp.type_member');
    ?>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <form class="form-horizontal" action="{{route('do.simpan_bop')}}" method="post" enctype="multipart/form-data" id="form_bop">
                            {{csrf_field()}}
                            <input type="hidden" name="do_id" value="{{$do->id}}">
                            <div class="card-header">
                                <strong>Form Tambah</strong> Kendaraan
                            </div>
                            <div class="card-body">
                                <h4><i class="fa fa-info-circle"></i>&nbsp;&nbsp;Info Delivery</h4>
                                <hr>
                                <div class="alert alert-primary">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <table class="table table-borderless" >
                                                <tr>
                                                    <th width="30%" style="padding:  0;">Nomor DO</th>
                                                    <td style="padding:  0;">:</td>
                                                    <td style="padding:  0;">{{$do->nomor}}</td>
                                                </tr>
                                                <tr>
                                                    <th width="30%" style="padding:  0;">Tanggal</th>
                                                    <td style="padding:  0;" >:</td>
                                                    <td style="padding:  0;" >{{date('d-m-Y', strtotime($do->tanggal))}}</td>
                                                </tr>
                                                <tr>
                                                    <th width="30%" style="padding:  0;">Total Berat</th>
                                                    <td style="padding:  0;" >:</td>
                                                    <td style="padding:  0;" >
                                                        <?php
                                                            $berat = $do->detile()->sum('kuantitas');

                                                            if($berat<1000){
                                                                echo $berat.' Kg';
                                                            }else{
                                                                $berat = $berat/1000;
                                                                echo $berat.' Ton';
                                                            }
                                                        ?>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-md-6">
                                            <table class="table table-sm table-borderless">
                                                <tr>
                                                    <th style="padding:  0;">Nomor DO</th>
                                                    <td style="padding:  0;">:</td>
                                                    <td style="padding:  0;">{{$do->nomor}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="kode">Kode BOP</label>
                                    <div class="col-md-9">
                                        <input class="form-control" id="kode" type="text" readonly value="{{strtoupper($generate_code)}}" name="kode" placeholder="Text">
                                        <span class="help-block info"><small>Kode Ini Digenerate Otomatis Oleh Sistem</small></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="name">Penerima</label>
                                    <div class="col-md-9">
                                        <select name="penerima_id" id="penerima_id" class="form-control" disabled>
                                            <option value="">Pilih Jenis</option>
                                            @forelse($list_driver as $driver)
                                                <option value="{{$driver->id}}" {{$driver->id == $do->driver_id ? 'selected' : ''}}>{{strtoupper($driver->name)}}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                        @if($errors->has('jenis_kendaraan'))
                                            <span class="invalid-feedback">{{$errors->first('jenis_kendaraan')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="name">Tujuan</label>
                                    <div class="col-md-9">
                                        <input class="form-control text-uppercase {{$errors->has('tujuan') ? 'is-invalid' : ''}}" readonly value="{{$do->kabupaten}}" id="tujuan" type="text" name="pabrikan" placeholder="Masukan Pabrikan">
                                        @if($errors->has('tujuan'))
                                            <span class="invalid-feedback">{{$errors->first('tujuan')}}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="biaya">Biaya</label>
                                    <div class="col-md-9">
                                        <input class="form-control mask_money text-uppercase {{$errors->has('biaya') ? 'is-invalid' : ''}}" value="{{old('biaya')}}" id="biaya" type="text" name="biaya" placeholder="Masukan Biaya Pengiriman">
                                        @if($errors->has('biaya'))
                                            <span class="invalid-feedback">{{$errors->first('biaya')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="biaya">Bukti</label>
                                    <div class="col-md-9">
                                        <input class="form-control {{$errors->has('bukti') ? 'is-invalid' : ''}}" value="{{old('biaya')}}" id="bukti" type="file" name="bukti">
                                        @if($errors->has('bukti'))
                                            <span class="invalid-feedback">{{$errors->first('bukti')}}</span>
                                        @endif
                                    </div>
                                </div>

                            </div>
                            <div class="card-footer">
                                <button class="btn btn-sm btn-primary" type="submit">
                                    <i class="fa fa-dot-circle-o"></i> Submit</button>
                                <button class="btn btn-sm btn-danger" type="reset">
                                    <i class="fa fa-ban"></i> Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('breadcrumb')
    {{ Breadcrumbs::render('do.proses', $do) }}
@endsection